#version 330 core
uniform sampler2D grass;

in Vertex {
	smooth vec2 texCoord;
	smooth vec4 colour;
} IN;

// Basic fragment shader that uses the texture coords
out vec4 fragColour;

void main(void) {
	fragColour = texture(grass, IN.texCoord);
}