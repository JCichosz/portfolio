#pragma once
#include <iostream>
#include <Graphics.h>
#include <KeyboardController.h>
#include <GamepadController.h>
#include <EventSystem.h>
#include <Physics.h>
#include <Audio.h>
#include <ResourceManager.h>
#include <BootManager.h>
#include <Profiler.h>
#include <FileIO.h>
#include <GameLogic.h>

/*
 The main game loop. Initialises all subsystems and executes until the the shutdown event has been completed.
*/

int main() {

		int width = 1920;
		int height = 1080;

		EventSystem* es = new EventSystem();
		std::vector<GameObject*>* gameObjects = new vector<GameObject*>();

		BootManager bootMng(es, gameObjects);
		bootMng.start(); // Boot manager initialises the Resource manager and loads the first level

		// Ijnitialise all subsystem
		Graphics graphics(width, height, es, gameObjects);
		Physics physics(es, gameObjects);
		Audio audio(es, gameObjects);
		KeyboardController keyboard(es, gameObjects);
		GamepadController gamepad(0, es, gameObjects);
		FileIO fileIO(es, gameObjects);
		GameLogic logic(es, gameObjects);

		Profiler profiler(es, gameObjects);
		GameTimer timer = GameTimer();

		// Runs until the shutdown event is raised and completed
		while (!bootMng.doShutdown()) {
			float msec = timer.GetTimedMS();
			fileIO.handleEvents();
			keyboard.update();
			gamepad.update();
			physics.update(msec);
			graphics.update(msec);
			audio.update();
			logic.update();
			profiler.update(msec);
			es->clearHandledEvents();
			profiler.printNumberOfEvents();
			bootMng.handleEvents();
		}

}