#pragma once
#include <EventSystem.h>

// Loads and unloads levels 
class FileIO: public Subsystem {
public:
	FileIO(EventSystem* es, std::vector<GameObject*>* objects);
	~FileIO();

	void handleEvents();
	void loadLevel(std::string path);
	void unloadLevel();
	void reloadLevel();
	void loadGameVariables(std::string path);

	GameObjectType stringtoGameObjectType(std::string s);

private:
	bool fileExists(std::string path);

	int currentIndex;
	std::string currentLevel;
	std::string gameVariables;
	std::vector<std::string> levels;
};

