#include "FileIO.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <iterator>
#include <vector>
#include <string>
#include <SpawnEvent.h>
#include <FlatGround.h>
#include <SpawnPlayer.h>
#include <Ring.h>
#include <Quad.h>
#include <LoadLevelEvent.h>
#include <DestroyEvent.h>
#include <LoadGameVariablesEvent.h>
#include <WorldBound.h>

const std::string LEVEL_PATH = "../Files/level-";

FileIO::FileIO(EventSystem* es, std::vector<GameObject*>* objects): Subsystem(es, objects, FILE_IO) {
	currentIndex = 0;
}

FileIO::~FileIO() {
}

void FileIO::handleEvents() {
	for (int i = 0; i < es->getEventQueue().size(); i++) {
		Event* e = es->getEventQueue().at(i);

		if (needToHandleEvent(*e)) {
			switch (e->getEventType()) {
			case LOAD_LEVEL: {
				currentIndex++;
				loadLevel(static_cast<LoadLevelEvent*>(e)->getPath());
				es->markHandledBySubsystem(i, id);
				break;
			}
			case LOAD_GAME_VARIABLES: {
				gameVariables = static_cast<LoadGameVariablesEvent*>(e)->getPath();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case UNLOAD_LEVEL: {
				unloadLevel();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case RELOAD_LEVEL: {
				reloadLevel();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case WIN_EVENT: {
				currentIndex++;
				unloadLevel();
				std::string path = LEVEL_PATH + std::to_string(currentIndex) + "/";
				loadLevel(path);
				es->markHandledBySubsystem(i, id);
				break;
			}
			case GAME_OVER_EVENT: {
				reloadLevel();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case RESTART_GAME_EVENT: {
				currentIndex = 1;
				loadLevel(LEVEL_PATH + "1/");				
				es->markHandledBySubsystem(i, id);
				break;
			}
			case SPAWN_PLAYER_EVENT: {
				Player* object = static_cast<Player*>(static_cast<SpawnPlayer*>(e)->getGameObject());
				player = object;
				es->markHandledBySubsystem(i, id);
				break;
			}
			default: break;
			}
		}
	}
}

// sends spawn events for new objects based on read in data
void FileIO::loadLevel(std::string path) {
	if (fileExists(path + "GameVariables.csv")) {
		loadGameVariables(path + "GameVariables.csv");
	}

	currentLevel = path;

	path = path + "level-" + std::to_string(currentIndex) + ".csv";
	std::ifstream  data(path.c_str());

	if (fileExists(path)) {

		std::string line;
		std::vector<std::vector<std::string>> parsedCsv;
		while (std::getline(data, line))
		{
			std::stringstream lineStream(line);
			std::string cell;
			std::vector<std::string> parsedRow;
			while (std::getline(lineStream, cell, ','))	{
				parsedRow.push_back(cell);
			}

			parsedCsv.push_back(parsedRow);
		}

		for (int i = 1; i < parsedCsv.size(); i++) {
			std::vector<std::string> row = parsedCsv.at(i);
			GameObjectType objectType = stringtoGameObjectType(row.at(0));

			switch (objectType) {
			case CUBE: sendEvent(new SpawnEvent(new Cube(
				Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				std::stof(row.at(4)),
				Vector3(std::stof(row.at(5)), std::stof(row.at(6)), std::stof(row.at(7))),
				Quaternion::EulerAnglesToQuaternion(std::stof(row.at(8)), std::stof(row.at(9)), std::stof(row.at(10))),
				row.at(11),
				row.at(12)))); break;
			case FLAT_GROUND: sendEvent(new SpawnEvent(new FlatGround(
				Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				std::stof(row.at(4)),
				Vector3(std::stof(row.at(5)), std::stof(row.at(6)), std::stof(row.at(7))),
				Quaternion::EulerAnglesToQuaternion(std::stof(row.at(8)), std::stof(row.at(9)), std::stof(row.at(10))),
				row.at(11),
				row.at(12)))); break;
			case PLAYER: sendEvent(new SpawnPlayer(new Player(
				Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				std::stof(row.at(4)),
				Vector3(std::stof(row.at(5)), std::stof(row.at(6)), std::stof(row.at(7))),
				Quaternion::EulerAnglesToQuaternion(std::stof(row.at(8)), std::stof(row.at(9)), std::stof(row.at(10))),
				row.at(11),
				row.at(12)))); break;
			case RING: sendEvent(new SpawnEvent(new Ring(
				Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				std::stof(row.at(4)),
				Vector3(std::stof(row.at(5)), std::stof(row.at(6)), std::stof(row.at(7))),
				Quaternion::EulerAnglesToQuaternion(std::stof(row.at(8)), std::stof(row.at(9)), std::stof(row.at(10))),
				row.at(11),
				row.at(12)))); break;
			case OBSTACLE: sendEvent(new SpawnEvent(new Ring(
				Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				std::stof(row.at(4)),
				Vector3(std::stof(row.at(5)), std::stof(row.at(6)), std::stof(row.at(7))),
				Quaternion::EulerAnglesToQuaternion(std::stof(row.at(8)), std::stof(row.at(9)), std::stof(row.at(10))),
				row.at(11),
				row.at(12), OBSTACLE))); break;
			case QUAD: sendEvent(new SpawnEvent(new Quad(
				Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				std::stof(row.at(4)),
				Vector3(std::stof(row.at(5)), std::stof(row.at(6)), std::stof(row.at(7))),
				Quaternion::EulerAnglesToQuaternion(std::stof(row.at(8)), std::stof(row.at(9)), std::stof(row.at(10))),
				row.at(11),
				row.at(12), QUAD))); break;
			case CONTROLS: sendEvent(new SpawnEvent(new Quad(
				Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				std::stof(row.at(4)),
				Vector3(std::stof(row.at(5)), std::stof(row.at(6)), std::stof(row.at(7))),
				Quaternion::EulerAnglesToQuaternion(std::stof(row.at(8)), std::stof(row.at(9)), std::stof(row.at(10))),
				row.at(11),
				row.at(12), CONTROLS))); break;
			}
		}
	}
	else {
		sendEvent(new SpawnEvent(new Quad(player->position + Vector3(0, 10, -19), "end", "end", Vector3(100, 100, 100), Quaternion::EulerAnglesToQuaternion(90, 0, 0), END_SCREEN)));
		GameVariables::END = true;
	}

	data.close();
}

void FileIO::unloadLevel() {
	for (int i = 0; i < gameObjects->size(); i++) {
		sendEvent(new DestroyEvent(gameObjects->at(i)));
	}
	sendEvent(new DestroyEvent(player));
}

void FileIO::reloadLevel() {
	unloadLevel();
	loadLevel(currentLevel);
}

void FileIO::loadGameVariables(std::string path) {
	std::ifstream  data(path.c_str());

	std::string line;
	std::vector<std::vector<std::string>> parsedCsv;
	while (std::getline(data, line))
	{
		std::stringstream lineStream(line);
		std::string cell;
		std::vector<std::string> parsedRow;
		while (std::getline(lineStream, cell, ',')) {
			parsedRow.push_back(cell);
		}

		parsedCsv.push_back(parsedRow);
	}

	for (int i = 0; i < parsedCsv.size(); i++) {
		std::vector<std::string> row = parsedCsv.at(i);
		std::string variable = row.at(0);

		if (variable == "top") {
			sendEvent(new SpawnEvent(new WorldBound(Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				Vector3(0,-1,0),
				Quaternion::EulerAnglesToQuaternion(0,0,0))));

		}
		else if (variable == "bottom") {
			sendEvent(new SpawnEvent(new WorldBound(Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				Vector3(0, 1, 0),
				Quaternion::EulerAnglesToQuaternion(0, 0, 0))));
		}
		else if (variable == "front") {
			sendEvent(new SpawnEvent(new WorldBound(Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				Vector3(0, -1, 0),
				Quaternion::EulerAnglesToQuaternion(90, 0, 0))));
		}
		else if (variable == "back") {
			sendEvent(new SpawnEvent(new WorldBound(Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				Vector3(0, -1, 0),
				Quaternion::EulerAnglesToQuaternion(90, 180, 0))));
		}
		else if (variable == "left") {
			sendEvent(new SpawnEvent(new WorldBound(Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				Vector3(0, -1, 0),
				Quaternion::EulerAnglesToQuaternion(90, 90, 0))));
		}
		else if (variable == "right") {
			sendEvent(new SpawnEvent(new WorldBound(Vector3(std::stof(row.at(1)), std::stof(row.at(2)), std::stof(row.at(3))),
				Vector3(0, -1, 0),
				Quaternion::EulerAnglesToQuaternion(90, 270, 0))));
		}
		else if (variable == "hp") {
			GameVariables::INITIAL_HP = std::stoi(row.at(1));
		}
		else if (variable == "ring") {
			GameVariables::RING_SCORE = std::stoi(row.at(1));
		}
		else if (variable == "obstacle") {
			GameVariables::OBSTACLE_DAMAGE = std::stoi(row.at(1));
		}
		else if (variable == "needed") {
			GameVariables::NEEDED_SCORE = std::stoi(row.at(1));
		}
		else throw std::invalid_argument(std::string("invalid game variable: ") + variable);
	}
	data.close();
}

GameObjectType FileIO::stringtoGameObjectType(std::string s) {
	if (s == "CUBE") return CUBE;
	else if (s == "TERRAIN") return TERRAIN;
	else if (s == "FLAT_GROUND") return FLAT_GROUND;
	else if (s == "STARSHIP") return STARSHIP;
	else if (s == "PLAYER") return PLAYER;
	else if (s == "RING") return RING;
	else if (s == "QUAD") return QUAD;
	else if (s == "CONTROLS") return CONTROLS;
	else if (s == "OBSTACLE") return OBSTACLE;
	else throw std::invalid_argument(std::string("invalid game object type: ") + s);
}

bool FileIO::fileExists(std::string path) {
	std::ifstream f(path.c_str());
	return f.good();
}
