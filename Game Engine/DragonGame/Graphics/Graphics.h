#pragma once

#include "Renderer.h"
#include <EventSystem.h>
#include <Event.h>
#include <Subsystem.h>
#include <GameObject.h>
#include <unordered_map>
#include <string>

#include "gltext.h"

#define SHADERS "../Shaders/"
#define MESHES "../Assets/Meshes/"

// The graphics subsystem handles rendering the data to the screen
class Graphics: public Subsystem {
public:
	Graphics(int x, int y, EventSystem* es, std::vector<GameObject*>* objects);
	~Graphics();

	void handleEvents();

	void loadMeshes();
	void loadShaders();
	void update(float msec);
	void updateRenderObjects();
	void createRenderObject(GameObject* go);
	
	
private:
	Window* w;
	Renderer* r;
	float width;
	float height;
	float aspect;
	GLTtext* scoreText;
	GLTtext* hitpointsText;
	GLTtext* reloadText;
	GLTtext* nextText;
	GLTtext* restartText;

	vector<RenderObject*> toRender; // objects to render in 3D
	vector<RenderObject*> toRenderOrtho; // objects to render using the orthographic projection

	// All meshes, shaders, and textures are loaded at startup to minimise overhead. They are all deleted in the destructor.
	std::unordered_map<std::string, Mesh*> meshes;
	std::unordered_map<std::string, Shader*> shaders;
	std::unordered_map<std::string, GLuint> textures;
	std::unordered_map<RenderObject*, GameObject*> renderObjtoGameObj;
};

