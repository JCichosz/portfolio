#pragma once
#include "Graphics.h"
#include "Renderer.h"
#include <SpawnEvent.h>
#include <FlatGround.h>
#include <Starship.h>
#include <Player.h>
#include <SpawnPlayer.h>
#include <TimeEvent.h>
#include <SetScoreTextEvent.h>
#include <Quad.h>
#include <DestroyEvent.h>
#include <SetHitpointsTextEvent.h>
#include <Matrix4.h>

Graphics::Graphics(int x, int y, EventSystem* eventSys, std::vector<GameObject*>* objects) : Subsystem(eventSys, objects, GRAPHICS) {
	width = x;
	height = y;
	w = new Window(width, height);
	r = new Renderer(*w);
	aspect = (float) width / (float) height;
	r->SetProjectionMatrix(Matrix4::Perspective(1, 1000, aspect, 60.0f));
	loadMeshes();
	loadShaders();
	r->loadTextureToRender(textures);
	gltInit();
	scoreText = gltCreateText();
	hitpointsText = gltCreateText();
	reloadText = gltCreateText();
	nextText = gltCreateText();
	restartText = gltCreateText();

	gltSetText(restartText, "Press U/Dpad DOWN to replay the game");
	gltSetText(reloadText, "Press U/Dpad DOWN to reload");
	gltSetText(nextText, "Press U/Dpad DOWN to load next level");

	gltColor(1.0f, 1.0f, 1.0f, 1.0f);
}

// Delete all assets and clear maps
Graphics::~Graphics() {
	gltDeleteText(scoreText);
	gltDeleteText(hitpointsText);
	gltDeleteText(reloadText);
	gltDeleteText(nextText);
	gltDeleteText(restartText);

	gltTerminate();

	delete r;
	delete w;

	for (int i = 0; i < toRender.size(); i++) {
		delete toRender.at(i);
	}
	toRender.clear();

	for (int i = 0; i < toRenderOrtho.size(); i++) {
		delete toRenderOrtho.at(i);
	}
	toRenderOrtho.clear();

	for (auto pair : meshes) {
		delete pair.second;
	}
	meshes.clear();

	for (auto pair : shaders) {
		delete pair.second;
	}
	shaders.clear();

	textures.clear();

	renderObjtoGameObj.clear();
}

void Graphics::handleEvents() {
	for (int i = 0; i < es->getEventQueue().size(); i++) {
		Event* e = es->getEventQueue().at(i);

		if (needToHandleEvent(*e)) {
			switch (e->getEventType()) {
			case SPAWN_EVENT: {
				createRenderObject(static_cast<SpawnEvent*>(e)->getGameObject());
				es->markHandledBySubsystem(i, id);
				break;
			}
			case SPAWN_PLAYER_EVENT: {
				GameObject* go = static_cast<SpawnPlayer*>(e)->getGameObject();
				Player* object = static_cast<Player*>(go);
				createRenderObject(object);
				player = object;
				std::string text = "Health: " + std::to_string(player->getHitpoints());
				gltSetText(hitpointsText, text.c_str());
				es->markHandledBySubsystem(i, id);
				break;
			}
			case SET_SCORE_TEXT: {
				int score = static_cast<SetScoreTextEvent*>(e)->getScore();
				std::string text = "Score: " + std::to_string(score);
				gltSetText(scoreText, text.c_str());
				es->markHandledBySubsystem(i, id);
				break;
			}
			case SET_HITPOINTS_TEXT: {
				int hp = static_cast<SetHitpointsTextEvent*>(e)->getHitpoints();
				std::string text = "Health: " + std::to_string(hp);
				gltSetText(hitpointsText, text.c_str());
				es->markHandledBySubsystem(i, id);
				break;
			}
			case DESTROY_EVENT: {
				GameObject* object = static_cast<DestroyEvent*>(e)->getGameObject();
				for (auto pair : renderObjtoGameObj) {
					if (pair.second == object) {
						renderObjtoGameObj.erase(pair.first);
						for (int i = 0; i < toRender.size(); i++) {
							if (toRender.at(i) == pair.first) {
								toRender.erase(toRender.begin() + i);
							}
						}
						for (int i = 0; i < toRenderOrtho.size(); i++) {
							if (toRenderOrtho.at(i) == pair.first) {
								toRenderOrtho.erase(toRenderOrtho.begin() + i);
							}
						}
						delete pair.first;
						break;
					}
				}
				for (int i = 0; i < gameObjects->size(); i++) {
					if (gameObjects->at(i) == object) gameObjects->erase(gameObjects->begin() + i);
				}

				es->markHandledBySubsystem(i, id);
				break;
			}
			default: break;
			}
		}
	}
}

// main update function
void Graphics::update(float msec) {
	float start = timer.GetMS();
	w->UpdateWindow();
	handleEvents();
	updateRenderObjects();

	r->SetViewMatrix(Quaternion::EulerAnglesToQuaternion(0, 180, 0).ToMatrix() * Matrix4::Translation(Vector3(0, -10, 20)) * player->reverseYRotation().ToMatrix() * Matrix4::Translation(-player->position));

	r->UpdateScene(msec, toRender, toRenderOrtho);
	r->ClearBuffers();
	r->RenderScene(toRender, toRenderOrtho);

	gltDrawText2D(scoreText, 50, 10, 5);
	gltDrawText2D(hitpointsText, 1400, 10, 5);
	if(GameVariables::LOST) gltDrawText2D(reloadText, 525, 170, 4);
	if (GameVariables::WON) gltDrawText2D(nextText, 400, 170, 4);
	if (GameVariables::END) gltDrawText2D(restartText, 400, 170, 4);

	r->SwapBuffers();

	float end = timer.GetMS();
	float time = end - start;
	sendEvent(new TimeEvent(id, time));
}

// Updates the view matrices of objects after they have been moved by the physics system
void Graphics::updateRenderObjects() {
	for (int i = 0; i < toRender.size(); i++) {
		GameObject* go = renderObjtoGameObj[toRender.at(i)];
		toRender.at(i)->SetModelMatrix(Matrix4::Translation(go->position) * go->rotation.ToMatrix() * Matrix4::Scale(go->scale));
	}
	for (int i = 0; i < toRenderOrtho.size(); i++) {
		GameObject* go = renderObjtoGameObj[toRenderOrtho.at(i)];
		toRenderOrtho.at(i)->SetModelMatrix(player->zRotation().ToMatrix() * player->yRotation().ToMatrix() * Matrix4::Translation(player->position + Vector3(0, 10, 50)) * go->rotation.ToMatrix() * Matrix4::Scale(go->scale));
	}
}

// Creates a render object for new game objects
void Graphics::createRenderObject(GameObject* go) {
	RenderObject* o = new RenderObject(meshes[go->meshName], shaders[go->shaderName], textures[go->textureName]);
	o->SetModelMatrix(Matrix4::Translation(go->position) *  go->rotation.ToMatrix()* Matrix4::Scale(go->scale));
	o->setGameObject(go);
	if (go->type != WIN_SCREEN && go->type != LOSE_SCREEN && go->type != CONTROLS && go->type != END_SCREEN) toRender.push_back(o);
	else toRenderOrtho.push_back(o);

	renderObjtoGameObj.emplace(o, go);
}

// Load methods for preloading all graphical assets
void Graphics::loadMeshes() {
	Mesh* cube = Mesh::LoadMeshFile(MESHES"cube.asciimesh");
	meshes.emplace("cube", cube);

	Mesh* quad = Mesh::GenerateQuad();
	meshes.emplace("quad", quad);

	Mesh* starship = Mesh::GenerateStarship(Vector4(1, 0.54, 0, 1), Vector4(64, 0.7, 208, 1), Vector4(1, 0, 0.5, 1));
	meshes.emplace("starship", starship);

	Mesh* dragon = Mesh::loadObjFile(MESHES"Dragon.obj");
	meshes.emplace("dragon", dragon);

	Mesh* ring = Mesh::loadObjFile(MESHES"ring.obj");
	meshes.emplace("ring", ring);
}

void Graphics::loadShaders() {
	Shader* basic = new Shader(SHADERS"basicVert.glsl", SHADERS"basicFrag.glsl");
	shaders.emplace("basic", basic);

	Shader* dragon = new Shader(SHADERS"basicVert.glsl", SHADERS"dragonFrag.glsl");
	shaders.emplace("dragon", dragon);

	Shader* texture = new Shader(SHADERS"basicVert.glsl", SHADERS"basicTextureFrag.glsl");
	shaders.emplace("texture", texture);

	Shader* flatGround = new Shader(SHADERS"basicVert.glsl", SHADERS"flatGroundFrag.glsl");
	shaders.emplace("flatGround", flatGround);
	
	Shader* yellow = new Shader(SHADERS"basicVert.glsl", SHADERS"YellowFrag.glsl");
	shaders.emplace("yellow", yellow);

	Shader* controls = new Shader(SHADERS"orthoShader.glsl", SHADERS"ControlsFrag.glsl");
	shaders.emplace("controls", controls);

	Shader* winner = new Shader(SHADERS"orthoShader.glsl", SHADERS"winnerFrag.glsl");
	shaders.emplace("winner", winner);

	Shader* lost = new Shader(SHADERS"orthoShader.glsl", SHADERS"lostFrag.glsl");
	shaders.emplace("lost", lost);

	Shader* red = new Shader(SHADERS"basicVert.glsl", SHADERS"RedFrag.glsl");
	shaders.emplace("red", red);

	Shader* sky = new Shader(SHADERS"basicVert.glsl", SHADERS"SkyFrag.glsl");
	shaders.emplace("sky", sky);

	Shader* end = new Shader(SHADERS"basicVert.glsl", SHADERS"EndFrag.glsl");
	shaders.emplace("end", end);

}