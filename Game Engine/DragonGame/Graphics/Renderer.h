#pragma once
#include "../Middleware/nclgl/OGLRenderer.h"

#include "RenderObject.h"

#include <vector>
#include <unordered_map>
using std::vector;

class Renderer : public OGLRenderer	{
public:
	Renderer(Window &parent);
	~Renderer(void);

	virtual void	Render(const RenderObject &o);

	GLuint LoadTexture(const string& filename);

	// CSC3224 NCODE Justyna Cichosz 140449594
	void RenderScene() {};

	virtual void	RenderScene(vector<RenderObject*> toRender, vector<RenderObject*> toRenderOrtho);

	virtual void	UpdateScene(float msec, vector<RenderObject*> toRender, vector<RenderObject*> toRenderOrtho);

	void loadTextureToRender(std::unordered_map<std::string, GLuint>& textures);
	// CSC3224 NCODE BLOCK ENDS
};

