#include "Renderer.h"

Renderer::Renderer(Window &parent) : OGLRenderer(parent) {

	glEnable(GL_DEPTH_TEST);
	// CSC3224 NCODE Justyna Cichosz 140449594
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// CSC3224 NCODE BLOCK ENDS

	orthoProjection = Matrix4::Orthographic(-1.0f, 10000.0f, width / 2.0f, -width / 2.0f, height / 2.0f, -height / 2.0f);
}

Renderer::~Renderer(void) {

}

void	Renderer::Render(const RenderObject &o) {
	modelMatrix = o.GetWorldTransform();

	if (o.GetShader() && o.GetMesh()) {
		GLuint program = o.GetShader()->GetShaderProgram();

		glUseProgram(program);

		UpdateShaderMatrices(program);

		o.Draw();
	}

	for (vector<RenderObject*>::const_iterator i = o.GetChildren().begin(); i != o.GetChildren().end(); ++i) {
		Render(*(*i));
	}
}

GLuint Renderer::LoadTexture(const string& filename) {
	GLuint texName = SOIL_load_OGL_texture(filename.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);

	if (texName == 0) {
		cout << "Texture file " << filename << " failed to load!" << std::endl;
	}

	return texName;
}


// CSC3224 NCODE Justyna Cichosz 140449594
void	Renderer::RenderScene(vector<RenderObject*> toRender, vector<RenderObject*> toRenderOrtho) {
	for (vector<RenderObject*>::iterator i = toRender.begin(); i != toRender.end(); ++i) {
		Render(*(*i));
	}
	for (vector<RenderObject*>::iterator i = toRenderOrtho.begin(); i != toRenderOrtho.end(); ++i) {
		Render(*(*i));
	}
}

void	Renderer::UpdateScene(float msec, vector<RenderObject*> toRender, vector<RenderObject*> toRenderOrtho) {
	for (vector<RenderObject*>::iterator i = toRender.begin(); i != toRender.end(); ++i) {
		(*i)->Update(msec);
	}
	for (vector<RenderObject*>::iterator i = toRenderOrtho.begin(); i != toRenderOrtho.end(); ++i) {
		(*i)->Update(msec);
	}
}

#define TEXTURES "../Assets/Textures/"

void Renderer::loadTextureToRender(std::unordered_map<std::string, GLuint>& textures) {

	glActiveTexture(GL_TEXTURE1);
	GLuint dragonTex = LoadTexture(TEXTURES"Dragon_ground_color.png");
	textures.emplace("dragon", dragonTex);
	glBindTexture(GL_TEXTURE_2D, dragonTex);

	glActiveTexture(GL_TEXTURE2);
	GLuint grassTex = LoadTexture(TEXTURES"grass-sketchuptextureclub.png");
	textures.emplace("grass", grassTex);
	glBindTexture(GL_TEXTURE_2D, grassTex);

	glActiveTexture(GL_TEXTURE3);
	GLuint controlsTex = LoadTexture(TEXTURES"controls.png");
	textures.emplace("controls", controlsTex);
	glBindTexture(GL_TEXTURE_2D, controlsTex);

	glActiveTexture(GL_TEXTURE4);
	GLuint winnerTex = LoadTexture(TEXTURES"winner.png");
	textures.emplace("winner", winnerTex);
	glBindTexture(GL_TEXTURE_2D, winnerTex);

	glActiveTexture(GL_TEXTURE5);
	GLuint lostTex = LoadTexture(TEXTURES"lost.png");
	textures.emplace("lost", lostTex);
	glBindTexture(GL_TEXTURE_2D, lostTex);

	glActiveTexture(GL_TEXTURE6);
	GLuint skyTex = LoadTexture(TEXTURES"sky.png");
	textures.emplace("sky", skyTex);
	glBindTexture(GL_TEXTURE_2D, skyTex);

	glActiveTexture(GL_TEXTURE7);
	GLuint endTex = LoadTexture(TEXTURES"end.png");
	textures.emplace("end", endTex);
	glBindTexture(GL_TEXTURE_2D, endTex);

	// Set active texture for glText bitmap
	glActiveTexture(GL_TEXTURE0);
}
// CSC3224 NCODE BLOCK ENDS