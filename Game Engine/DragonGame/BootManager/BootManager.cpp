#include "BootManager.h"
#include <LoadLevelEvent.h>
#include <ResourceManager.h>
#include <LoadGameVariablesEvent.h>

BootManager::BootManager(EventSystem * es, std::vector<GameObject*>* objects) : Subsystem(es, objects, BOOT_MANAGER), turnOff(false) {
}

BootManager::~BootManager() {
}

void BootManager::start() {
	ResourceManager::initialise();
	sendEvent(new LoadLevelEvent("../Files/level-1/"));
}

void BootManager::shutdown() {
	es->clearQueue();
	delete es;

	for (int i = 0; i < gameObjects->size(); i++) {
		delete gameObjects->at(i);
	}
	delete player;

	gameObjects->clear();
	delete gameObjects;

	ResourceManager::destroy();

	turnOff = true;
}

void BootManager::handleEvents() {
	for (int i = 0; i < es->getEventQueue().size(); i++) {
		Event* e = es->getEventQueue().at(i);

		if (needToHandleEvent(*e)) {
			switch (e->getEventType()) {
			case SHUTDOWN: {
				shutdown();
				break;
			}
			default: break;
			}
		}
	}
}
