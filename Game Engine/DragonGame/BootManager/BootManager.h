#pragma once
#include <EventSystem.h>


// Loads and destroys data for clean shutdown
class BootManager: public Subsystem {

public:
	BootManager(EventSystem* eventSys, std::vector<GameObject*>* objects);
	~BootManager();

	void start();
	void shutdown();
	void handleEvents();
	bool doShutdown() { return turnOff; }

private:
	bool turnOff;
};

