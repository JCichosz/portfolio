#pragma once
#include "Event.h"
class LoadLevelEvent: public Event {
public:
	LoadLevelEvent(std::string s);
	~LoadLevelEvent();

	std::string getPath() { return path; }

private:
	std::string path;
};

