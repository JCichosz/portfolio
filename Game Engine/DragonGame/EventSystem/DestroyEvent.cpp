#include "DestroyEvent.h"

DestroyEvent::DestroyEvent(GameObject * go): Event(DESTROY_EVENT) {
	object = go;
	toHandle.push_back(GRAPHICS);
	if(go->collides()) 	toHandle.push_back(PHYSICS);
}

DestroyEvent::~DestroyEvent() {

}
