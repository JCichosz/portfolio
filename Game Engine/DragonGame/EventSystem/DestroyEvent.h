#pragma once
#include "Event.h"
#include <GameObject.h>
class DestroyEvent : public Event {
public:
	DestroyEvent(GameObject* go);
	~DestroyEvent();

	GameObject* getGameObject() { return object; }

private:
	GameObject* object;
};

