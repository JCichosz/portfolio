#include "SpawnPlayer.h"

SpawnPlayer::SpawnPlayer(Player* go) : Event(SPAWN_PLAYER_EVENT) {
	object = go;
	toHandle.push_back(PHYSICS);
	toHandle.push_back(GRAPHICS);
	toHandle.push_back(KEYBOARD_CONTROLLER);
	toHandle.push_back(GAMEPAD_CONTROLLER);
	toHandle.push_back(GAME_LOGIC);
	toHandle.push_back(FILE_IO);
}


SpawnPlayer::~SpawnPlayer() {
}
