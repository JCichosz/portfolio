#pragma once
#include "Event.h"
#include <Cube.h>

class SpawnEvent: public Event {
	
public:
	SpawnEvent(GameObject* go = new Cube());
	~SpawnEvent();
	GameObject* getGameObject() { return object; }

private:
	GameObject* object;
};

