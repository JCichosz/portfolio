#include "SpawnEvent.h"


 SpawnEvent::SpawnEvent(GameObject* go): Event(SPAWN_EVENT) {
	 object = go;
	 toHandle.push_back(GRAPHICS);
	 if (go->collides()) toHandle.push_back(PHYSICS);
}

SpawnEvent::~SpawnEvent() {
}
