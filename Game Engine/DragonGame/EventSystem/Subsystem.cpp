#include "Subsystem.h"
#include "EventSystem.h"
#include "SpawnEvent.h"

Subsystem::Subsystem(EventSystem* e, SubsystemID sys) :
	es(e), id(sys) {
	timer = GameTimer();
}

Subsystem::Subsystem(EventSystem* e, std::vector<GameObject*>* objects, SubsystemID sys): 
	es(e), gameObjects(objects), id(sys) {
	timer = GameTimer();
}


Subsystem::~Subsystem() {
}

void Subsystem::sendEvent(Event* e) {
	if (dynamic_cast<SpawnEvent*>(e)) {
		SpawnEvent* se = static_cast<SpawnEvent*>(e);
		gameObjects->push_back(se->getGameObject());

		if (se->getGameObject()->type == PLAYER) player = static_cast<Player*>(se->getGameObject());
	}
	es->addEvent(e);
}

bool Subsystem::needToHandleEvent(Event e) {
	std::vector<SubsystemID> toHandle = e.getSystemsToHandle();
	if (std::find(toHandle.begin(), toHandle.end(), id) != toHandle.end()) {
		return true;
	}
	else return false;
}
