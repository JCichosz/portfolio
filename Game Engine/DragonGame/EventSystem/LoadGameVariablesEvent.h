#pragma once
#include "Event.h"
class LoadGameVariablesEvent: public Event {

public:
	LoadGameVariablesEvent(std::string s);
	~LoadGameVariablesEvent();

	std::string getPath() { return path; }

private:
	std::string path;
};

