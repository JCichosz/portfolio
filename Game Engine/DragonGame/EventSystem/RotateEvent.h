#pragma once
#include "Event.h"
#include <GameObject.h>

class RotateEvent:	public Event {
public:
	RotateEvent(GameObject* obj, Vector3 dir);
	~RotateEvent();

	Vector3 getDirection() { return direction; }
	GameObject* getGameObject() { return go; }

private:
	GameObject* go;
	Vector3 direction;
};

