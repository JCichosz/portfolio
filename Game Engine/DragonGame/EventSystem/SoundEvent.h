#pragma once
#include "Event.h"
#include <string>
class SoundEvent:	public Event {
public:
	SoundEvent(std::string s, Vector3 pos = Vector3(0,0,0));
	~SoundEvent();
	std::string getName() { return name; }
	Vector3 getPosition() { return position; }

private:
	std::string name;
	Vector3 position;
};

