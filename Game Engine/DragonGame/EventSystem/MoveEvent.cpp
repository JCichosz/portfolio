#include "MoveEvent.h"

MoveEvent::MoveEvent(GameObject* obj, Vector3 dir): Event(MOVEMENT_EVENT) {
	go = obj;
	direction = dir;
	toHandle.push_back(PHYSICS);
}


MoveEvent::~MoveEvent() {

}
