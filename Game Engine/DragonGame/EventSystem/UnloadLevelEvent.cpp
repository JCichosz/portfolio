#include "UnloadLevelEvent.h"

UnloadLevelEvent::UnloadLevelEvent(std::string s): Event(UNLOAD_LEVEL) {
	path = s;
	toHandle.push_back(FILE_IO);
}


UnloadLevelEvent::~UnloadLevelEvent() {
}
