#pragma once
#include <vector>
#include <Vector3.h>

#pragma once

enum EventType {
	MOVEMENT_EVENT,
	SPAWN_EVENT,
	SPAWN_PLAYER_EVENT,
	SOUND_EVENT,
	JUMP_EVENT,
	DESTROY_EVENT,
	TIMER_EVENT,
	TOGGLE_PROFILER_EVENT,
	INCREASE_SCORE,
	LOAD_LEVEL,
	UNLOAD_LEVEL,
	RELOAD_LEVEL,
	RESTART_GAME_EVENT,
	LOAD_GAME_VARIABLES,
	SET_SCORE_TEXT,
	SET_HITPOINTS_TEXT,
	WIN_EVENT,
	GAME_OVER_EVENT,
	DAMAGE_PLAYER,
	SHUTDOWN,
	ROTATE_EVENT
};

enum SubsystemID {
	GRAPHICS,
	PHYSICS,
	AUDIO,
	BOOT_MANAGER,
	FILE_IO,
	PROFILER,
	RESOURCE_MANAGER,
	KEYBOARD_CONTROLLER,
	GAMEPAD_CONTROLLER,
	GAME_LOGIC
};

class Event {
public:

	Event(EventType type);
	virtual ~Event();

	void* operator new(size_t s);
	void operator delete(void* p);

	EventType getEventType() { return type; }
	void removeSubsystemFromVector(SubsystemID);
	std::vector<SubsystemID> getSystemsToHandle() { return toHandle; }

protected:
	EventType type;
	std::vector<SubsystemID> toHandle;
};

