#pragma once
#include <queue>
#include <vector>
#include "Event.h"
#include "Subsystem.h"

// Has a queue of events each subsystem reads ato see if it needs to handls
class EventSystem {
public:
	EventSystem();
	~EventSystem();

	void addEvent(Event* e);
	void deleteEvent(int index);
	void clearHandledEvents();
	void markHandledBySubsystem(int index, SubsystemID s);
	void clearQueue();
	std::vector<Event*> getEventQueue() const { return eventQueue; }

private:
	std::vector<Event*> eventQueue;
};

