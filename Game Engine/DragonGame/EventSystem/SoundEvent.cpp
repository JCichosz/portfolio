#include "SoundEvent.h"

SoundEvent::SoundEvent(std::string s, Vector3 pos): Event(SOUND_EVENT) {
	name = s;
	position = pos;
	toHandle.push_back(AUDIO);
}

SoundEvent::~SoundEvent() {
}
