#include "ScoreIncreaseEvent.h"

ScoreIncreaseEvent::ScoreIncreaseEvent(int num): Event(INCREASE_SCORE) {
	amount = num;
	toHandle.push_back(GAME_LOGIC);
}


ScoreIncreaseEvent::~ScoreIncreaseEvent() {
}
