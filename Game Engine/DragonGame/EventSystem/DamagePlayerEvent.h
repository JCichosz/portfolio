#pragma once
#include "Event.h"
class DamagePlayerEvent: public Event {
public:
	DamagePlayerEvent(int amount);
	~DamagePlayerEvent();

	int getAmount() { return amount; }

private:
	int amount;
};

