#include "DamagePlayerEvent.h"


DamagePlayerEvent::DamagePlayerEvent(int num): Event(DAMAGE_PLAYER) {
	amount = num;
	toHandle.push_back(GAME_LOGIC);
}

DamagePlayerEvent::~DamagePlayerEvent() {
}
