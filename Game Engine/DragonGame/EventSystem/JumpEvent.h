#pragma once
#include "Event.h"
#include <GameObject.h>

class JumpEvent : public Event {

public:
	JumpEvent(GameObject* obj);
	~JumpEvent();
	GameObject* getGameObject() { return go; }
	Vector3 getDirection() { return direction; }

private:
	GameObject * go;
	Vector3 direction;
};

