#pragma once
#include "Event.h"
class SetHitpointsTextEvent: public Event {
public:
	SetHitpointsTextEvent(int amount);
	~SetHitpointsTextEvent();

	int getHitpoints() { return hitpoints; }

private:
	int hitpoints;
};

