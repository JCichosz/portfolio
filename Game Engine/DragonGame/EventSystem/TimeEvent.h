#pragma once
#include "Event.h"
class TimeEvent: public Event {
public:
	TimeEvent(SubsystemID sys, float t);
	~TimeEvent();
	SubsystemID getId() { return id; }
	float getTime() { return time; }

private:
	SubsystemID id;
	float time;

};

