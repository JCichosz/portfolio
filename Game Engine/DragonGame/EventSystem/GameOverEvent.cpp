#include "GameOverEvent.h"

GameOverEvent::GameOverEvent(): Event(GAME_OVER_EVENT) {
	toHandle.push_back(GRAPHICS);
	toHandle.push_back(FILE_IO);
	toHandle.push_back(GAME_LOGIC);

}


GameOverEvent::~GameOverEvent() {
}
