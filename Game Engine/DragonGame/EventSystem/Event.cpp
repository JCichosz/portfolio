#include "Event.h"
#include <algorithm>
#include <ResourceManager.h>

Event::Event(EventType t) {
	type = t;
}

void* Event::operator new(size_t s) {
	return ResourceManager::newEvent();
}
void Event::operator delete(void* p) {
	ResourceManager::deleteEvent(p);
}

Event::~Event() {
	toHandle.clear();
}

void Event::removeSubsystemFromVector(SubsystemID s) {
	toHandle.erase(std::remove(toHandle.begin(), toHandle.end(), s), toHandle.end());
}
