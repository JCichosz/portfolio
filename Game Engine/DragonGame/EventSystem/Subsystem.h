#pragma once
#include "Event.h"
#include <vector>
#include <GameObject.h>
#include <GameTimer.h>
#include <Player.h>

class EventSystem;

class Subsystem {
public:
	Subsystem(EventSystem* es, SubsystemID id);
	Subsystem(EventSystem* es, std::vector<GameObject*>* objects, SubsystemID id);
	~Subsystem();

	virtual void handleEvents() = 0;
	void sendEvent(Event* e);
	bool needToHandleEvent(Event e);
	SubsystemID getId() { return id; }
	float getUpdateTime() { return updateTime; }

protected: 
	EventSystem* es;
	Player* player;
	GameTimer timer;
	std::vector<GameObject*>* gameObjects;
	SubsystemID id;
	float updateTime;
};

