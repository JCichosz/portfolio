#include "GameWonEvent.h"


GameWonEvent::GameWonEvent(): Event(WIN_EVENT) {
	toHandle.push_back(GRAPHICS);
	toHandle.push_back(FILE_IO);
	toHandle.push_back(GAME_LOGIC);
}


GameWonEvent::~GameWonEvent() {
}
