#pragma once
#include "SpawnEvent.h"
#include <Player.h>

class SpawnPlayer:	public Event {
public:
	SpawnPlayer(Player* go = new Player());
	~SpawnPlayer();
	GameObject* getGameObject() { return object; }

private:
	GameObject* object;
};

