#pragma once
#include "Event.h"
class ScoreIncreaseEvent: public Event {
public:
	ScoreIncreaseEvent(int num);
	~ScoreIncreaseEvent();
	int getAmount() { return amount; }

private:
	int amount;
};

