#include "TimeEvent.h"



TimeEvent::TimeEvent(SubsystemID sys, float t): Event(TIMER_EVENT) {
	id = sys;
	time = t;
	toHandle.push_back(PROFILER);
}


TimeEvent::~TimeEvent() {
}
