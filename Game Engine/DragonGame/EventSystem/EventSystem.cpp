#include "EventSystem.h"

EventSystem::EventSystem() {
}


EventSystem::~EventSystem() {

}

void EventSystem::addEvent(Event* e) {
	eventQueue.push_back(e);
}

void EventSystem::deleteEvent(int index) {
	
	eventQueue.erase(eventQueue.begin() + index);
}

void EventSystem::clearQueue() {
	for (int i = 0; i < eventQueue.size(); i++) {
		delete eventQueue.at(i);
	}
	eventQueue.clear();
}

void EventSystem::clearHandledEvents() {
	for (int i = 0; i < eventQueue.size(); i++) {
		if (eventQueue.at(i)->getSystemsToHandle().size() == 0) {
			delete eventQueue.at(i);
			eventQueue.erase(eventQueue.begin() + i);
			--i;
		}
	}
}

void EventSystem::markHandledBySubsystem(int index, SubsystemID s) {
	eventQueue.at(index)->removeSubsystemFromVector(s);
}