#include "SetHitpointsTextEvent.h"


SetHitpointsTextEvent::SetHitpointsTextEvent(int amount): Event(SET_HITPOINTS_TEXT) {
	hitpoints = amount;
	toHandle.push_back(GRAPHICS);
}

SetHitpointsTextEvent::~SetHitpointsTextEvent()
{
}
