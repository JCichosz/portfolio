#pragma once
#include "Event.h"
#include <GameObject.h>

class MoveEvent : public Event {
public:
	//MoveEvent(GameObject* obj);
	MoveEvent(GameObject* obj, Vector3 dir);
	~MoveEvent();
	Vector3 getDirection() { return direction; }
	GameObject* getGameObject() { return go; }

private:
	GameObject* go;
	Vector3 direction;
};

