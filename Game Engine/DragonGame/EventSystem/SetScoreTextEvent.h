#pragma once
#include "Event.h"
class SetScoreTextEvent: public Event {
public:
	SetScoreTextEvent(int num);
	~SetScoreTextEvent();

	int getScore() { return score; }

private:
	int score;
};

