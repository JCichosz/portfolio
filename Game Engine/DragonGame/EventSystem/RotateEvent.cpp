#include "RotateEvent.h"



RotateEvent::RotateEvent(GameObject* obj, Vector3 dir): Event(ROTATE_EVENT) {
	go = obj;
	direction = dir;
	toHandle.push_back(PHYSICS);
}


RotateEvent::~RotateEvent() {
}
