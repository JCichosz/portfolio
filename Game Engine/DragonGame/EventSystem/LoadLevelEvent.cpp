#include "LoadLevelEvent.h"



LoadLevelEvent::LoadLevelEvent(std::string s): Event(LOAD_LEVEL) {
	path = s;
	toHandle.push_back(FILE_IO);
	toHandle.push_back(GAME_LOGIC);
}


LoadLevelEvent::~LoadLevelEvent() {
}
