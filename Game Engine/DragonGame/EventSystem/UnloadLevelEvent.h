#pragma once
#include "Event.h"
class UnloadLevelEvent: public Event {
public:
	UnloadLevelEvent(std::string s);
	~UnloadLevelEvent();
	std::string getPath() { return path; }

private:
	std::string path;
};

