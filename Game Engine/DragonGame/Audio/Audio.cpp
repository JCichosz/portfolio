#include "Audio.h"
#include <SoundEvent.h>
#include <TimeEvent.h>

#define DISTANCEFACTOR 1.0f

Audio::Audio(EventSystem* eventSys, std::vector<GameObject*>* objects) : Subsystem(eventSys, objects, AUDIO) {
	result = FMOD::System_Create(&system);

	checkResult(result);

	result = system->init(10, FMOD_INIT_NORMAL, NULL);

	checkResult(result);

	result = system->set3DSettings(1.0, DISTANCEFACTOR, 1.0f);

	checkResult(result);

	loadSounds();
	playSound("background", true);
}

void Audio::checkResult(FMOD_RESULT res) {
	if (res != FMOD_OK) {
		std::cout << "An audio error has occured";
		return;
	}
}

Audio::~Audio() {
	for (auto pair : sounds) {
		pair.second->release();
		checkResult(result);
	}
	result = system->close();
	checkResult(result);
	result = system->release();
	checkResult(result);
}

void Audio::update() {
	float start = timer.GetMS();
	handleEvents();
	system->update();
	float end = timer.GetMS();
	float time = end - start;
	sendEvent(new TimeEvent(id, time));
}

void Audio::handleEvents() {
	for (int i = 0; i < es->getEventQueue().size(); i++) {
		Event* e = es->getEventQueue().at(i);

		if (needToHandleEvent(*e)) {
			switch (e->getEventType()) {
			case SOUND_EVENT: {
 				SoundEvent* se = static_cast<SoundEvent*>(e);
				std::string name = se->getName();
				Vector3 pos = se->getPosition();

				play3DSound(name, pos);

				es->markHandledBySubsystem(i, id);
				break;
			}
			default: break;
			}
		}
	}
}

void Audio::loadSounds() {
	FMOD::Sound* sound1;
	system->createSound(SOUNDS"Kaze_guard_unacceptable.ogx", FMOD_3D, 0, &sound1);
	sound1->set3DMinMaxDistance(0.5f * DISTANCEFACTOR, 5000.0f * DISTANCEFACTOR);
	sounds.emplace("unacceptable", sound1);

	FMOD::Sound* sound2;
	system->createSound(SOUNDS"FEH_bgm_menu_post.oga", FMOD_3D, 0, &sound2);
	sound2->set3DMinMaxDistance(0.5f * DISTANCEFACTOR, 5000.0f * DISTANCEFACTOR);
	sounds.emplace("background", sound2);

	FMOD::Sound* sound3;
	system->createSound(SOUNDS"Sonic-Ring.mp3", FMOD_3D, 0, &sound3);
	sound2->set3DMinMaxDistance(0.5f * DISTANCEFACTOR, 5000.0f * DISTANCEFACTOR);
	sounds.emplace("sonic", sound3);
}

void Audio::playSound(std::string s, bool loop) {
	if (!loop)	sounds[s]->setMode(FMOD_LOOP_OFF);
	else {
		sounds[s]->setMode(FMOD_LOOP_NORMAL);
		sounds[s]->setLoopCount(-1);
	}
	system->playSound(sounds[s], 0, false, &channel);
}

void Audio::play3DSound(std::string s, Vector3 pos, bool loop) {
	if (!loop)	sounds[s]->setMode(FMOD_LOOP_OFF);
	else {
		sounds[s]->setMode(FMOD_LOOP_NORMAL);
		sounds[s]->setLoopCount(-1);
	}

	FMOD_VECTOR position = { pos.x, pos.y, pos.z };
	FMOD_VECTOR vel = { 0.0f, 0.0f, 0.0f };

	system->playSound(sounds[s], 0, true, &channel);
	channel->set3DAttributes(&position, &vel);

	channel->setPaused(false);
}
