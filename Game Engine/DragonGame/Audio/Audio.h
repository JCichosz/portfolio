#pragma once
#include <fmod_studio.hpp>
#include <fmod.hpp>
#include <EventSystem.h>
#include <Subsystem.h>
#include <unordered_map>
#include <Vector3.h>
#define SOUNDS "../Assets/Sounds/"

class Audio : public Subsystem {

public:
	Audio(EventSystem* eventSys, std::vector<GameObject*>* objects);
	~Audio();

	void update();
	void handleEvents();
	void loadSounds();
	void playSound(std::string s, bool loop = false);
	void play3DSound(std::string s, Vector3 pos, bool loop = false);

private:
	FMOD_RESULT result;
	FMOD::System* system;
	FMOD::Channel* channel = 0;
	std::unordered_map<std::string, FMOD::Sound*> sounds;

	void checkResult(FMOD_RESULT res);
};

