#pragma once
#include <unordered_map>
#include <vector>


/* Hnadles memory allocation and deallocation for a Storage of a given size and number */
class Storage {

	/* A memory chunk is characterised by the bin number it lives in 
	and the index within that bin. 
	It also has a boolean to indicate whether the memory chunk is free. */
	struct Chunk {
		int bin;
		int index;
		bool free;
	};

public:
	Storage(size_t size, int number);
	~Storage();

	// Add and 'erase' memory.
	void* add();
	void erase(void* p);

private:
	std::vector<char*> memory;	// A vector of bins (char*) of allocated mamory
	size_t size;				// size of the data type
	int max;					// maximum number of objects in a bin
	int currentBin;				// index of currently used gin
	int numBins;				// number of created bins
	int nextFreeBin;			// index of next free bin
	int nextFreeChunk;			// index of next free chunk within a bun
	std::unordered_map<char*, Chunk> chunks; // map of pointers to chunk struck to retrieve information about the memory chunk
};

