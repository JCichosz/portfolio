#pragma once
#include "Storage.h"

/* Initialises and destroys starage used for game objets and events */
 class ResourceManager {

public:
	static void* newGameObject();
	static void deleteGameObject(void* p);
	static void* newEvent();
	static void deleteEvent(void* p);
	static void initialise();
	static void destroy();

private:
	static Storage* gameObjectStorage;
	static Storage* eventStorage;
};

