#pragma once
#include "ResourceManager.h"
#include <Cube.h>
#include <SoundEvent.h>
#include <Player.h>
#include <LoadLevelEvent.h>
#include <WorldBound.h>

// Declaration of starage objects to be created
Storage* ResourceManager::gameObjectStorage;
Storage* ResourceManager::eventStorage;


void* ResourceManager::newGameObject() {
	return gameObjectStorage->add();
}
void ResourceManager::deleteGameObject(void* p) {
	gameObjectStorage->erase(p);
}

void* ResourceManager::newEvent() {
	return eventStorage->add();
}

void ResourceManager::deleteEvent(void * p) {
	eventStorage->erase(p);
}

// Initialises the Storage objects
void ResourceManager::initialise() {
	gameObjectStorage = new Storage(sizeof(WorldBound), 100);
	eventStorage = new Storage(sizeof(SoundEvent), 200);
}

// Delete created Storage objects to free memory
void ResourceManager::destroy() {
	delete eventStorage;
	delete gameObjectStorage;
}
