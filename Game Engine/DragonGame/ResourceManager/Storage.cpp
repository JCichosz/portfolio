#include "Storage.h"
#include <stdlib.h>
#include <malloc.h>

Storage::Storage(size_t s, int number) {
	size = s;
	max = number;
	numBins = 1;
	currentBin = 0;
	memory.push_back( (char*)malloc(max * size));

	nextFreeBin = 0;
	nextFreeChunk = 0;

	// populate the created bin with free memory chunks
	for (int i = 0; i < max; i++) {
		Chunk temp = { currentBin, i, true };
		chunks.emplace(memory.at(currentBin) + (i * size), temp);
	}
}

Storage::~Storage() {
	// free all allocated mamory
	for (int i = 0; i < numBins; i++) {
		free(memory.at(i));
	}
}

// Returns the pointer to next free chunk of memory.
// If all chunks in all bins are full - create a new bin.
void* Storage::add() {
	char* ptr = nullptr;
	bool done = false;
	while (!done) {
		if (nextFreeChunk < max - 1) {
			ptr = memory.at(currentBin) + (nextFreeChunk * size);
			if (chunks[ptr].free == true) {
				chunks[ptr].free = false;
				done = true;
			}
			else {
				nextFreeChunk++;
				continue;
			}
		}		
		else {
			memory.push_back((char*)malloc(max * size));
			currentBin++;
			numBins++;
			nextFreeChunk = 0;
			ptr = memory.at(currentBin) + (nextFreeChunk * size);
			for (int i = 0; i < max; i++) {
				Chunk temp = { currentBin, i, true };
				chunks.emplace(memory.at(currentBin) + (i * size), temp);
			}
		}
	}	
	return (void*) ptr;
}

// 'Erases' data for a chunk. Sets the flag to 'free' so the memory chunk can be overriden by a new object.
// If index is smaller that currently set as nest free chunk - set the freed chunk as next free chunk.
void Storage::erase(void * p) {
	Chunk& ch = chunks[(char*) p];
	ch.free = true;
	if (nextFreeBin > ch.bin) {
		nextFreeBin = ch.index;
		nextFreeChunk = ch.index;
	}
	else if (nextFreeChunk > ch.index) nextFreeChunk = ch.index;
}
