#pragma once
#include <Keyboard.h>
#include <Mouse.h>
#include "Controller.h"

// Handles input from the keyboard
class KeyboardController: public Controller {
public:
	KeyboardController(EventSystem* es, std::vector<GameObject*>* objects);
	~KeyboardController();
	void update();
	void listenForInput();
};

