#pragma once
#include "Controller.h"
#include <Windows.h>
#include <Xinput.h>

// Hnaldes input from a gamepad
class GamepadController: public Controller {
public:
	GamepadController(int player, EventSystem* es, std::vector<GameObject*>* objects);
	~GamepadController();

	void listenForInput();
	void updateState();
	bool isConnected();
	void update();

private:
	XINPUT_STATE state;
	int playerNumber;
	float deadzoneX;
	float deadzoneY;
	bool leftStickInDeadzone();
	bool isPressed(WORD button);

	bool yPressed;
	bool aPressed;
	bool xPressed;
	bool startPressed;
	bool backPressed;
	bool dPadDownPressed;
};

