#include "GamepadController.h"
#include <SpawnPlayer.h>
#include <SoundEvent.h>
#include <ToggleProfilerEvent.h>
#include <TimeEvent.h>
#include <DestroyEvent.h>
#include <ShutdownEvent.h>
#include <UnloadLevelEvent.h>
#include <ReloadLevelEvent.h>
#include <RotateEvent.h>
#include <GameOverEvent.h>
#include <GameWonEvent.h>
#include <RestartGameEvent.h>

GamepadController::GamepadController(int player, EventSystem* es, std::vector<GameObject*>* objects):
	Controller(es, objects, GAMEPAD_CONTROLLER), playerNumber(player), deadzoneX(0.05f), deadzoneY(0.02f) {
}

GamepadController::~GamepadController() {
}

// listen for player input and issue appropriate events
void GamepadController::listenForInput() {
	if (isConnected()) {
		updateState();
		bool move = false;
		//bool rotate = false;

		Vector3 rotation = Vector3(0, 0, 0);
		Vector3 movement = Vector3(0, 0, 0);

		if (isPressed(XINPUT_GAMEPAD_BACK) && !backPressed) sendEvent(new ShutdownEvent());

		if (GameVariables::WON && isPressed(XINPUT_GAMEPAD_DPAD_DOWN) && !dPadDownPressed) {
			dPadDownPressed = true;

			sendEvent(new GameWonEvent());
			GameVariables::WON = false;
		}
		if (GameVariables::LOST && isPressed(XINPUT_GAMEPAD_DPAD_DOWN) && !dPadDownPressed) {
			dPadDownPressed = true;

			sendEvent(new GameOverEvent());
			GameVariables::LOST = false;
		}
		if (GameVariables::END && isPressed(XINPUT_GAMEPAD_DPAD_DOWN) && !dPadDownPressed) {
			dPadDownPressed = true;
			sendEvent(new RestartGameEvent());
			std::vector<GameObject*> end;
			for (int i = 0; i < gameObjects->size(); i++) {
				if (gameObjects->at(i)->type == END_SCREEN) end.push_back(gameObjects->at(i));
			}
			for (int i = 0; i < end.size(); i++) sendEvent(new DestroyEvent(end.at(i)));
			GameVariables::END = false;
		}

		if (!leftStickInDeadzone()) {
			move = true;
			rotation += Vector3(0, -state.Gamepad.sThumbLX * 0.00001f, 0);
			movement += Vector3(0, 0, state.Gamepad.sThumbLY * 0.00001f);
		}
		if (isPressed(XINPUT_GAMEPAD_Y) && !yPressed) {
			yPressed = true;
			sendEvent(new ToggleProfilerEvent());
		}
		if (isPressed(XINPUT_GAMEPAD_A) && !aPressed) {
			aPressed = true;
			sendEvent(new JumpEvent(player));
		}
		if (isPressed(XINPUT_GAMEPAD_X) && !xPressed) {
			xPressed = true;
			sendEvent(new SpawnEvent());
			Vector3 pos = Vector3(5, 5, 5) - player->position;
			pos = pos * 0.01f;
			sendEvent(new SoundEvent("unacceptable", pos));
		}
		if (isPressed(XINPUT_GAMEPAD_START) && !startPressed) {
			startPressed = true;
			std::vector<GameObject*> controls;
			for (int i = 0; i < gameObjects->size(); i++) {
				if (gameObjects->at(i)->type == CONTROLS) controls.push_back(gameObjects->at(i));
			}
			for (int i = 0; i < controls.size(); i++) sendEvent(new DestroyEvent(controls.at(i)));
		}
		movement.Normalise();
		if (move) {
			sendEvent(new MoveEvent(player, movement));
			sendEvent(new RotateEvent(player, rotation));
		}

		if (yPressed == true && !(isPressed(XINPUT_GAMEPAD_Y))) yPressed = false;
		if (aPressed == true && !(isPressed(XINPUT_GAMEPAD_A))) aPressed = false;
		if (xPressed == true && !(isPressed(XINPUT_GAMEPAD_X))) xPressed = false;
		if (startPressed == true && !(isPressed(XINPUT_GAMEPAD_START))) startPressed = false;
		if (backPressed == true && !(isPressed(XINPUT_GAMEPAD_BACK))) backPressed = false;
		if (dPadDownPressed == true && !(isPressed(XINPUT_GAMEPAD_DPAD_DOWN))) dPadDownPressed = false;

	}

}

bool GamepadController::isPressed(WORD button) {
	return (state.Gamepad.wButtons & button) != 0;
}

void GamepadController::updateState() {
	ZeroMemory(&state, sizeof(XINPUT_STATE));
	XInputGetState(playerNumber, &state);
}

bool GamepadController::isConnected() {
	ZeroMemory(&state, sizeof(XINPUT_STATE));
	DWORD check = XInputGetState(playerNumber, &state);

	if (check == ERROR_SUCCESS) return true;
	else return false;
}

void GamepadController::update() {
	float start = timer.GetMS();
	handleEvents();
	listenForInput();
	float end = timer.GetMS();
	float time = end - start;
	sendEvent(new TimeEvent(id, time));
}

bool GamepadController::leftStickInDeadzone() {
	
	short leftX = state.Gamepad.sThumbLX;
	short leftY = state.Gamepad.sThumbLY;

	if (leftX > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE || leftX < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) {
		return false;
	}		
	else if (leftY > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE || leftY < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) {
		return false;
	}	
	else return true;
}
