#pragma once
#include <EventSystem.h>
#include <MoveEvent.h>
#include <JumpEvent.h>

// Abstract input class
class Controller: public Subsystem {

public:
	Controller(EventSystem* es, std::vector<GameObject*>* objects, SubsystemID id);
	~Controller();

	virtual void handleEvents();
	virtual void update() = 0;
	virtual void listenForInput() = 0;
};

