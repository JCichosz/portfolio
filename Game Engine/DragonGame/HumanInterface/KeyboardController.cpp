#include "KeyboardController.h"
#include <SpawnPlayer.h>
#include <SoundEvent.h>
#include <ToggleProfilerEvent.h>
#include <TimeEvent.h>
#include <DestroyEvent.h>
#include <ShutdownEvent.h>
#include <UnloadLevelEvent.h>
#include <ReloadLevelEvent.h>
#include <GameWonEvent.h>
#include <GameOverEvent.h>
#include <RotateEvent.h>
#include <RestartGameEvent.h>

KeyboardController::KeyboardController(EventSystem* es, std::vector<GameObject*>* objects): Controller(es, objects, KEYBOARD_CONTROLLER) {
}


KeyboardController::~KeyboardController() {
}

void KeyboardController::update() {
	float start = timer.GetMS();
	handleEvents();
	listenForInput();
	float end = timer.GetMS();
	float time = end - start;
	sendEvent(new TimeEvent(id, time));
}


// listen for player input and issue appropriate events
void KeyboardController::listenForInput() {
	bool move = false;
	bool rotate = false;

	if (Keyboard::KeyTriggered(KEY_ESCAPE)) sendEvent(new ShutdownEvent());

	Vector3 rotation = Vector3(0, 0, 0);
	Vector3 movement = Vector3(0, 0, 0);

	if (Keyboard::KeyDown(KEY_W)) {
		move = true;
		movement += Vector3(0, 0, 1.0f);
	}
	if (Keyboard::KeyDown(KEY_S)) {
		move = true;
		movement += Vector3(0, 0, -1.0f);
	}
	if (Keyboard::KeyDown(KEY_A)) {
		rotate = true;
		rotation += Vector3(0, 1.0f, 0.0f);
	}
	if (Keyboard::KeyDown(KEY_D)) {
		rotate = true;
		rotation += Vector3(0, -1.0f, 0.0f);
	}
	if (Keyboard::KeyTriggered(KEY_SPACE)) sendEvent(new JumpEvent(player));

	if (Keyboard::KeyTriggered(KEY_O)) {
		sendEvent(new SpawnEvent());
		Vector3 pos = Vector3(5, 5, 5) - player->position;
		pos = pos * 0.01f;
		sendEvent(new SoundEvent("unacceptable", pos));
	}
	if (Keyboard::KeyTriggered(KEY_P)) sendEvent(new ToggleProfilerEvent());

	if (Keyboard::KeyTriggered(KEY_RETURN)) {
		std::vector<GameObject*> controls;
		for (int i = 0; i < gameObjects->size(); i++) {
			if (gameObjects->at(i)->type == CONTROLS) controls.push_back(gameObjects->at(i));
		}
		for (int i = 0; i < controls.size(); i++) sendEvent(new DestroyEvent(controls.at(i)));
	}
	if (GameVariables::WON && Keyboard::KeyTriggered(KEY_U)) {
		sendEvent(new GameWonEvent());
		GameVariables::WON = false;
	}
	if (GameVariables::LOST && Keyboard::KeyTriggered(KEY_U)) {
		sendEvent(new GameOverEvent());
		GameVariables::LOST = false;
	}
	if (GameVariables::END && Keyboard::KeyTriggered(KEY_U)) {
		sendEvent(new RestartGameEvent());
		std::vector<GameObject*> end;
		for (int i = 0; i < gameObjects->size(); i++) {
			if (gameObjects->at(i)->type == END_SCREEN) end.push_back(gameObjects->at(i));
		}
		for (int i = 0; i < end.size(); i++) sendEvent(new DestroyEvent(end.at(i)));
		GameVariables::END = false;
	}

	if(move) sendEvent(new MoveEvent(player, movement));
	if (rotate) sendEvent(new RotateEvent(player, rotation));

}