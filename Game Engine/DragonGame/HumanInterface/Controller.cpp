#include "Controller.h"
#include <SpawnPlayer.h>

Controller::Controller(EventSystem* eventSys, std::vector<GameObject*>* objects, SubsystemID id): Subsystem(eventSys, objects, id) {
}

void Controller::handleEvents() {
	for (int i = 0; i < es->getEventQueue().size(); i++) {
		Event* e = es->getEventQueue().at(i);

		if (needToHandleEvent(*e)) {
			switch (e->getEventType()) {
			case SPAWN_PLAYER_EVENT: {
				Player* object = static_cast<Player*>(static_cast<SpawnPlayer*>(e)->getGameObject());
				player = object;
				es->markHandledBySubsystem(i, id);
				break;
			}
			default: break;
			}
		}
	}
}

Controller::~Controller() {
}