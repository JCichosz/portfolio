#include "FlatGround.h"



FlatGround::FlatGround(): GameObject(Vector3(0.0f, -4.0f, 0.0f), 0.0f, Vector3(20,1,200)) {
	setMesh("quad");
	setShader("flatGround");
	setTexture("grass");
	setType(FLAT_GROUND);
}

FlatGround::FlatGround(Vector3 pos) : GameObject(pos, 0.0f) {
	setMesh("quad");
	setShader("");
	setTexture("");
	setType(FLAT_GROUND);
}

FlatGround::FlatGround(Vector3 pos, Vector3 sc): GameObject(pos, 0.0f, sc) {
	setMesh("quad");
	setShader("flatGround");
	setTexture("grass");
	setType(FLAT_GROUND);
}

FlatGround::FlatGround(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture): GameObject(pos, m, sc, rot, shader, texture) {
	setMesh("quad");
	setType(FLAT_GROUND);
}


FlatGround::~FlatGround() {
}
