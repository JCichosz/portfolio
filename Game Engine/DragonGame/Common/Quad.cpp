#include "Quad.h"



Quad::Quad(): GameObject(0.0f) {
	setMesh("");
	setShader("");
	setTexture("");
	setType(QUAD);
}

Quad::Quad(Vector3 pos, std::string shader, std::string texture): GameObject(pos, 0.0f) {
	setMesh("quad");
	setShader(shader);
	setTexture(texture);
	setType(QUAD);
}

Quad::Quad(Vector3 pos, std::string shader, std::string texture, Vector3 scale, Quaternion rot): GameObject(pos, 0.0f, scale, rot) {
	setMesh("quad");
	setShader(shader);
	setTexture(texture);
	setType(QUAD);
}

Quad::Quad(Vector3 pos, std::string shader, std::string texture, Vector3 scale, Quaternion rot, GameObjectType t) : GameObject(pos, 0.0f, scale, rot) {
	setMesh("quad");
	setShader(shader);
	setTexture(texture);
	setType(t);
}

Quad::Quad(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture, GameObjectType t): GameObject(pos, m, sc, rot, shader, texture) {
	setType(t);
	setMesh("quad");

}

Quad::Quad(Vector3 pos, std::string shader, std::string texture, Quaternion rot): GameObject(pos, 0.0f, Vector3(1,1,1), rot) {
	setMesh("quad");
	setShader(shader);
	setTexture(texture);
	setType(QUAD);
}

Quad::Quad(Vector3 pos) : GameObject(pos, 0.0f) {
	setMesh("quad");
	setShader("");
	setTexture("");
	setType(QUAD);
}

Quad::Quad(Vector3 pos, Vector3 sc): GameObject(pos, 0.0f, sc, Quaternion::EulerAnglesToQuaternion(-90, 0, 0)) {
	setMesh("quad");
	setShader("");
	setTexture("");
	setType(QUAD);
}

Quad::~Quad() {
}
