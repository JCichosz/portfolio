#include "Cube.h"

Cube::Cube():GameObject() {
	setMesh("cube");
	setShader("basic");
	setType(CUBE);
}

Cube::Cube(Vector3 pos): GameObject(pos) {
	setMesh("cube");
	setShader("basic");
	setType(CUBE);
}

Cube::Cube(Vector3 pos, Vector3 sc): GameObject(pos, sc) {
	setMesh("cube");
	setShader("basic");
	setType(CUBE);
}

Cube::Cube(Vector3 pos, float m): GameObject(pos, m) {
	setMesh("cube");
	setShader("basic");
	setType(CUBE);
}

Cube::Cube(Vector3 pos, float m, Vector3 sc): GameObject(pos, m, sc) {
	setMesh("cube");
	setShader("basic");
	setType(CUBE);
}

Cube::Cube(Vector3 pos, float m, Vector3 sc, Quaternion rot): GameObject(pos, m, sc, rot) {
	setMesh("cube");
	setShader("basic");
	setType(CUBE);
}

Cube::Cube(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture): GameObject(pos, m, sc, rot, shader, texture) {
	setMesh("cube");
	setType(CUBE);
}


Cube::~Cube() {
}
