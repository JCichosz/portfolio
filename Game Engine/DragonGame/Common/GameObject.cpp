#include "GameObject.h"
#include <ResourceManager.h>

const Vector3 defaultPos = Vector3(5, 5, 5);
const Quaternion defaultRot = Quaternion(0, 0, 0, 1);
const Vector3 defaultScale = Vector3(1, 1, 1);
const float defaultMass = 1.0f;

GameObject::GameObject(): position(defaultPos), mass(defaultMass), scale(defaultScale), rotation(defaultRot) {
}

GameObject::GameObject(float m): position(defaultPos), mass(m), scale(defaultScale), rotation(defaultRot) {
}

GameObject::GameObject(Vector3 pos): position(pos), mass(defaultMass), scale(defaultScale), rotation(defaultRot) {
}

GameObject::GameObject(Vector3 pos, Vector3 sc): position(pos), mass(defaultMass), scale(sc), rotation(defaultRot) {

}

GameObject::GameObject(Vector3 pos, float m) : position(pos), mass(m), scale(defaultScale), rotation(defaultRot) {
}

GameObject::GameObject(Vector3 pos, float m, Vector3 sc): position(pos), mass(m), scale(sc), rotation(defaultRot) {
}

GameObject::GameObject(Vector3 pos, float m, Vector3 sc, Quaternion rot) : position(pos), mass(m), scale(sc), rotation(rot) {
}

GameObject::GameObject(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture): position(pos), mass(m), scale(sc), rotation(rot) {
	if (shader != "null")	setShader(shader);
	if (texture != "null")	setTexture(texture);
}

GameObject::~GameObject(){

}

void* GameObject::operator new(size_t s) {
	return ResourceManager::newGameObject();
}
void GameObject::operator delete(void* p) {
	ResourceManager::deleteGameObject(p);
}