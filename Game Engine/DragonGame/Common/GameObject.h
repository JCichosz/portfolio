#pragma once
#include "Vector3.h"
#include "Quaternion.h"

#include <string>

enum GameObjectType {
		CUBE,
		TERRAIN,
		FLAT_GROUND,
		STARSHIP,
		PLAYER,
		RING, 
		QUAD,
		CONTROLS,
		WIN_SCREEN,
		LOSE_SCREEN,
		END_SCREEN,
		HEALTH_BOOST,
		OBSTACLE,
		WORLD_BOUND
	};

class GameObject {

public:
	GameObject();
	GameObject(float mass);
	GameObject(Vector3 pos);
	GameObject(Vector3 pos, Vector3 sc);
	GameObject(Vector3 pos, float mass);
	GameObject(Vector3 pos, float mass, Vector3 sc);
	GameObject(Vector3 pos, float mass, Vector3 sc, Quaternion rot);
	GameObject(Vector3 pos, float mass, Vector3 sc, Quaternion rot, std::string shader, std::string texture);
	~GameObject();

	void* operator new(size_t s);
	void operator delete(void* p);

	virtual bool moves() = 0;
	virtual bool collides() = 0;
	virtual bool destructible() = 0;

	void setScale(Vector3 s) { scale = s; }
	void setMesh(std::string name) { meshName = name; };
	void setShader(std::string name) { shaderName = name; }
	void setType(GameObjectType t) { type = t; }
	void setTexture(std::string name) { textureName = name; }

	GameObjectType type;
	Vector3 position;
	Vector3 scale;
	Quaternion rotation;
	float mass;
	std::string meshName;
	std::string shaderName;
	std::string textureName = "";
};

