#pragma once
#include "Player.h"


Player::Player(): GameObject(10.0f), hitpoints(GameVariables::INITIAL_HP) {
	setMesh("dragon");
	setShader("dragon");
	setTexture("dragon");
	setType(PLAYER);
}

Player::Player(Vector3 pos) : GameObject(pos), hitpoints(GameVariables::INITIAL_HP) {
	setMesh("dragon");
	setShader("dragon");
	setTexture("dragon");
	setType(PLAYER);
}

Player::Player(Vector3 pos, Vector3 sc): GameObject(pos, 5.0f, sc), hitpoints(GameVariables::INITIAL_HP) {
	setMesh("dragon");
	setShader("dragon");
	setTexture("dragon");
	setType(PLAYER);
}

Player::Player(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture): GameObject(pos, m, sc, rot, shader, texture), hitpoints(GameVariables::INITIAL_HP) {
	setMesh("dragon");
	setType(PLAYER);
}

Player::~Player() {
}

Quaternion Player::reverseYRotation() {
	Quaternion quat = rotation;
	quat.y = -quat.y;
	return quat;
}

Quaternion Player::xRotation() {
	Quaternion quat = rotation;
	quat.y = 0;
	quat.z = 0;

	return quat;
}

Quaternion Player::yRotation() {
	Quaternion quat = rotation;
	quat.x = 0;
	quat.z = 0;

	return quat;
}

Quaternion Player::zRotation() {
	Quaternion quat = rotation;
	quat.x = 0;
	quat.y = 0;

	return quat;
}
