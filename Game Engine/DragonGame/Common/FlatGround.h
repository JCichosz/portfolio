#pragma once
#include "GameObject.h"
class FlatGround:	public GameObject {
public:
	FlatGround();
	FlatGround(Vector3 pos);
	FlatGround(Vector3 pos, Vector3 sc);
	FlatGround(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture);
	~FlatGround();

	bool collides() { return true; }
	bool moves() { return false; }
	bool destructible() { return false; }

};

