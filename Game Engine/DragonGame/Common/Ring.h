#pragma once
#include "GameObject.h"
class Ring: public GameObject {
public:
	Ring();
	Ring(Vector3 pos);
	Ring(Vector3 pos, Vector3 sc);
	Ring(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture);
	Ring(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture, GameObjectType t);

	~Ring();

	bool moves() { return false; }
	bool collides() { return true; }
	bool destructible() { return true; }
};

