#include "GameVariables.h"

int GameVariables::INITIAL_HP;
int GameVariables::RING_SCORE;
int GameVariables::OBSTACLE_DAMAGE;
int GameVariables::NEEDED_SCORE;

bool GameVariables::WON;
bool GameVariables::LOST;
bool GameVariables::END;