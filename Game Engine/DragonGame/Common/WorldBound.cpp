#include "WorldBound.h"


WorldBound::WorldBound(Vector3 pos, Vector3 n, Quaternion rot): GameObject(pos, 0.0f, Vector3(1000, 1000, 1000), rot) {
	normal = n;
	setType(WORLD_BOUND);
	setMesh("quad");
	setShader("sky");
	setTexture("sky");
}

WorldBound::~WorldBound() {
}
