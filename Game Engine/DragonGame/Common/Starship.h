#pragma once
#include "GameObject.h"
class Starship :
	public GameObject
{
public:
	Starship();
	Starship(Vector3 pos);
	Starship(Vector3 pos, float mass);
	Starship(Vector3 pos, float mass, Vector3 sc);
	Starship(Vector3 pos, float mass, Vector3 sc, Quaternion rot);
	~Starship();

	bool collides() { return true; }
	bool moves() { return true; }
};

