#pragma once
#include "GameObject.h"
class Quad : public GameObject {
public:
	Quad();
	Quad(Vector3 pos);
	Quad(Vector3 pos, Vector3 sc);
	Quad(Vector3 pos, std::string shader, std::string texture);
	Quad(Vector3 pos, std::string shader, std::string texture, Vector3 scale, Quaternion rot);
	Quad(Vector3 pos, std::string shader, std::string texture, Vector3 scale, Quaternion rot, GameObjectType t);
	Quad(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture, GameObjectType t);
	Quad(Vector3 pos, std::string shader, std::string texture, Quaternion rot);
	~Quad();

	bool collides() { return false; }
	bool moves() { return false; }
	bool destructible() { return false; }
};

