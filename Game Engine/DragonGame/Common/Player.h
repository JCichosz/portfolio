#pragma once
#include "GameObject.h"
#include "GameVariables.h"

class Player: public GameObject {
public:
	Player();
	Player(Vector3 pos);
	Player(Vector3 pos, Vector3 sc);
	Player(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture);
	~Player();

	virtual bool moves() { return true; }
	virtual bool collides() { return true; }
	virtual bool destructible() { return false; }

	int getHitpoints() { return hitpoints; }
	void decreaseHitpoints(int amount) { hitpoints -= amount; }
	void increaseHitpoints(int amount) { hitpoints += amount; }
	void resetHitpoints() { hitpoints = GameVariables::INITIAL_HP; }
	Quaternion reverseYRotation();
	Quaternion xRotation();
	Quaternion yRotation();
	Quaternion zRotation();


private:
	int hitpoints;
};

