#include "Starship.h"



Starship::Starship() {
	setMesh("starship");
	setShader("basic");
	setType(STARSHIP);
}

Starship::Starship(Vector3 pos) : GameObject(pos) {
	setMesh("starship");
	setShader("basic");
	setType(STARSHIP);
}

Starship::Starship(Vector3 pos, float m) : GameObject(pos, m) {
	setMesh("starship");
	setShader("basic");
	setType(STARSHIP);
}

Starship::Starship(Vector3 pos, float m, Vector3 sc) : GameObject(pos, m, sc) {
	setMesh("starship");
	setShader("basic");
	setType(STARSHIP);
}

Starship::Starship(Vector3 pos, float m, Vector3 sc, Quaternion rot) : GameObject(pos, m, sc, rot) {
	setMesh("starship");
	setShader("basic");
	setType(STARSHIP);
}

Starship::~Starship() {
}
