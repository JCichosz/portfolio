#pragma once
#include "GameObject.h"
class WorldBound:public GameObject {
public:
	WorldBound(Vector3 pos, Vector3 normal, Quaternion rot);

	~WorldBound();

	bool collides() { return true; }
	bool moves() { return false; }
	bool destructible() { return false; }

	Vector3 getNormal() { return normal; }

private:
	Vector3 normal;
};

