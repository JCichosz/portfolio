#include "Ring.h"


Ring::Ring(): GameObject() {
	setMesh("ring");
	setShader("yellow");
	setType(RING);
}

Ring::Ring(Vector3 pos): GameObject(pos) {
	setMesh("ring");
	setShader("yellow");
	setType(RING);
}

Ring::Ring(Vector3 pos, Vector3 sc) : GameObject(pos, sc) {
	setMesh("ring");
	setShader("yellow");
	setType(RING);
}

Ring::Ring(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture): GameObject(pos, m, sc, rot, shader,  texture){
	setType(RING);
	setMesh("ring");
}

Ring::Ring(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture, GameObjectType t) : GameObject(pos, m, sc, rot, shader, texture) {
	setType(t);
	setMesh("ring");
}

Ring::~Ring() {
}
