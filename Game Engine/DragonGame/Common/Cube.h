#pragma once
#include "GameObject.h"

class Cube: public GameObject {

public:
	Cube();
	Cube(Vector3 pos);
	Cube(Vector3 pos, Vector3 sc);
	Cube(Vector3 pos, float mass);
	Cube(Vector3 pos, float mass, Vector3 sc);
	Cube(Vector3 pos, float mass, Vector3 sc, Quaternion rot);
	Cube(Vector3 pos, float m, Vector3 sc, Quaternion rot, std::string shader, std::string texture);
	~Cube();

	bool collides() { return true; }
	bool moves() { return true; }
	bool destructible() { return false; }

};

