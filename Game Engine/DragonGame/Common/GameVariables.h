#pragma once
#include "Vector3.h"


class GameVariables {

public:
	static int INITIAL_HP;
	static int RING_SCORE;
	static int OBSTACLE_DAMAGE;
	static int NEEDED_SCORE;

	static bool WON;
	static bool LOST;
	static bool END;
};




