#pragma once
#include <btBulletDynamicsCommon.h>
#include <EventSystem.h>
#include <Subsystem.h>
#include <unordered_map>
#include <GameObject.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>

/* Physics system responsible for movement and collision response. */
class Physics: public Subsystem {

public:
	Physics(EventSystem* eventSys, std::vector<GameObject*>* objects);
	~Physics();

	void update(float msec);
	void updateGameObjects(float msec);
	void applyForce(GameObject* obj, Vector3 dir);
	void aaplyForceInFacingDirection(GameObject* obj, Vector3 dir);
	void applyTorque(GameObject* obj, Vector3 dir);
	Vector3 facingDirection(GameObject* object, Vector3 dir);
	void handleEvents();
	void createRigidbody(GameObject* object);
	void createCollider(GameObject* object);
	void checkCollisions();

private:
	void destroyObject(GameObject* object);

	btBroadphaseInterface* broadphase;
	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btDiscreteDynamicsWorld* dynamicsWorld;
	
	std::unordered_map<GameObject*, btCollisionObject*> collisionObjects; // map of game objects to collision objects for easy lookup of a body for a given game object
	std::unordered_map<GameObject*, btRigidBody*> rigidbodies; // map of rigidbodies
	std::unordered_map<GameObject*, btCollisionShape*> colliders; // map of game objects to their collision shapes
};

