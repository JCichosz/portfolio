#pragma once
#include "Physics.h"
#include <iostream>
#include <SpawnEvent.h>
#include <MoveEvent.h>
#include <JumpEvent.h>
#include <SpawnPlayer.h>
#include <TimeEvent.h>
#include <DestroyEvent.h>
#include <ScoreIncreaseEvent.h>
#include <DamagePlayerEvent.h>
#include <SoundEvent.h>
#include <GameVariables.h>
#include <WorldBound.h>
#include <RotateEvent.h>

// Constructor initialises Bullet.
Physics::Physics(EventSystem* eventSys, std::vector<GameObject*>* objects): Subsystem(eventSys, objects, PHYSICS) {
	broadphase = new btDbvtBroadphase();
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);
	solver = new btSequentialImpulseConstraintSolver;
	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	dynamicsWorld->setGravity(btVector3(0, -10, 0));
}

Physics::~Physics() {
	for (auto pair : collisionObjects) {
		// remove the collision object from the physics world
		dynamicsWorld->removeCollisionObject(pair.second);

		// if it's a rigidbody - delete it's motion state
		if(dynamic_cast<btRigidBody*>(pair.second)) delete static_cast<btRigidBody*>(pair.second)->getMotionState();

		delete pair.second;
	}

	// Remove the collision shapes
	for (auto pair : colliders) {
		delete pair.second;
	}

	// delete all Bullet systems
	delete dynamicsWorld;
	delete solver;
	delete collisionConfiguration;
	delete dispatcher;
	delete broadphase;
}

// Update the physics system and send the timer event to profiler.
void Physics::update(float msec) {
	float start = timer.GetMS();
	handleEvents();
	dynamicsWorld->stepSimulation(msec * 0.001f, 10);
	updateGameObjects(msec); // update game objects with new positions and rotations
	checkCollisions(); // check for collisions with the player to inform the game
	float end = timer.GetMS();
	float time = end - start;
	sendEvent(new TimeEvent(id, time));
}

// Updates game objects with updated data
float spin = 0;
void Physics::updateGameObjects(float msec) {
	spin += msec * 0.15f;
	for (auto pair : collisionObjects) {
		pair.first->position = pair.second->getWorldTransform().getOrigin().toVector3();

		if (pair.first->type == RING || pair.first->type == OBSTACLE) {
			pair.second->getWorldTransform().setRotation(Quaternion::EulerAnglesToQuaternion(0, spin, 0));
		}

		pair.first->rotation = pair.second->getWorldTransform().getRotation().toQuaternion();
	}
}

// Creates a rigidbody for moveable objects and a ghost object for objects that collide but
// have no collision response or move. 
void Physics::createRigidbody(GameObject* object) {

	if (object->type != RING && object->type != OBSTACLE) {
		btVector3 fallInertia(0, 0, 0);
		colliders[object]->calculateLocalInertia(object->mass, fallInertia);

		btDefaultMotionState* defaultMotionState = new btDefaultMotionState(btTransform(object->rotation, object->position));

		btRigidBody::btRigidBodyConstructionInfo constInfo(object->mass,
			defaultMotionState,
			colliders[object],
			fallInertia);

		btRigidBody* rigidbody = new btRigidBody(constInfo);

		rigidbody->setGameObject(object);
		if (object->type == PLAYER) {
			//rigidbody->setAngularFactor(btVector3(0, 1.0f, 0));
			rigidbody->setDamping(0.9f, 0.9f);
		}
		rigidbodies.emplace(object, rigidbody);
		collisionObjects.emplace(object, rigidbody);
		dynamicsWorld->addRigidBody(rigidbody);

	}
	else {
		btGhostObject* ghost = new btGhostObject();
		ghost->setCollisionShape(colliders[object]);
		ghost->setWorldTransform(btTransform(object->rotation, object->position));
		ghost->setGameObject(object);

		collisionObjects.emplace(object, ghost);
		dynamicsWorld->addCollisionObject(ghost);
	}

}


// Cerates and sppropriate collision shape for each created Collision object
void Physics::createCollider(GameObject* object) {
	btCollisionShape* shape;
	switch (object->type) {
	case CUBE: {
		shape = new btBoxShape(btVector3(object->scale));
		colliders.emplace(object, shape);
		break;
	}
	case RING: {
		shape = new btBoxShape(btVector3(object->scale));
		colliders.emplace(object, shape);
		break;
	}
	case OBSTACLE: {
		shape = new btBoxShape(btVector3(object->scale));
		colliders.emplace(object, shape);
		break;
	}
	case FLAT_GROUND: {
		shape = new btStaticPlaneShape(btVector3(0,-1,0), 1);
		colliders.emplace(object, shape);
		break;
	}
	case WORLD_BOUND: {
		WorldBound* bound = static_cast<WorldBound*>(object);
		shape = new btStaticPlaneShape(bound->getNormal(), 1);
		colliders.emplace(object, shape);
		break;
	}
	case STARSHIP: {
		shape = new btConvexHullShape(btVector3(0, 1, 0), 1);
		colliders.emplace(object, shape);
		break;
	}
	case PLAYER: {
		shape = new btBoxShape(btVector3(object->scale.x *5, object->scale.y * 3, object->scale.z *5));
		colliders.emplace(object, shape);
		break;
	}
	default: break;
	}
}


// Check for collisions to create appropriate events
void Physics::checkCollisions() {
	int manifolds = dynamicsWorld->getDispatcher()->getNumManifolds();

	for (int i = 0; i < manifolds; i++) {
		btPersistentManifold* mani = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
		
		GameObject* obj0 = mani->getBody0()->getGameObject();
		GameObject* obj1 = mani->getBody1()->getGameObject();

		if (obj0->destructible() || obj1->destructible()) {
			if (obj0->type == PLAYER || obj1->type == PLAYER) {
				GameObject* toDestroy;
				if (obj0->destructible()) toDestroy = obj0;
				else toDestroy = obj1;

				// if collided with ring - increase score and despawn ring
				if (toDestroy->type == RING && !GameVariables::LOST) {
					sendEvent(new ScoreIncreaseEvent(GameVariables::RING_SCORE));
					sendEvent(new SoundEvent("sonic"));
				}
				else if (toDestroy->type == OBSTACLE && !GameVariables::WON) {
					sendEvent(new DamagePlayerEvent(GameVariables::OBSTACLE_DAMAGE)); // else collided with an obstacle - decrease player hitpoints
					sendEvent(new SoundEvent("unacceptable"));
				}
				sendEvent(new DestroyEvent(toDestroy));
			}
		}
	}
}

// apply force to move a rigidbody
void Physics::applyForce(GameObject* obj, Vector3 dir) {
	if (rigidbodies[obj]) {
		rigidbodies[obj]->activate(true);
		rigidbodies[obj]->applyCentralImpulse(dir);
	}
}

void Physics::applyTorque(GameObject* obj, Vector3 dir) {
	if (rigidbodies[obj]) {
		rigidbodies[obj]->activate(true);
		if (dir.Length() > 1.0f) {
			dir.Normalise();
		}
		rigidbodies[obj]->applyTorqueImpulse(dir * 10);
	}
}

void Physics::destroyObject(GameObject* object) {
	btCollisionObject* coll = collisionObjects[object];
	dynamicsWorld->removeCollisionObject(collisionObjects[object]);
	collisionObjects.erase(object);
	if (rigidbodies[object]) {
		delete rigidbodies[object]->getMotionState();
		rigidbodies.erase(object);
	}
	delete coll->getCollisionShape();
	delete coll;
	colliders.erase(object);
}

Vector3 Physics::facingDirection(GameObject* obj, Vector3 d) {
	Vector3 dir = Vector3(obj->rotation.x, obj->rotation.y, obj->rotation.z);

	float w = obj->rotation.w;

	btVector3 direction = (2.0f * Vector3::Dot(dir, d) * dir) +(w*w - Vector3::Dot(dir, dir)) * d	+ 2.0f * w * Vector3::Cross(dir, d);

	return direction.toVector3();
}

void Physics::aaplyForceInFacingDirection(GameObject* obj, Vector3 dir) {
	if (rigidbodies[obj]) {
		Vector3 v = Vector3(0, 0, 1);
		Vector3 direction = facingDirection(obj, v);
		rigidbodies[obj]->activate(true);
		if (dir.Length() > 1.0f) dir.Normalise();
		rigidbodies[obj]->applyCentralImpulse(direction * dir.z * 3.0f);
	}
}

void Physics::handleEvents() {
	for (int i = 0; i < es->getEventQueue().size(); i++) {
		Event* e = es->getEventQueue().at(i);

		if (needToHandleEvent(*e)) {
			switch (e->getEventType()) {
			case MOVEMENT_EVENT: {
				MoveEvent* move = static_cast<MoveEvent*>(e);
				aaplyForceInFacingDirection(move->getGameObject(), move->getDirection());
				es->markHandledBySubsystem(i, id);
				break;
			}
			case ROTATE_EVENT: {
				RotateEvent* move = static_cast<RotateEvent*>(e);
				applyTorque(move->getGameObject(), move->getDirection());
				es->markHandledBySubsystem(i, id);
				break;
			}
			case JUMP_EVENT: {
				JumpEvent* jump = static_cast<JumpEvent*>(e);
				applyForce(jump->getGameObject(), jump->getDirection());
				es->markHandledBySubsystem(i, id);
				break;
			}
			case SPAWN_EVENT: {
				GameObject* object = static_cast<SpawnEvent*>(e)->getGameObject();
				createCollider(object);
				if (object->collides()) {
					createRigidbody(object);
				}
				es->markHandledBySubsystem(i, id);
				break;
			}
			case DESTROY_EVENT: {
				GameObject* object = static_cast<DestroyEvent*>(e)->getGameObject();
				destroyObject(object);
				es->markHandledBySubsystem(i, id);
				break;
			}
			case SPAWN_PLAYER_EVENT: {
				Player* object = static_cast<Player*>(static_cast<SpawnPlayer*>(e)->getGameObject());
				player = object;
				createCollider(object);
				createRigidbody(object);
				es->markHandledBySubsystem(i, id);
				break;
			}
			default: break;
			}
		}
	}
}