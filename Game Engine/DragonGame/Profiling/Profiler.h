#pragma once
#include <Subsystem.h>
#include <map>
#include <EventSystem.h>

/* The profiler handles time events and updates it's map of Subsystems to update times and prints to screen. 
Can be enabled and disabled. */
class Profiler: public Subsystem {
public:
	Profiler(EventSystem* es, std::vector<GameObject*>* objects);
	~Profiler();

	void enable();
	void disable();
	void update(float msec);
	void handleEvents();
	void output();
	void printNumberOfEvents();
	bool isEnabled() { return enabled; }
	std::string toString(SubsystemID id);

private:
	bool enabled;
	std::map<SubsystemID, float> times;
};

