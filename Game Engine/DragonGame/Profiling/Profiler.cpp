#include "Profiler.h"
#include <TimeEvent.h>

Profiler::Profiler(EventSystem* es, std::vector<GameObject*>* objects): Subsystem(es, objects, PROFILER) {
	enable();
	times.emplace(GRAPHICS, 0.0f);
	times.emplace(PHYSICS, 0.0f);
	times.emplace(AUDIO, 0.0f);
	times.emplace(KEYBOARD_CONTROLLER, 0.0f);
	times.emplace(GAMEPAD_CONTROLLER, 0.0f);
}


Profiler::~Profiler() {
}

void Profiler::enable() {
	enabled = true;
}

void Profiler::disable() {
	enabled = false;
}

void Profiler::update(float msec) {
	handleEvents();
	if (enabled) {
		output();
		float sec = msec * 0.001;
		std::cout << "FPS:  " << (1 / sec) <<"\n";
	}
}

void Profiler::handleEvents() {
	for (int i = 0; i < es->getEventQueue().size(); i++) {
		Event* e = es->getEventQueue().at(i);

		if (needToHandleEvent(*e)) {
			switch (e->getEventType()) {
			case TIMER_EVENT: {
				TimeEvent* te = static_cast<TimeEvent*>(e);
				times[te->getId()] = te->getTime();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case TOGGLE_PROFILER_EVENT: {
				if (enabled) disable();
				else enable();
				es->markHandledBySubsystem(i, id);
				break;
			}
			default: break;
			}
		}
	}
}

void Profiler::output() {
	for (auto pair : times) {
		std::cout << toString(pair.first) << ": " << pair.second << " msec\n";
	}
}

void Profiler::printNumberOfEvents() {
	if(enabled)	std::cout << "Number of events after a clear: " << es->getEventQueue().size() << "\n";
}

std::string Profiler::toString(SubsystemID id) {
	switch (id) {
	case GRAPHICS: return "Graphics";
	case PHYSICS: return "Physics";
	case AUDIO: return "Audio";
	case KEYBOARD_CONTROLLER: return "Keyboard Controller";
	case GAMEPAD_CONTROLLER: return "Gamepad Controller";
	}
	return std::string();
}
