#include "GameLogic.h"
#include <ScoreIncreaseEvent.h>
#include <SetScoreTextEvent.h>
#include <GameWonEvent.h>
#include <SpawnEvent.h>
#include <Quad.h>
#include <GameOverEvent.h>
#include <SpawnPlayer.h>
#include <DamagePlayerEvent.h>
#include <SetHitpointsTextEvent.h>
#include <GameVariables.h>

GameLogic::GameLogic(EventSystem* es, std::vector<GameObject*>* objects): Subsystem(es, objects, GAME_LOGIC), score(0) {
	GameVariables::WON = false;
	GameVariables::LOST = false;
	GameVariables::END = false;
}

GameLogic::~GameLogic() {
}

void GameLogic::handleEvents() {
	for (int i = 0; i < es->getEventQueue().size(); i++) {
		Event* e = es->getEventQueue().at(i);

		if (needToHandleEvent(*e)) {
			switch (e->getEventType()) {
			case INCREASE_SCORE: {
				int amount = static_cast<ScoreIncreaseEvent*>(e)->getAmount();
				increaseScore(amount);
				sendEvent(new SetScoreTextEvent(score));
				es->markHandledBySubsystem(i, id);
				break;
			}
			case DAMAGE_PLAYER: {
				int amount = static_cast<DamagePlayerEvent*>(e)->getAmount();
				player->decreaseHitpoints(amount);
				sendEvent(new SetHitpointsTextEvent(player->getHitpoints()));
				es->markHandledBySubsystem(i, id);
				break;
			}
			case LOAD_LEVEL: {
				resetStats();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case WIN_EVENT: {
				resetStats();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case GAME_OVER_EVENT: {
				resetStats();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case RELOAD_LEVEL: {
				resetStats();
				es->markHandledBySubsystem(i, id);
				break;
			}
			case SPAWN_PLAYER_EVENT: {
				GameObject* go = static_cast<SpawnPlayer*>(e)->getGameObject();
				Player* object = static_cast<Player*>(go);			
				player = object;
				es->markHandledBySubsystem(i, id);
				break;
			}
			default: break;
			}
		}
	}
}

void GameLogic::resetStats() {
	if (player != nullptr) {
		player->resetHitpoints();		
		sendEvent(new SetHitpointsTextEvent(player->getHitpoints()));
	}
	score = 0;
	sendEvent(new SetScoreTextEvent(0));


}

// handles game logic
void GameLogic::update() {
	handleEvents();

	// if HP low enough - lose screen
	if (player->getHitpoints() <= 0 && !GameVariables::LOST) {
		GameVariables::LOST = true;
		sendEvent(new SpawnEvent(new Quad(player->position + Vector3(0, 10, -19), "lost", "lost", Vector3(500, 500, 500), Quaternion::EulerAnglesToQuaternion(90, 0, 0), LOSE_SCREEN)));
	}
	// if score high enough win
	if (score >= GameVariables::NEEDED_SCORE && !GameVariables::WON && !GameVariables::LOST) {
		GameVariables::WON = true;
		sendEvent(new SpawnEvent(new Quad(player->position + Vector3(0, 10, -19), "winner", "winner", Vector3(500,500,500), Quaternion::EulerAnglesToQuaternion(90,0,0), WIN_SCREEN)));
	}
}

void GameLogic::increaseScore(int num) {
	score += num;
}
