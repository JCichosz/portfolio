#pragma once
#include <Subsystem.h>
#include <EventSystem.h>

// Handles game logic
class GameLogic: public Subsystem {
public:
	GameLogic(EventSystem* es, std::vector<GameObject*>* objects);
	~GameLogic();

	void handleEvents();
	void update();
	void resetStats();
	void setScore(int num) { score = num; }
	void increaseScore(int num);
	int getScore() { return score; }

private:
	int score;
};

