package equinox.scales

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

/* The starting screen of the application, used as an introduction to the application*/
class MainScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_screen)
        window.decorView.setBackgroundColor(getColor(R.color.darkGrey))
       title = ""


        var button = findViewById<Button>(R.id.button3)

        /* Go to the egg selection screen when the user presses the button */
        button.setOnClickListener {
            val intent = Intent(this@MainScreen,EggChoice::class.java)
            startActivity(intent)
        }
    }




}
