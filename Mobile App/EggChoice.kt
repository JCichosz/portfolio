package equinox.scales

import android.app.AlertDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView

class EggChoice : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_egg_choice)
        window.decorView.setBackgroundColor(getColor(R.color.darkGrey))
        title = ""



        var redEgg = findViewById<ImageButton>(R.id.redEggButton)
        var orangeEgg = findViewById<ImageButton>(R.id.orangeEggButton)
        var greenEgg = findViewById<ImageButton>(R.id.greenEggButton)
        var purpleEgg = findViewById<ImageButton>(R.id.purpleEggButton)
        var aquaEgg = findViewById<ImageButton>(R.id.aquaEggButton)
        var blueEgg = findViewById<ImageButton>(R.id.blueEggButton)

        redEgg.setBackgroundColor(getColor(R.color.darkGrey))
        orangeEgg.setBackgroundColor(getColor(R.color.darkGrey))
        greenEgg.setBackgroundColor(getColor(R.color.darkGrey))
        purpleEgg.setBackgroundColor(getColor(R.color.darkGrey))
        aquaEgg.setBackgroundColor(getColor(R.color.darkGrey))
        blueEgg.setBackgroundColor(getColor(R.color.darkGrey))

        /* Prompt the confirmation screen when the user chooses an egg */
        redEgg.setOnClickListener {
            confirmChoice("red")
        }
        orangeEgg.setOnClickListener {
            confirmChoice("orange")
        }
        greenEgg.setOnClickListener {
            confirmChoice("green")
        }
        purpleEgg.setOnClickListener {
            confirmChoice("purple")
        }
        aquaEgg.setOnClickListener {
            confirmChoice("aqua")
        }
        blueEgg.setOnClickListener {
            confirmChoice("yellow")
        }
    }

    /* Confirm the egg choice */
   private fun confirmChoice(colour: String) {
       val builder = AlertDialog.Builder(this@EggChoice)
       val confirmView = layoutInflater.inflate(R.layout.confirm_choice, null)
       val confirm = confirmView.findViewById<Button>(R.id.confirm)
       val cancel = confirmView.findViewById<Button>(R.id.cancel)

       builder.setView(confirmView)
       val dialog = builder.create()

       dialog.setMessage("Do you choose the " + colour + " egg?")

       dialog.show()

       confirm.setOnClickListener {
           val intent = Intent(this@EggChoice,EggActivity::class.java)
           intent.putExtra("EggColour", colour)
           startActivity(intent)
       }
       cancel.setOnClickListener {
           dialog.cancel();
       }

    }
}


