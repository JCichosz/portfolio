package equinox.scales

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity


import kotlinx.android.synthetic.main.activity_egg.*
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar



class EggActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_egg)
        window.decorView.setBackgroundColor(getColor(R.color.darkGrey))

        val eggColour = intent.getStringExtra("EggColour")

        val incubate = findViewById<Button>(R.id.button4)
        val hatch = findViewById<Button>(R.id.button5)
        hatch.visibility = View.INVISIBLE

        val image = findViewById<ImageView>(R.id.imageView3)

        /* Identify which picture to load */
        when(eggColour) {
            "red" -> image.setImageResource(R.drawable.red)
            "orange" -> image.setImageResource(R.drawable.orange)
            "green" -> image.setImageResource(R.drawable.green)
            "purple" -> image.setImageResource(R.drawable.purple)
            "aqua" -> image.setImageResource(R.drawable.aqua)
            "yellow" -> image.setImageResource(R.drawable.yellow)
        }

        /* Initialise the incubation timer when 'Incubate' button is pressed*/
        incubate.setOnClickListener {
                val mCountDownTimer: CountDownTimer
                var i = 0

            /* Change to appropriate hat & scarf picture */
            when(eggColour) {
                "red" -> image.setImageResource(R.drawable.red_incubating)
                "orange" -> image.setImageResource(R.drawable.orange_incubating)
                "green" -> image.setImageResource(R.drawable.green_incubating)
                "purple" -> image.setImageResource(R.drawable.purple_incubating)
                "aqua" -> image.setImageResource(R.drawable.aqua_incubating)
                "yellow" -> image.setImageResource(R.drawable.yellow_incubating)
            }

            /* Adjust picture size */
            image.layoutParams.height = 472
            image.layoutParams.width = 392

            val progressBar = findViewById<ProgressBar>(R.id.progressBar)
            progressBar.scaleY  = 5f;

            /* Make the progress bar go up with time smoothly */
                progressBar.progress = i
                mCountDownTimer = object : CountDownTimer(10000, 500) {

                    override fun onTick(millisUntilFinished: Long) {
                        i++
                        progressBar.progress = i * 100 / (10000 / 500)
                    }

                    override fun onFinish() {
                        /* Enable the 'Hatch' button when incubation finishes */
                        hatch.visibility = View.VISIBLE
                        i++
                        progressBar.progress = 100

                    }
                }
                mCountDownTimer.start()
            /* Delete the 'Incubate' button */
            incubate.visibility = View.GONE
            }

        /* Go to the dragon's page */
        hatch.setOnClickListener {
            val intent = Intent(this@EggActivity,DragonPage::class.java)
            intent.putExtra("EggColour", eggColour)
            startActivity(intent)
        }
    }

}




