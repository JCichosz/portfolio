package equinox.scales

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.view.animation.DecelerateInterpolator
import android.animation.ObjectAnimator
import android.app.AlertDialog
import android.content.Intent
import android.view.animation.AccelerateInterpolator


class DragonPage : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dragon_page)
        window.decorView.setBackgroundColor(getColor(R.color.darkGrey))
        title = ""

        /* Get the chosen colour */
        val eggColour = intent.getStringExtra("EggColour")

        val image = findViewById<ImageView>(R.id.imageView5)

        /* Set the dragon image to the approptriate colour */
        when(eggColour) {
            "red" -> image.setImageResource(R.drawable.red_dragon)
            "orange" -> image.setImageResource(R.drawable.orange_dragon)
            "green" -> image.setImageResource(R.drawable.green_dragon)
            "purple" -> image.setImageResource(R.drawable.purple_dragon)
            "aqua" -> image.setImageResource(R.drawable.aqua_dragon)
            "yellow" -> image.setImageResource(R.drawable.yellow_dragon)
        }

        val hunger = findViewById<ProgressBar>(R.id.hungerBar)
        val entertainment = findViewById<ProgressBar>(R.id.entertainmentBar)
        val health = findViewById<ProgressBar>(R.id.healthBar)

        hunger.max = 5
        entertainment.max = 5
        health.max = 5

        hunger.progress = hunger.max
        entertainment.progress = entertainment.max
        health.progress = health.max

        hunger.scaleY  = 5f;
        entertainment.scaleY  = 5f;
        health.scaleY  = 5f;

        var dead = false

        /* Continuously start the timer to decrease the dragon stats  */
        object : CountDownTimer(3000, 100) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                /* Decrease stats on theve timer finish */
                hunger.progress -= 1
                entertainment.progress -= 1

                /* Decrease dragon health if it is bored and hungry */
                if(hunger.progress == 0 && entertainment.progress == 0) {
                    if(health.progress > 0) {
                        health.progress -= 1;
                    }

                    /* If dragon has no health left - display the death screen and prompt to
                    * choose a new egg */
                    if(health.progress <= 0 && !dead) {
                        dead = true

                        val builder = AlertDialog.Builder(this@DragonPage)
                        val deathView = layoutInflater.inflate(R.layout.dragon_death, null)
                        val popup = deathView.findViewById<Button>(R.id.chooseNew)

                        builder.setView(deathView)
                        val dialog = builder.create()
                        dialog.show()
                        popup.setOnClickListener {
                            val intent = Intent(this@DragonPage, EggChoice::class.java)
                            startActivity(intent)
                        }
                    }
                }
                this.start()
            }

        }.start()

        /* Restore the hunger meter when the 'Feed' button is pressed */
        var feed = findViewById<Button>(R.id.button6)
        feed.setOnClickListener {
            hunger.progress = hunger.max

            /* Restore health when both needs have been refilled */
            if(hunger.progress == hunger.max && entertainment.progress == entertainment.max) {
            health.progress = health.max;
            }
        }

        var play = findViewById<Button>(R.id.button2)
        /* Restore the entertainment meter when the 'Play' button is pressed */
        play.setOnClickListener {
            entertainment.progress = entertainment.max

            /* Restore health when both needs have been refilled */
            if(hunger.progress == hunger.max && entertainment.progress == entertainment.max) {
                health.progress = health.max;
            }
        }
    }


}
