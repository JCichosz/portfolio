﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingMove : MonoBehaviour {

    public Transform horse;
    Animator anim;
    int animState = Animator.StringToHash("State");
    int animMultiplier = Animator.StringToHash("animMultiplier");
    public float speed;
    public bool gallop = false;

	void Start () {
        anim = GetComponent<Animator>();
    }
	
	void Update () {
        horse.transform.Translate(new Vector3(0,0,1) * speed * Time.deltaTime);

        if (gallop)
        {
            anim.SetInteger(animState, 2);
            anim.SetFloat(animMultiplier, speed * 0.2f);
        }
        else
        {
            anim.SetInteger(animState, 1);
            anim.SetFloat(animMultiplier, speed * 0.4f);
        }


    }
}
