﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Joint : Component {

    public Transform jointTransform;
    public Quaternion initialRotation;
    public JointConstraints constraints;

    public Joint(Transform o, Quaternion q, JointConstraints c)
    {
        jointTransform = o;
        initialRotation = q;
        constraints = c;
    }

}
