﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

/* Collects data about current run and writes results to csv. */
public class ResetHorse : MonoBehaviour {

    public Vector3 initialHorsePos = new Vector3(10, 0, 4);
    private int maxRuns = 10; // number of runs on a particular slope
    private int completedRuns = 0;
    Animator anim;
    public Transform horse;
    public static List<FrameData> results = new List<FrameData>();

    // Struct describing the error values of all legs.
    public struct LegConstraints
    {
        public LegError frontLeft;
        public LegError frontRight;
        public LegError backLeft;
        public LegError backRight;

        public LegConstraints(LegError fl, LegError fr, LegError bl, LegError br)
        {
            frontLeft = fl;
            frontRight = fr;
            backLeft = bl;
            backRight = br;
        }
    }

    public struct LegError
    {
        public bool within; // is the position within constraints
        public bool ikApplied; // was IK applied
        public int iterations; // how many iterations performed on a leg
        public float error; // by how much exceeded constraints (max value from all joints in leg)

        public LegError(bool r, bool ik, int num, float err)
        {
            within = r;
            ikApplied = ik;
            iterations = num;
            error = err;
        }
     }

    public struct FrameData
    {
       public float time; // time taken by the algorithm in msec

        public int iterations; // total iterations performed in this frame
        public LegConstraints leg; // data for all legs

        public FrameData(float t, int i, LegConstraints res)
        {
            time = t;
            iterations = i;
            leg = res;
        }
    }

    void Start()
    {
        anim = horse.GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        // If not completed desired number of runs -> reset to start position & go again
        if (completedRuns < maxRuns)
        {
            other.gameObject.transform.position = initialHorsePos;
            anim.Play(0, -1, 0f); // reset animation to always start with the same setup
            completedRuns++;

            string scene = SceneManager.GetActiveScene().name;
            string path = "../Test Results/example/Walking/" + scene + "/" + "run" + completedRuns + ".csv";

            if (!File.Exists(path))
            {
                var fs = new FileStream(path,FileMode.Create);
                fs.Dispose();
            }

            using (StreamWriter writer = new StreamWriter(path))
            {
                string header = "Msec,Iterations,Front left,IK Applied,Iterations,Max error,Front right,IK Applied,Iterations,Max error,Back left,IK Applied,Iterations,Max error,Back Right,IK Applied,Iterations,Max error";
                writer.WriteLine(header);

                foreach(FrameData frame in results)
                {
                    string line = frame.time + "," + frame.iterations + ","
                        + frame.leg.frontLeft.within + "," + frame.leg.frontLeft.ikApplied + "," + frame.leg.frontLeft.iterations + "," + frame.leg.frontLeft.error + ","
                        + frame.leg.frontRight.within + "," + frame.leg.frontRight.ikApplied + "," + frame.leg.frontRight.iterations + "," + frame.leg.frontRight.error + ","
                        + frame.leg.backLeft.within + "," + frame.leg.backLeft.ikApplied + "," + frame.leg.backLeft.iterations + "," + frame.leg.backLeft.error + ","
                        + frame.leg.backRight.within + "," + frame.leg.backRight.ikApplied + "," + frame.leg.backRight.iterations + "," + frame.leg.backRight.error;
                    writer.WriteLine(line);
                }             
            }
            results.Clear();
        }
        else
        {
            int total = SceneManager.sceneCountInBuildSettings;
            int current = SceneManager.GetActiveScene().buildIndex;

            if ((current + 1) < total) // go through every scene
            {
                SceneManager.UnloadSceneAsync(current);
                SceneManager.LoadScene(current + 1);
            }
            else Application.Quit(); 
            
       }
    }

    public void AddToResults(FrameData fd)
    {
        results.Add(fd);
    }
}
