﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class HorseLocomotion : MonoBehaviour
{
    public Transform horse;       
    public Transform frontLeftLeg; 
    public Transform frontRightLeg;
    public Transform backLeftLeg;
    public Transform backRightLeg;

    public Transform spine;
    public Transform hips;
    public Transform root;

    public int maxIterations; // maximum number of iterations
    public const float TOLERANCE = 0.01f; // error tolerance - when joint is 'close enough' but not at exact target

    List<Joint> joints = new List<Joint>(); 

    public GameObject prefab; // Prefab that can be spawned at the target point etc.

    // Arrays to hold all children objects of a given leg
    Transform[] frontLeftchildren;
    Transform[] frontRightChildren;
    Transform[] backLeftChildren;
    Transform[] backRightChildren;

    // A list of joints to be adjusted in each leg
    List<Joint> frontLeftJoints = new List<Joint>();
    List<Joint> frontRightJoints = new List<Joint>();
    List<Joint> backLeftJoints = new List<Joint>();
    List<Joint> backRightJoints = new List<Joint>();

    // List of all end effectors (hooves)
    List<Transform> endEffectors = new List<Transform>();

    // Used for keeping track of leg iterations
    private int iterations;

    int animState = Animator.StringToHash("State");
    Animator anim;

    Stopwatch stopwatch = new Stopwatch();

    void Start()
    {
        anim = GetComponent<Animator>();

        // Populate arrays of children objects
        frontLeftchildren = frontLeftLeg.GetComponentsInChildren<Transform>();
        frontRightChildren = frontRightLeg.GetComponentsInChildren<Transform>();
        backLeftChildren = backLeftLeg.GetComponentsInChildren<Transform>();
        backRightChildren = backRightLeg.GetComponentsInChildren<Transform>();

        // Look through the lists of children to identify joints in each leg and end effectors 
        PopulateChildren(frontLeftJoints, frontLeftchildren);
        PopulateChildren(frontRightJoints, frontRightChildren);
        PopulateChildren(backLeftJoints, backLeftChildren);
        PopulateChildren(backRightJoints, backRightChildren);
    }

    void LateUpdate()
    {
        stopwatch.Start();

        AlignBodyToSlope();

        // Correct rotation to slope
        frontLeftLeg.transform.rotation = CorrectLegRotation(frontLeftLeg.transform);
        frontRightLeg.transform.rotation = CorrectLegRotation(frontRightLeg.transform);
        backLeftLeg.transform.rotation = CorrectLegRotation(backLeftLeg.transform);
        backRightLeg.transform.rotation = CorrectLegRotation(backRightLeg.transform);

        bool flIK = false;
        bool frIK = false;
        bool blIK = false;
        bool brIK = false;

        int flIterations = 0;
        int frIterations = 0;
        int blIterations = 0;
        int brIterations = 0;
        
        // For each end effector
        for (int i = 0; i < endEffectors.Count; i++)
        {
            // Raycast down, if didn't hit anything -> raycast up as leg might be under terrain
            RaycastHit ground;
            bool raycastUp = false;
            Physics.Raycast(endEffectors[i].position, Vector3.down, out ground);
            if (ground.collider == null)
            {
                raycastUp = true;
                Physics.Raycast(endEffectors[i].position, Vector3.up, out ground);
            }

            // If distance to ground is small enough or raycast upwards
            if (ground.distance < 0.1099f || raycastUp)
            {
                Transform endEffector = endEffectors[i]; // Identify which hoof it is
                List<Joint> joints = IdentifyLeg(endEffector.name); // Find the joints of the appropriate leg

                // If horse standing still - raycast from hip.shoulder to imitate balancing on slope
                RaycastHit hit;
                if (anim.GetInteger(animState) == 0) hit = CastRayToGround(joints[0].jointTransform);
                else hit = ground;

                Vector3 target = hit.point; // set the IK target to where raycast hit the ground
                Vector3 normal = hit.normal;

                // Can instantiate a prefab at the target hoof position
                //Instantiate(prefab, target, Quaternion.FromToRotation(Vector3.up, normal));

                iterations = 0; // reset iteration for each leg
                bool done = false;

                while (iterations < maxIterations && !done)
                {
                    // if the end effector is farther away from target than allowed -> do IK
                    if (Vector3.Distance(endEffector.position, target) > TOLERANCE)
                    {
                        if (joints.Count > 0) // Make sure there are joints to be modified
                        {
                            // Do CCD from top to bottom 
                            for (int j = 0; j < joints.Count - 1; j++)
                            {
                                SolveCCD(endEffector.position, target, joints[j].jointTransform);
                                CheckJointRestrictions(joints);
                            }
                            // if it's a front leg, do CCD bottom to top to better position legs
                            if (endEffector.name == "FrontLeftHoofCentre" || endEffector.name == "FrontRightHoofCentre")
                            {
                                if (anim.GetInteger(animState) != 0) // only if not stationary
                                {
                                    for (int j = joints.Count - 1; j >= 0; j--)
                                    {
                                        SolveCCD(endEffector.position, target, joints[j].jointTransform);
                                        CheckJointRestrictions(joints);
                                    }
                                }
                            }
                            // align hoof to the ground surface 
                            AlignHoof(hit.normal, joints[joints.Count - 1].jointTransform);
                            CheckJointRestrictions(joints);
                            iterations++;

                            if (endEffector.name == "FrontLeftHoofCentre")
                            {
                                flIK = true;
                                flIterations = iterations;
                            }
                            else if (endEffector.name == "FrontRightHoofCentre")
                            {
                                frIK = true;
                                frIterations = iterations;
                            }
                            else if (endEffector.name == "BackLeftHoofCentre")
                            {
                                blIK = true;
                                blIterations = iterations;
                            }
                            else if (endEffector.name == "BackRightHoofCentre")
                            {
                                brIK = true;
                                brIterations = iterations;
                            }
                        }
                    }
                    else done = true;                                   
                }
            }
        }

        float msec = stopwatch.ElapsedMilliseconds;
        stopwatch.Stop();
        stopwatch.Reset();
        ResetHorse.LegError fl = WithinConstraints(frontLeftJoints, flIterations, flIK);
        ResetHorse.LegError fr = WithinConstraints(frontRightJoints, frIterations, frIK);
        ResetHorse.LegError bl = WithinConstraints(backLeftJoints, blIterations, blIK);
        ResetHorse.LegError br = WithinConstraints(backRightJoints, brIterations, brIK);

        int total = flIterations + frIterations + blIterations + brIterations;

        ResetHorse.results.Add(new ResetHorse.FrameData(msec, total, new ResetHorse.LegConstraints(fl, fr, bl, br)));
    }

    private void SolveCCD(Vector3 endEffector, Vector3 target, Transform joint)
    {
        // find vectors from joint to end effector and target
        Vector3 vectorToEndEffector = endEffector - joint.position;
        Vector3 vectorToDesiredPos = target - joint.position;

        vectorToEndEffector.Normalize();
        vectorToDesiredPos.Normalize();

        float cos = Vector3.Dot(vectorToDesiredPos, vectorToEndEffector);

        Vector3 crossResult = Vector3.Cross(vectorToEndEffector, vectorToDesiredPos).normalized;
        float angleToTurn = Mathf.Acos(cos) * Mathf.Rad2Deg;

        if (cos < 0.9999f) // cos = 1, when angle == 0 (no need to turn by 0 degrees)
        {
            Quaternion angleToQuat = Quaternion.AngleAxis(angleToTurn, crossResult);
            Quaternion newRot = angleToQuat * joint.rotation;
            joint.rotation = newRot;
        }
    }

    private ResetHorse.LegError WithinConstraints(List<Joint> joints, int num, bool ik)
    {
        float largestErr = 0.0f;
        for (int i = 0; i < joints.Count; i++)
        {
            float rotX = joints[i].jointTransform.localEulerAngles.x;

            if (rotX > 180)
            {
                rotX -= 360;
            }
            if (rotX > (joints[i].constraints.maxX + 0.001f) || rotX < (joints[i].constraints.minX - 0.001f))
            {
                float err;
                if(rotX < joints[i].constraints.minX)
                {
                    err = joints[i].constraints.minX - rotX;
                }
                else
                {
                    err = rotX - joints[i].constraints.maxX;
                }
                if (err > largestErr) largestErr = err;
            }
        }
        if (largestErr > 0.5f) { 
            return new ResetHorse.LegError(false, ik, num, largestErr);
        }
        else return new ResetHorse.LegError(true, ik, num, largestErr);
    }

    /* Rotates the leg away from the slope by half the elevation */
    private Quaternion CorrectLegRotation(Transform t)
    {
        RaycastHit ground = CastRayToGround(t);
        Quaternion elevation = Quaternion.FromToRotation(Vector3.up, ground.normal);
        Vector3 newEuler;
        float rotX = elevation.eulerAngles.x;
        float rotY = elevation.eulerAngles.y;
        float rotZ = elevation.eulerAngles.z;

        if (rotX > 180)
        {
            rotX -= 360;
        }

        if (rotY > 180)
        {
            rotY -= 360;
        }

        if (rotZ > 180)
        {
            rotZ -= 360;
        }
        newEuler.x = rotX * 0.5f;
        newEuler.y = rotY;
        newEuler.z = rotZ;
        Quaternion half = Quaternion.Euler(newEuler);
        Quaternion finalRot = t.rotation * Quaternion.Inverse(half);
        return finalRot;
    }

    private RaycastHit CastRayToGround(Transform t)
    {
        RaycastHit groundHit;
        Physics.Raycast(t.position, Vector3.down, out groundHit);
        return groundHit;
    }

    /* Aligns body to slope and corrects positioning relative to ground */
    private void AlignBodyToSlope()
    {
        RaycastHit frontHit;
        RaycastHit backHit;
        Physics.Raycast(spine.position, Vector3.down, out frontHit);
        Physics.Raycast(hips.position, Vector3.down, out backHit);
        Vector3 added = frontHit.normal + backHit.normal;
        Vector3 mediumNormal = added / 2;
        Quaternion tilt = Quaternion.FromToRotation(Vector3.up, mediumNormal);

        Quaternion interpolated = Quaternion.Slerp(horse.transform.rotation, tilt, Time.deltaTime * 10.0f);
        horse.transform.rotation = interpolated;

        /* Correct vertical position of the horse on the slope 
         * (prevent going 'into' ground or floating). */
        RaycastHit ground;
        bool up = false;
        Physics.Raycast(root.position, Vector3.down, out ground);
        if (ground.collider == null)
        {
            Physics.Raycast(root.position, Vector3.up, out ground);
            up = true;
        }

        if (anim.GetInteger(animState) != 0)
        {
            if (ground.distance > 0.02)
            {
                if (!up) root.transform.Translate(-ground.normal * ground.distance * 0.5f);
                else root.transform.Translate(ground.normal * ground.distance * 0.5f);
            }
        }
       
    }

    /* Clamp rotations to be wihin allowed range */
    private void CheckJointRestrictions(List<Joint> joints)
    {
        for (int i = 0; i < joints.Count; i++)
        {
            Vector3 newAngles;

            float rotationX = joints[i].jointTransform.localEulerAngles.x;
            float rotationY = joints[i].jointTransform.localEulerAngles.y;
            float rotationZ = joints[i].jointTransform.localEulerAngles.z;

            if (rotationX > 180)
            {
                rotationX -= 360;
            }

            if (rotationY > 180)
            {
                rotationY -= 360;
            }

            if (rotationZ > 180)
            {
                rotationZ -= 360;
            }

            rotationY = Mathf.Clamp(rotationY, joints[i].constraints.minY, joints[i].constraints.maxY);
            rotationZ = Mathf.Clamp(rotationZ, joints[i].constraints.minZ, joints[i].constraints.maxZ);
            rotationX = Mathf.Clamp(rotationX, joints[i].constraints.minX, joints[i].constraints.maxX);
            newAngles.x = rotationX;
            newAngles.y = rotationY;
            newAngles.z = rotationZ;

            joints[i].jointTransform.localEulerAngles = newAngles;
            float k = newAngles.x;
        }
    }

    private List<Joint> IdentifyLeg(string name)
    {
        if (name == "FrontLeftHoofCentre")
        {
            return frontLeftJoints;
        }
        else if (name == "FrontRightHoofCentre")
        {
            return frontRightJoints;
        }
        else if (name == "BackLeftHoofCentre")
        {
            return backLeftJoints;
        }
        else if (name == "BackRightHoofCentre")
        {
            return backRightJoints;
        }
        else return new List<Joint>();
    }

    private void AlignHoof(Vector3 averageNormal, Transform t)
    {
        var tilt = Quaternion.FromToRotation(Vector3.up, averageNormal);

        Quaternion intepolated = Quaternion.Slerp(t.rotation, tilt, Time.deltaTime * 20.0f);

        Vector3 eulerAngles = new Vector3(intepolated.eulerAngles.x,
            transform.rotation.eulerAngles.y,
            intepolated.eulerAngles.z);
        Quaternion newRot = Quaternion.Euler(eulerAngles);
        t.rotation = newRot;
    }

    /* Find all joints to be adjusted on the given limb and identify end effectors */
    private void PopulateChildren(List<Joint> joints, Transform[] children)
    {
        foreach (Transform child in children)
        {
            if (child.tag == "Joint")
            {
                JointConstraints constraints = JointConstraints.FindConstraints(child.name);
                Joint temp = new Joint(child.transform, child.rotation, constraints);
                joints.Add(temp);
            }
            if (child.tag == "EndEffector")
            {
                endEffectors.Add(child);
            }
        }
    }
}

