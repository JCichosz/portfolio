﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointConstraints : Component {

    public const float SHOULDER_MAX_X       = 35.0f;
    public const float SHOULDER_MIN_X       = -100.0f;
    //public const float ELBOW_MAX_X          = 145.0f; // Used model has no elbow
    //public const float ELBOW_MIN_X          = 85.0f;
    public const float CARPUS_MAX_X         = 146.7f;
    public const float CARPUS_MIN_X         = -5.3f;
    public const float FRONT_FETLOCK_MAX_X  = 97.3f;
    public const float FRONT_FETLOCK_MIN_X  = -52.7f;

    public const float HIP_MAX_X            = 29.0f;
    public const float HIP_MIN_X            = -24.3f;
    public const float STIFLE_MAX_X         = 62.8f;
    public const float STIFLE_MIN_X         = -44.9f;
    public const float HOCK_MAX_X           = 24.5f; 
    public const float HOCK_MIN_X           = -108.5f;
    public const float BACK_FETLOCK_MAX_X   = 115.2f;
    public const float BACK_FETLOCK_MIN_X   = -49.8f;

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public float minZ;
    public float maxZ;

    public JointConstraints()
    {
        minX = 0.0f;
        maxX = 360.0f;
        minY = 0.0f;
        maxY = 360.0f;
        minZ = 0.0f;
        maxZ = 360.0f;
    }

    public JointConstraints(float xMin, float xMax, float yMin, float yMax, float zMin, float zMax)
    {
        minX = xMin;
        maxX = xMax;
        minY = yMin;
        maxY = yMax;
        minZ = zMin;
        maxZ = zMax;
    }

    public static JointConstraints FindConstraints(string name)
    {
        JointConstraints hip = new JointConstraints(HIP_MIN_X, HIP_MAX_X, -2, 2, -2, 2);
        JointConstraints stifle = new JointConstraints(STIFLE_MIN_X, STIFLE_MAX_X, -2, 2, -2, 2);
        JointConstraints hock = new JointConstraints(HOCK_MIN_X, HOCK_MAX_X, -2, 2, -2, 2);
        JointConstraints backFetlock = new JointConstraints(BACK_FETLOCK_MIN_X, BACK_FETLOCK_MAX_X, -2, 2, -2, 2);
        JointConstraints shoulder = new JointConstraints(SHOULDER_MIN_X, SHOULDER_MAX_X, -2, 2, -2, 2);
        //JointConstraints elbow = new JointConstraints(ELBOW_MIN_X, ELBOW_MAX_X, -2, 2, -2, 2);
        JointConstraints carpus = new JointConstraints(CARPUS_MIN_X, CARPUS_MAX_X, -2, 2, -2, 2);
        JointConstraints frontFetlock = new JointConstraints(FRONT_FETLOCK_MIN_X, FRONT_FETLOCK_MAX_X, -2, 2, -2, 2);

        switch (name)
        {
            case "LeftHip":             return hip;
            case "LeftStifle":          return stifle;
            case "LeftHock":            return hock;
            case "LeftBackFetlock":     return backFetlock;
            case "LeftShoulder":        return shoulder;
            //case "LeftElbow":           return elbow;
            case "LeftCarpus":          return carpus;
            case "LeftFrontFetlock":    return frontFetlock;
            case "RightHip":            return hip;
            case "RightStifle":         return stifle;
            case "RightHock":           return hock;
            case "RightBackFetlock":    return backFetlock;
            case "RightShoulder":       return shoulder;
            //case "RightElbow":          return elbow;
            case "RightCarpus":         return carpus;
            case "RightFrontFetlock":   return frontFetlock;
            default: {
                    Debug.Log("Constraints for joint " + name + " not found.");
                    return new JointConstraints();
                     }
        }
    }
}