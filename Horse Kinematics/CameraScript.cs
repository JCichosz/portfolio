﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public GameObject horse;
    private Vector3 positionOffset;

    void Start () {
        positionOffset = transform.position - horse.transform.position;
	}
	
    void Update()
    {
       float newX = horse.transform.position.x + positionOffset.x;
       float newY = horse.transform.position.y + positionOffset.y;
       float newZ = horse.transform.position.z + positionOffset.z;

       transform.position = new Vector3(newX, newY, newZ);
    }
}
