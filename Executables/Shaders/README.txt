To switch between shaders, press keys 1 to 8. 

Each key corresponds to a shader as follows:
1. Shrinking cube
2. Texture blending
3. Fading effect
4. Breaking into pieces
5. Decreasing tessellation
6. Laser beam creating a crater
7. Day/night cycle using Phong
8. Real time different colour lighting