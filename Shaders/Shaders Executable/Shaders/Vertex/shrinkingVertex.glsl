#version 330 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform float time;

in  vec3 position;
in  vec2 texCoord;
in  vec4 colour;
in	vec3 normal;

out Vertex	{
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} OUT;

// A vertex shader changing the position of vertices to apper like the cube is shrinking.
void main(void)	{
	float colourChange = sin(time);
	float vanish = 8.0f; // time it takes the cube to vanish
	float startSize = 1.0f; // starting size

	float diff = vanish - time; // current time difference
	float size = startSize / vanish; 

	float scaleValue;
	if (diff > 0) {	// if there is time difference, set scale based on time difference
		scaleValue = size * diff;
	} else scaleValue = 0; // if over the vanish time, stop rendering the cube
	
	mat4 scale = mat4(scaleValue); // scaling matrix
	scale[3][3] = 1.0f;

	// change colours over time
	vec4 newColour = vec4((colour.x * colourChange), (colour.y * cos(time)), (colour.z * (pow(colourChange,2))), 1.0);
	
	OUT.texCoord	= texCoord;
	OUT.colour		= newColour;
	gl_Position		= projMatrix * viewMatrix * modelMatrix * scale * vec4(position, 1.0);

	mat3 normalMatrix = transpose(inverse(mat3(modelMatrix)));

	OUT.normal = normalize(normalMatrix * normalize(normal));
}