#version 400 core

layout(triangles, equal_spacing, ccw) in;

uniform sampler2D pattern;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform float time;

in Vertex	{
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} IN[];

out Vertex {
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} OUT;

float maxIndent = 5;

// Tessellation shader that indents one face of a cube based on
// a texture (pattern) to create a crater.
void main(void) {
	vec3 p0 = gl_TessCoord.x * gl_in[0].gl_Position.xyz;
	vec3 p1 = gl_TessCoord.y * gl_in[1].gl_Position.xyz;
	vec3 p2 = gl_TessCoord.z * gl_in[2].gl_Position.xyz;

	vec3 combinedPos = p0 + p1 + p2;
	vec4 worldPos = modelMatrix * vec4(combinedPos, 1);

	vec4 c0 = gl_TessCoord.x * IN[0].colour;
	vec4 c1 = gl_TessCoord.y * IN[1].colour;
	vec4 c2 = gl_TessCoord.z * IN[2].colour;

	vec4 combinedColour = c0 + c1 + c2;

	vec2 t0 = gl_TessCoord.x * IN[0].texCoord;
	vec2 t1 = gl_TessCoord.y * IN[1].texCoord;
	vec2 t2 = gl_TessCoord.z * IN[2].texCoord;

	vec2 combinedTexCoord = t0 + t1 + t2;

	vec3 n0 = gl_TessCoord.x * IN[0].normal;
	vec3 n1 = gl_TessCoord.y * IN[1].normal;
	vec3 n2 = gl_TessCoord.z * IN[2].normal;

	vec3 combinedNormal = n0 + n1 + n2;

	OUT.colour = combinedColour;
	OUT.texCoord = combinedTexCoord;
	OUT.normal = combinedNormal;

	// Only for cube face made out of these two triangles
	if (gl_PrimitiveID == 4 || gl_PrimitiveID == 5) {

		float height = texture(pattern, OUT.texCoord).x; // read in the texture
		float heightTime = height*time; // set the height based on time

		// If the height would go above maximum set height
		if (height*time > maxIndent) {
			worldPos.z -= maxIndent; // set z to maximum indentation
		} else worldPos.z -= height*time; // else increase the indentation
	}

	OUT.worldPos = worldPos.xyz;
	gl_Position = projMatrix * viewMatrix * worldPos;
}