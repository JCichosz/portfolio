#include "Renderer.h"

float inner[] = { 1.0f, 2.0f };
float outer[] = { 4.0f, 4.0f, 2.0f, 2.0f };

Renderer::Renderer(Window &parent) : OGLRenderer(parent) {
	time = 0;
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPatchParameterfv(GL_PATCH_DEFAULT_INNER_LEVEL, inner);
	glPatchParameterfv(GL_PATCH_DEFAULT_OUTER_LEVEL, outer);
}

Renderer::~Renderer(void)	{
	 
}

void	Renderer::RenderScene() {
	for(vector<RenderObject*>::iterator i = renderObjects.begin(); i != renderObjects.end(); ++i ) {
		Render(*(*i));
	}
}

void	Renderer::Render(const RenderObject &o) {
	modelMatrix = o.GetWorldTransform();
	Matrix3 rotation = Matrix3(viewMatrix);
	Vector3 invCamPos = viewMatrix.GetPositionVector();
	Vector3 cameraPos = rotation * -invCamPos;

	if(o.GetShader() && o.GetMesh()) {
		GLuint program = o.GetShader()->GetShaderProgram();
		
		glUseProgram(program);
		UpdateShaderMatrices(program);
		glUniform3fv(glGetUniformLocation(program, "cameraPos"), 1, (float*)&cameraPos);
		ApplyShaderLight(program);
		UpdateTimer(program);
		o.Draw();
	}

	for(vector<RenderObject*>::const_iterator i = o.GetChildren().begin(); i != o.GetChildren().end(); ++i ) {
		Render(*(*i));
	}
}

void Renderer::UpdateTimer(GLuint program) {
	glUniform1f(glGetUniformLocation(program, "time"), time);
}

void Renderer::ResetTime() {
	time = 0;
}

void	Renderer::UpdateScene(float msec) {
	time += msec * 0.001f;
	for(vector<RenderObject*>::iterator i = renderObjects.begin(); i != renderObjects.end(); ++i ) {
		(*i)->Update(msec);
		
	}
}

GLuint Renderer::LoadTexture(const string& filename) {
	GLuint texName = SOIL_load_OGL_texture(filename.c_str(), SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);

	if (texName == 0) {
		cout << "Texture file " << filename << " failed to load!" << std::endl;
	}

	return texName;
}

void Renderer::SetTextures(string tex, string tex2, string tex3) {
	
	glActiveTexture(GL_TEXTURE0);
	GLuint clearName = LoadTexture(tex);
	glBindTexture(GL_TEXTURE_2D, clearName);

	glActiveTexture(GL_TEXTURE1);
	GLuint damagedName = LoadTexture(tex2);
	glBindTexture(GL_TEXTURE_2D, damagedName);

	glActiveTexture(GL_TEXTURE2);
	GLuint patternName = LoadTexture(tex3);
	glBindTexture(GL_TEXTURE_2D, patternName);
}
