#version 400 core

layout(vertices = 3) out;

uniform float tessLevelInner;
uniform float tessLevelOuter;
uniform float time;

in Vertex {
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} IN[];

out Vertex {
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} OUT[];

// Creates heavily tessellated triangles at the start. As
// time passes, less and less tesselation occurs.
void main(void) {

	int numVertices;
	float fade = 15.0; // time the cube should fade in
	float diff = fade - time; // how much time until vanishing left
	if (time > fade) numVertices = 0; // stop rendering vertices when after vanish time
	else numVertices = int(diff * 8); // as difference gets smaller with time, less and less tessellation occurs

	gl_TessLevelInner[0] = numVertices;
	gl_TessLevelInner[1] = numVertices;

	gl_TessLevelOuter[0] = numVertices;
	gl_TessLevelOuter[1] = numVertices;
	gl_TessLevelOuter[2] = numVertices;

	OUT[gl_InvocationID].texCoord = IN[gl_InvocationID].texCoord;
	OUT[gl_InvocationID].colour = IN[gl_InvocationID].colour;
	OUT[gl_InvocationID].worldPos = IN[gl_InvocationID].worldPos;
	OUT[gl_InvocationID].normal = IN[gl_InvocationID].normal;

	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

}