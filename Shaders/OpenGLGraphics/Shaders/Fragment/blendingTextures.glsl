#version 330 core

uniform sampler2D clear;
uniform sampler2D damaged;
uniform float time;

in Vertex {
	smooth vec2 texCoord;
	smooth vec4 colour;
} IN;

out vec4 fragColour;

// Blends two textures together based on time
void main(void) {
	vec4 clearColour = texture(clear, IN.texCoord);
	vec4 damagedColour = texture(damaged, IN.texCoord);

	float blend = time * 0.3;
	
	// Prevent going over 1
	if (blend > 1) {
		blend = 1;
	}
	fragColour = mix(clearColour, damagedColour, blend);
}