#version 330 core

uniform sampler2D tex0;
uniform float time;

in Vertex {
	smooth vec2 texCoord;
	smooth vec4 colour;
} IN;

out vec4 fragColour;

// A fragment shader that increeases the alpha value over time 
void main(void) {
	
	float fade = 1 - (time * 0.2);

	if (fade < 0) {
		fade = 0;
	}

	fragColour =  vec4(IN.colour.rgb, fade);
}