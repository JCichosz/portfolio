#version 330 core

uniform sampler2D tex;
uniform vec3 cameraPos;

uniform vec3 lightColour;
uniform vec3 lightPosition;
uniform float lightRadius;

in Vertex {
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} IN;

out vec4 fragColour;

// Tutorial Phong fragment shader
void main(void) {
	vec3 incident = normalize(lightPosition - IN.worldPos);
	vec3 viewDirection = normalize(cameraPos - IN.worldPos);
	vec3 halfDirection = normalize(incident + viewDirection);

	float distance = length(lightPosition - IN.worldPos);
	float attenuation = 1.0 - clamp(distance/lightRadius, 0.0, 1.0);

	float lambert = max(0.0, dot(incident, IN.normal));

	float rFactor = max(0.0, dot(halfDirection, IN.normal));
	float sFactor = pow(rFactor, 50.0);

	vec4 texColour = texture(tex, IN.texCoord);
	vec3 ambient = texColour.rgb * lightColour * 0.1;
	vec3 diffuse = texColour.rgb * lightColour * lambert * attenuation;
	vec3 specular = lightColour * sFactor * attenuation;
	
	fragColour = vec4(ambient+diffuse+specular, texColour.a);
}
