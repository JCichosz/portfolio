#version 330 core

layout(triangles) in;
layout(points, max_vertices = 93) out;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform float time;

in Vertex {
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;

} IN[];

out Vertex {
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} OUT;

// Spawns 3 points at every vertex. Used to show tessellation.
void main(void) {
	for(int i = 0; i < gl_in.length(); ++i) {
		
		gl_Position = gl_in[0].gl_Position + vec4(0.0, 0.0, 0.0, 0.0);
		OUT.colour = IN[i].colour;
		OUT.normal = IN[i].normal;

   	 	EmitVertex();
		EndPrimitive();

   	 	gl_Position = gl_in[0].gl_Position + vec4(0.1, 0.1, 0.1, 0.0);
		OUT.colour = IN[i].colour;
		OUT.normal = IN[i].normal;

   	 	EmitVertex();
    	EndPrimitive();

    	gl_Position = gl_in[0].gl_Position + vec4(-0.1, -0.1, -0.1, 0.0);
		OUT.colour = IN[i].colour;
		OUT.normal = IN[i].normal;

   	 	EmitVertex();
    	EndPrimitive();
	}
	
}
