#version 330 core

layout(triangles) in;
layout(triangle_Strip, max_vertices = 48) out;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform float time;

in Vertex {
	vec2 texCoord;
	vec4 colour;
} IN[];

out Vertex {
	vec2 texCoord;
	vec4 colour;
} OUT;

float change = time * 0.7;

vec2 tex1 = vec2(1, 0);
vec2 tex2 = vec2(0, 0);
vec2 tex3 = vec2(1, 1);
vec2 tex4 = vec2(0, 1);

// Creates a vertex, scales it based on time to
// make the cube appear as shrinking,
// and then calculates the final position.
void createVertex(vec3 pos, vec2 tex) {
	OUT.texCoord = tex;

	mat4 scale = mat4(change + 0.5);
	scale[3][3] = 1.0f;

	vec4 position = vec4(pos, 1.0);
	vec4 world = gl_in[0].gl_Position + position;
	vec4 newModelMatrix = modelMatrix * scale * world; 
	newModelMatrix.y -= time * 2;
	vec4 result = projMatrix * viewMatrix * newModelMatrix;
	gl_Position = result;

	EmitVertex();
}

// Creates a cube made out of 6 faces.
void createCube(float size) {
	createVertex(vec3(-size, size, size), tex1);
	createVertex(vec3(-size, -size, size), tex2);
	createVertex(vec3(size, size, size), tex3);
	createVertex(vec3(size, -size, size), tex4);

	createVertex(vec3(size, size, size), tex1);
	createVertex(vec3(size, -size, size), tex2);
	createVertex(vec3(size, size, -size), tex3);
	createVertex(vec3(size, -size, -size), tex4);

	createVertex(vec3(size, size, -size), tex1);
	createVertex(vec3(size, -size, -size), tex2);
	createVertex(vec3(-size, size, -size), tex3);
	createVertex(vec3(-size, -size, -size), tex4);
	
	createVertex(vec3(-size, size, -size), tex1);
	createVertex(vec3(-size, -size, -size), tex2);
	createVertex(vec3(-size, size, size), tex3);
	createVertex(vec3(-size, -size, size), tex4);

	createVertex(vec3(size, size, size), tex1);
	createVertex(vec3(size, size, -size), tex2);
	createVertex(vec3(-size, size, size), tex3);
	createVertex(vec3(-size, size, -size), tex4);
	
	createVertex(vec3(-size, -size, size), tex1);
	createVertex(vec3(-size, -size, -size), tex2);
	createVertex(vec3(size, -size, size), tex3);
	createVertex(vec3(size, -size, -size), tex4);

	EndPrimitive();
}

void main(void) {
	float vanish = 5.0f; // Time it takes for cubes to vanish
	float startSize = 1.2f; // Starting cube size

	float diff = vanish - time;  // How much time left until vanishing
	float size = startSize / vanish; // Start size

	float scaleValue;
	if (diff > 0) {
		scaleValue = size * diff; // cube size is based on time difference until vanishing
	} else scaleValue = 0; // if time > venish, make cube disappear

	createCube(scaleValue); // create cube with current size
}


