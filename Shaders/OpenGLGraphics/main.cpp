#include "Renderer.h"
#include "RenderObject.h"

#define FRAGMENT_SHADERS  "Shaders/Fragment/"
#define VERTEX_SHADERS "Shaders/Vertex/"
#define GEOMETRY_SHADERS "Shaders/Geometry/"
#define TESSELLATION_SHADERS "Shaders/Tessellation/"

#pragma comment(lib, "nclgl.lib")

void resetLightSource(Renderer &r) {
	r.SetShaderLight(Vector3(5, 50.0f, -1), Vector3(1, 1, 1), 200);
}

void main(void) {
	Window w = Window(800, 600);
	Renderer r(w);

	string pattern = "Textures/pattern.png";
	/* Textures from on https://www.pexels.com */
	string woodTex = "Textures/painted_wood_pexels.jpeg";
	string damagedWoodTex = "Textures/bleached_wood_pexels.jpeg";

	/* Vertex shaders */
	string basicVert = VERTEX_SHADERS"BasicVert.glsl";
	string passThroughVertex = VERTEX_SHADERS"passThroughVertex.glsl";
	//string passThroughVertexTess = VERTEX_SHADERS"passThroughTess.glsl";
	string shrinkingVertex = VERTEX_SHADERS"shrinkingVertex.glsl";
	string normalVertex = VERTEX_SHADERS"normalVertex.glsl";

	/* Fragment shaders */
	string basicFragment = FRAGMENT_SHADERS"basicFrag.glsl";
	string basicTextureFragment = FRAGMENT_SHADERS"basicTextureFrag.glsl";
	string blendingTextures = FRAGMENT_SHADERS"blendingTextures.glsl";
	string fadingFrag = FRAGMENT_SHADERS"fadingFrag.glsl";
	string phongFrag = FRAGMENT_SHADERS"phongFragment.glsl";
	string colourPhongFrag = FRAGMENT_SHADERS"colourfulLightPhongFrag.glsl";
	string phongUseColour = FRAGMENT_SHADERS"PhongFragColour.glsl";

	/* Geometry shaders */
	string fallingApartGeometry = GEOMETRY_SHADERS"fallingApartGeometry.glsl";
	string lineGeo = GEOMETRY_SHADERS"pointsGeoShader.glsl";

	/* Tessellation shaders */
	string basicTess = TESSELLATION_SHADERS"basicTessellation.glsl";
	string evaluationTess = TESSELLATION_SHADERS"evaluationTess.glsl";
	string deformTess = TESSELLATION_SHADERS"deformTessellation.glsl";
	string deformTessEvaluation = TESSELLATION_SHADERS"deformEvaluation.glsl";

	/* Shader presets */
	Shader* shrinkingSetup = new Shader(shrinkingVertex, basicFragment);
	Shader* texBlendSetup = new Shader(basicVert, blendingTextures);
	Shader* fadingSetup = new Shader(basicVert, fadingFrag);
	Shader* fallingApartSetup = new Shader(passThroughVertex, basicTextureFragment, fallingApartGeometry);
	Shader* dayNightCycle = new Shader(normalVertex, phongFrag);
	Shader* colourfulPhong = new Shader(shrinkingVertex, colourPhongFrag);
	Shader* tessellationSetup = new Shader(passThroughVertex, phongUseColour, lineGeo, basicTess, evaluationTess);
	Shader* laserSetup = new Shader(passThroughVertex, phongFrag, "", deformTess, deformTessEvaluation);

	Matrix4 initialMatrix = Matrix4::Translation(Vector3(0, 0, -10)) * Matrix4::Scale(Vector3(1, 1, 1)) *
		Matrix4::Rotation(30.0f, Vector3(0, 1.0, 0));

	Mesh* m = Mesh::LoadMeshFile("cube.asciimesh");
	RenderObject o(m, shrinkingSetup, 0);
	o.SetModelMatrix(initialMatrix);

	r.SetTextures(woodTex, damagedWoodTex, pattern);
	r.SetShaderLight(Vector3(5, 50.0f, -1), Vector3(1, 1, 1), 200);
	r.SetProjectionMatrix(Matrix4::Perspective(1, 100, 1.33f, 45.0f));
	r.SetViewMatrix(Matrix4::BuildViewMatrix(Vector3(0, 0, 0), Vector3(0, 0, -10)));
	r.AddRenderObject(o);

	while(w.UpdateWindow()) {
		float msec = w.GetTimer()->GetTimedMS();

		/* Shrinking cube */
		if (Keyboard::KeyTriggered(KEY_1)) {
			m->type = GL_TRIANGLES;
			o.SetModelMatrix(initialMatrix);
			r.ResetTime();
			o.SetShader(shrinkingSetup);
		}
		/* 'Clean' & 'destroyed' texture blend */
		if (Keyboard::KeyTriggered(KEY_2)) {
			m->type = GL_TRIANGLES;
			o.SetModelMatrix(initialMatrix);
			r.ResetTime();
			o.SetShader(texBlendSetup);
		}
		/* Fecresing alpha colour fade */
		if (Keyboard::KeyTriggered(KEY_3)) {
			m->type = GL_TRIANGLES;
			o.SetModelMatrix(initialMatrix);
			r.ResetTime();
			o.SetShader(fadingSetup);
		}
		/* Cube falling into pieces */
		if (Keyboard::KeyTriggered(KEY_4)) {
			m->type = GL_TRIANGLES;
			o.SetModelMatrix(initialMatrix);
			r.ResetTime();
			o.SetShader(fallingApartSetup);
		}
		/* Decreasing tessellation with phong lighting */
		if (Keyboard::KeyTriggered(KEY_5)) {
			m->type = GL_PATCHES;
			resetLightSource(r);
			o.SetModelMatrix(initialMatrix);
			r.ResetTime();
			o.SetShader(tessellationSetup);
		}
		/* Laset beam deforming a cube */
		if (Keyboard::KeyTriggered(KEY_6)) {
			m->type = GL_PATCHES;
			o.SetModelMatrix(initialMatrix);
			r.ResetTime();
			r.SetShaderLight(Vector3(1.0f, 0.0f, -9.0f), Vector3(1.0f, 0.3f, 0.3f), 15);
			o.SetShader(laserSetup);
		}
		/* Day/night cycle lighting */
		if (Keyboard::KeyTriggered(KEY_7)) {
			m->type = GL_TRIANGLES;
			o.SetModelMatrix(initialMatrix);
			r.ResetTime();
			o.SetShader(dayNightCycle);
		}
		/* Different colout lighting */
		if (Keyboard::KeyTriggered(KEY_8)) {
			m->type = GL_TRIANGLES;
			resetLightSource(r);
			o.SetModelMatrix(initialMatrix);
			r.ResetTime();
			o.SetShader(colourfulPhong);
		}
		/* Cube rotation for some shaders */
		if (o.GetShader() == dayNightCycle || o.GetShader() == colourfulPhong || o.GetShader() == tessellationSetup) {
			o.SetModelMatrix(o.GetModelMatrix() * Matrix4::Rotation(0.05f * msec, Vector3(0.5f, 0.1f, 0.5f)));
		}
		/* Change light position for day/night cycle */
		if (o.GetShader() == dayNightCycle) {
			r.SetShaderLight(Vector3(5, cos((r.GetTime())*0.5f) *50.0f, 10.0f), Vector3(1.0f, 1.0f, 1.0f), 50);
		}
		r.UpdateScene(msec);
		r.ClearBuffers();
		r.RenderScene();
		r.SwapBuffers();
	}

	delete m;
	delete shrinkingSetup;
	delete texBlendSetup;
	delete fadingSetup;
	delete fallingApartSetup;
	delete dayNightCycle;
	delete colourfulPhong;
	delete tessellationSetup;
	delete laserSetup;
}