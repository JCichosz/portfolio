#pragma once
#include "../nclgl/OGLRenderer.h"
#include "../nclgl/Matrix3.h"

#include "RenderObject.h"

#include <vector>

using std::vector;

struct Light {
	Vector3 position;
	float radius;
	Vector3 colour;
};

class Renderer : public OGLRenderer	{
public:
	Renderer(Window &parent);
	~Renderer(void);

	virtual void	RenderScene();

	virtual void Render(const RenderObject &o);
	virtual void UpdateTimer(GLuint program);
	virtual void UpdateScene(float msec);

	void	AddRenderObject(RenderObject &r) {
		renderObjects.push_back(&r);
	}
	void ResetTime();
	float GetTime() {return time;};
	GLuint LoadTexture(const string& filename);
	void SetTextures(string tex, string tex2, string tex3);
	void SetShaderLight(Vector3 position, Vector3 colour, float radius) {
		lighting.position = position;
		lighting.colour = colour;
		lighting.radius = radius;  
	}

protected:
	float time;
	Light lighting;
	vector<RenderObject*> renderObjects;
	void ApplyShaderLight(GLuint program) {
		glUniform3fv(glGetUniformLocation(program, "lightColour"), 1, (float*)&lighting.colour);
		glUniform3fv(glGetUniformLocation(program, "lightPosition"), 1, (float*)&lighting.position);
		glUniform1f(glGetUniformLocation(program, "lightRadius"), lighting.radius);
	}
};

