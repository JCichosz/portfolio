#include "PauseMenu.h"

namespace NCL {
	namespace CSC8503 {

		PauseMenu::PauseMenu(GameStatus* status, GolfGame* g): Menu(status, g) {
			CreateEntries();
		}


		PauseMenu::~PauseMenu() {
		}

		void PauseMenu::Update() {
			if (status->gameRunning == true) status->gameRunning = false;

			UpdateKeys();

			PrintMenu();
		}

		void PauseMenu::UpdateKeys() {
			UpdateNavigation();
			if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_RETURN)) {
				std::string entryText = menuEntries[activeEntry]->text;

				if (entryText == "RESUME GAME") {
					status->gameRunning = true;
					status->paused = false;
				}
				else if (entryText == "MAIN MENU") {
					status->goToMenu = true;
					status->paused = false;
					status->worldInitialised = false;
				}
			}
		}

		void PauseMenu::CreateEntries() {
			menuEntries.clear();

			activeEntry = 0;

			MenuEntry* resume = new MenuEntry("RESUME GAME", true);
			menuEntries.push_back(resume);

			MenuEntry* mainMenu = new MenuEntry("MAIN MENU");
			menuEntries.push_back(mainMenu);
		}
	}
}