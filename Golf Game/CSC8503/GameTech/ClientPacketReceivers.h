#pragma once
#include "../CSC8503Common/NetworkBase.h"
#include "../CSC8503Common/GameServer.h"
#include "GolfGame.h"

namespace NCL {
	namespace CSC8503 {
		
		class CreatePlayerPacketReceiver : public PacketReceiver {
		public:
			CreatePlayerPacketReceiver(GolfGame* g) : game(g) {}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Create_Player) {
					CreatePlayerPacket* packet = (CreatePlayerPacket*)data;			
					Player* player = game->AddPlayer(packet->playerID, packet->position, packet->enetID);
					game->AddObjectToMap(player);
					game->SetCurrentLevel(packet->currentLevel);
				}
			}

		protected:
			GolfGame* game;
		};

		class ShutdownPacketReceiver : public PacketReceiver {
		public:
			ShutdownPacketReceiver(GolfGame* g): game(g) {}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Shutdown) {
					game->GetGameStatus()->goToMenu = true;
					game->GetGameStatus()->gameRunning = false;
					game->GetGameStatus()->worldInitialised = false;
					game->SetCurrentLevel(0);
				}
			}

		protected:
			GolfGame* game;
		};

		class CreateGameObjectPacketReceiver : public PacketReceiver {
		public:
			CreateGameObjectPacketReceiver(GolfGame* g) : game(g) {	}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Create_Object) {
					CreateGameObjectPacket* packet = (CreateGameObjectPacket*)data;

					if (game->GetObjectByID(packet->objectID) == nullptr) {

						switch (packet->objectType) {
							case STATIC: {
								if (packet->mesh == ORIENTED_CUBE) {
									GameObject* obj = game->AddOrientedCube(packet->position, packet->scale, packet->orientation, packet->tag, packet->inverseMass, packet->name, STATIC, packet->texture, packet->objectID);
									game->AddObjectToMap(obj);
								}
								else if(packet->mesh == CUBE){
									GameObject* obj = game->AddCube(packet->position, packet->scale, packet->tag, packet->inverseMass, packet->name, STATIC, packet->texture, packet->objectID);
									game->AddObjectToMap(obj);
								}
								break;
							}
							case DYNAMIC: {
								
								if (packet->mesh == CUBE) {
									GameObject* obj = game->AddCube(packet->position, packet->scale, packet->tag, packet->inverseMass, packet->name, DYNAMIC, packet->texture, packet->objectID);
									game->AddObjectToMap(obj);

								}
								else if (packet->mesh == ORIENTED_CUBE) {
									GameObject* obj = game->AddOrientedCube(packet->position, packet->scale, packet->orientation, packet->tag, packet->inverseMass, packet->name, DYNAMIC, packet->texture, packet->objectID);
									game->AddObjectToMap(obj);
								}
								else if (packet->mesh == SPHERE) {
									GameObject* obj = game->AddSphere(packet->position, packet->scale.x, packet->tag, packet->inverseMass, packet->name, DYNAMIC, packet->texture, packet->objectID);
									game->AddObjectToMap(obj);
								}
								break;
							}
						}
					}
				}
			}

		protected:
			GolfGame* game;
		};


		class UpdateGameObjectPacketReceiver : public PacketReceiver {
		public:
			UpdateGameObjectPacketReceiver(GolfGame* g) : game(g) {}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Update_Object) {
					UpdateGameObjectPacket* packet = (UpdateGameObjectPacket*)data;
					if (game->GetObjectByID(packet->objectID) != nullptr) {
						GameObject* object = game->GetObjectByID(packet->objectID);
						object->GetTransform().SetWorldPosition(packet->position);
						object->GetTransform().SetLocalOrientation(packet->orientation);
						object->GetPhysicsObject()->SetLinearVelocity(packet->linearVelocity);
						object->GetPhysicsObject()->SetAngularVelocity(packet->angularVelocity);
					}
				}
			}

		protected:
			GolfGame* game;
		};

		class DeleteLevelDataPacketReceiver : public PacketReceiver {
		public:
			DeleteLevelDataPacketReceiver(GolfGame* g) : game(g) {}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Delete_Level_Data) {
					game->ClearEverything();
				}
			}

		protected:
			GolfGame* game;
		};


		class HighscorePacketReceiver : public PacketReceiver {
		public:
			HighscorePacketReceiver(GolfGame* g) : game(g) {}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Highscore) {
					HighscorePacket* packet = (HighscorePacket*)data;
					game->AddHighscore(packet->level, packet->highscore);
				}
			}

		protected:
			GolfGame* game;
		};
	}
}

