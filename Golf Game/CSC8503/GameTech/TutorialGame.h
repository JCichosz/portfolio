#pragma once
#include "GameTechRenderer.h"
#include "../CSC8503Common/PhysicsSystem.h"


namespace NCL {
	namespace CSC8503 {
		class TutorialGame		{
		public:
			TutorialGame();
			~TutorialGame();

			virtual void UpdateGame(float dt);

			GameTechRenderer* GetRenderer() { return renderer; }

		protected:
			virtual void InitialiseAssets();

			void InitCamera();
			virtual void UpdateKeys();

			virtual void InitWorld();

			/*
			These are some of the world/object creation functions I created when testing the functionality
			in the module. Feel free to mess around with them to see different objects being created in different
			test scenarios (constraints, collision types, and so on). 
			*/
			void InitSphereGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, float radius);
			void InitMixedGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing);
			void InitCubeGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, const Vector3& cubeDims);
			void InitSphereCollisionTorqueTest();
			void InitCubeCollisionTorqueTest();
			void InitSphereAABBTest();
			void InitGJKWorld();
			void BridgeConstraintTest();
			void SimpleGJKTest();
			void SimpleAABBTest();
			void SimpleAABBTest2();			

			bool SelectObject();
			virtual void MoveSelectedObject(float dt);

			virtual GameObject* AddFloorToWorld(const Vector3& position, Vector3& size = Vector3(1000, 10, 1000));
			virtual GameObject* AddSphereToWorld(const Vector3& position, float radius, ObjectTag tag, float inverseMass = 10.0f,  std::string name = "", ObjectType type = STATIC);
			virtual GameObject* AddCubeToWorld(const Vector3& position, Vector3 dimensions, ObjectTag tag, float inverseMass = 10.0f,  std::string name = "", ObjectType type = STATIC);

			GameTechRenderer*	renderer;
			PhysicsSystem*		physics;
			GameWorld*			world;

			bool useGravity;
			bool inSelectionMode;

			float		forceMagnitude;

			OGLMesh*	cubeMesh	= nullptr;
			OGLMesh*	sphereMesh	= nullptr;
			OGLTexture* basicTex	= nullptr;
			OGLShader*	basicShader = nullptr;

		private:
			GameObject* selectionObject = nullptr;

		};
	}
}

