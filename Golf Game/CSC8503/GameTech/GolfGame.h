#pragma once
#include "TutorialGame.h"
#include "../CSC8503Common/Player.h"
#include "../CSC8503Common/Robot.h"

#include "GameStatus.h"
#include "PauseMenu.h"
#include "../CSC8503Common/GameClient.h"
#include "../CSC8503Common/GameServer.h"
#include "../CSC8503Common/NavigationGrid.h"

namespace NCL {
	namespace CSC8503 {

		enum GameMode {
			CLIENT,
			SERVER,
			SINGLEPLAYER
		};

		class GolfGame: public TutorialGame {
		public:
			GolfGame();
			virtual ~GolfGame();

			virtual void UpdateGame(float dt) override; // singleplayer update

			void Update(float dt);
			void UpdateServer(float dt); // when game running as a server
			void UpdateClient(float dt); // when game running as a client

			void Initialise();

			GameStatus* GetGameStatus() { return status; }

			// Updates the menu state machine
			void UpdateStateMachine(float dt) { gameStateMachine->Update(dt); }

			Player* AddPlayer(int id, Vector3& initialPos, int enetID);
			GameObject* AddSphere(const Vector3& position, float radius, ObjectTag tag, float inverseMass = 10.0f, std::string name = "", ObjectType type = DYNAMIC, std::string texture = "", int id = -1);
			GameObject* AddCube(const Vector3& position, Vector3 dimensions, ObjectTag tag, float inverseMass = 10.0f, std::string name = "", ObjectType type = STATIC, std::string texture = "", int id = -1);
			GameObject* AddOrientedCube(const Vector3& position, Vector3 dimensions, Quaternion orientation, ObjectTag tag, float inverseMass = 10.0f, std::string name = "", ObjectType type = DYNAMIC, std::string texture = "", int id = -1);
			Robot* AddRobot(const Vector3 & position, Vector3 dimensions, Vector3 arenaPos, Vector3 arenaDim, float inverseMass, int id = -1);
			
			GameObject* GetObjectByID(int id) { return gameObjects[id]; }	
			std::map<int, GameObject*> GetGameObjects() { return gameObjects; }

			void AddObjectToMap(GameObject* obj) { gameObjects[obj->GetNetworkID()] = obj;	}

			void SetPlayer(Player* p) { playerObject = p; }

			void ClearEverything();

			void DeleteServer();

			void AddHighscore(int i, HighscoreEntry hightscore);

			void SetCurrentLevel(int i) { currentLevel = i; }
			int GetCurrentLevel() { return currentLevel; }

		protected:
			GameMode mode;
			Menu* menu;
			PauseMenu* pauseMenu;
			GameStatus* status;
			StateMachine* gameStateMachine;

			NavigationGrid robotGrid;

			GameClient* client;
			GameServer* server;

			void InitialiseGolfAssets();

			vector<Vector3> ChasePlayer(Vector3 robotPos, Vector3 playerPos, Vector3 mapOffset);

			virtual void UpdateKeys() override;

			virtual void MoveSelectedObject(float dt) override;

			void LoadLevel(int lvlNumber);
			void ServerLoadLevel(int lvlNumber);

			void LoadFirstLevel();
			void LoadSecondLevel();
			void InitialiseGameStateMachine();

			bool PlayerBelowFloor() const;
			void FindTargetForRobot(Robot* r);

			int currentLevel;
			int maxPlayers;
			Player* playerObject;

			bool displayHighscores;

			std::map<std::string, OGLTexture*> textures;
			std::map<int, GameObject*> gameObjects;

			std::map<int, std::vector<HighscoreEntry>> highscores;
		};
	}
}
