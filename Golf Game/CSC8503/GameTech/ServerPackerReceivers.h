#pragma once
#include "../CSC8503Common/NetworkBase.h"
#include "../CSC8503Common/GameServer.h"

namespace NCL {
	namespace CSC8503 {
		class InputPacketReceiver : public PacketReceiver {
		public:
			InputPacketReceiver(GameServer* s): server(s) {}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Ball_Push) {
					InputPacket* packet = (InputPacket*)data;
					server->GetPlayer(packet->playerID)->GetPhysicsObject()->AddImpulseAtPosition(packet->direction, packet->positionOfClick);
					server->GetPlayer(packet->playerID)->IncreaseBallPushes();
				}
			}

		protected:
			GameServer* server;
		};

		class ResetPlayerPacketReceiver : public PacketReceiver {
		public:
			ResetPlayerPacketReceiver(GameServer* s): server(s) {}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Reset_Player) {
					ResetPlayerPacket* packet = (ResetPlayerPacket*)data;

					Player* player = server->GetPlayer(packet->playerID);
					player->ResetPositionAndVelocity();
					player->ResetBallPushes();
				}
			}

		protected:
			GameServer* server;
		};


		class PlayerNamePacketReceiver : public PacketReceiver {
		public:
			PlayerNamePacketReceiver(GolfGame* g, GameServer* s): game(g), server(s) {}

			void ReceivePacket(int type, GamePacket* data, int source) {
				if (type == Player_Name) {
					PlayerNamePacket* packet = (PlayerNamePacket*)data;
					int id = server->GetNextID();
					Vector3 playerPos = Vector3(0, 100, 140);

					Player* player = game->AddPlayer(id, playerPos, source);
					player->SetName(packet->name);
					game->AddObjectToMap(player);
					server->AddPlayer(player, source);

					CreatePlayerPacket playerPacket = CreatePlayerPacket(id, playerPos, game->GetCurrentLevel(), source);
					server->SendGlobalMessage(playerPacket);

					int currentLvl = game->GetCurrentLevel();

					vector<HighscoreEntry> highscores = server->GetLevelHighscores(currentLvl);

					for (int i = 0; i < highscores.size(); ++i) {
						HighscorePacket highscorePacket = HighscorePacket(currentLvl, highscores[i]);
						server->SendGlobalMessage(highscorePacket);
					}

					std::map<int, GameObject*> gameObjects = game->GetGameObjects();

					for (auto o : gameObjects) {
						GameObject* obj = o.second;

							CreateGameObjectPacket objectPacket = CreateGameObjectPacket(obj->GetNetworkID(), obj->GetTransform().GetWorldPosition(),
								obj->GetTransform().GetLocalScale(), obj->GetPhysicsObject()->GetFriction(), obj->GetPhysicsObject()->GetRestitution(),
								obj->GetPhysicsObject()->GetInverseMass(), obj->GetName(), obj->GetTexture(), obj->GetTransform().GetLocalOrientation(),
								obj->GetObjectType(), obj->GetObjectTag(), obj->GetMeshType());

							server->SendGlobalMessage(objectPacket);
							server->SendGlobalMessage(objectPacket);
							server->UpdateServer();
					}
				}
			}

		protected:
			GolfGame* game;
			GameServer* server;
		};

	}
}
