#pragma once
#include "GameStatus.h"
#include <string>
#include <vector>
#include "../../Common/Vector4.h"
#include "../../Common/Vector2.h"
#include "../CSC8503Common/Debug.h"
namespace NCL {
	namespace CSC8503 {
		using namespace Maths;

		struct MenuEntry {
		public:
			MenuEntry(std::string t, bool a = false) : text(t), active(a) {};

			std::string text;
			bool active;
		};

		class GolfGame;

		class Menu {
		public:
			Menu(GameStatus* status, GolfGame* g);
			virtual ~Menu();

			virtual void Update();

		protected:
			GameStatus* status;
			GolfGame* game;
			std::vector<MenuEntry*> menuEntries;

			Vector4 activeColour;
			Vector4 inactiveColour;

			int activeEntry;

			virtual void UpdateKeys();
			virtual void CreateEntries();
			void PrintMenu();

			void UpdateNavigation();
		};
	}
}

