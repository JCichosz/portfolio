#include "Menu.h"
#include "GolfGame.h"

namespace NCL {
	namespace CSC8503 {

		Menu::Menu(GameStatus * s, GolfGame* g): status(s), game(g) {
			CreateEntries();
			activeColour = Vector4(1.0f, 0.5f, 0, 1.0f);
			inactiveColour = Vector4(0.0f, 0.0f, 0.5f, 1.0f);
		}

		Menu::~Menu() {
			for (auto e : menuEntries) {
				delete e;
			}
		}

		void Menu::Update() {
			if (status->goToMenu == true) {
				status->goToMenu = false;
				status->client = false;

				if (status->server == true) game->DeleteServer();
				status->server = false;

				menuEntries[activeEntry]->active = false;
				activeEntry = 0;
				menuEntries[activeEntry]->active = true;
			}

			UpdateKeys();

			PrintMenu();		
		}

		void Menu::UpdateKeys() {
			UpdateNavigation();

			if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_RETURN)) {
				std::string entryText = menuEntries[activeEntry]->text;

				if (entryText == "SINGLEPLAYER") {
					status->gameRunning = true;
				}
				else if (entryText == "CLIENT MODE") {
					status->gameRunning = true;
					status->client = true;
				}
				else if (entryText == "SERVER MODE") {
					status->gameRunning = true;
					status->server = true;
				}
				else if (entryText == "EXIT") {
					status->shutdown = true;
				}
			}
		}

		void Menu::CreateEntries() {
			activeEntry = 0;

			MenuEntry* singleplayer = new MenuEntry("SINGLEPLAYER", true);
			menuEntries.push_back(singleplayer);

			MenuEntry* client = new MenuEntry("CLIENT MODE");
			menuEntries.push_back(client);

			MenuEntry* server = new MenuEntry("SERVER MODE");
			menuEntries.push_back(server);

			MenuEntry* exit = new MenuEntry("EXIT");
			menuEntries.push_back(exit);
		}


		void Menu::PrintMenu() {
			Vector2 startPos = Vector2(400, 400);
			Vector2 offset = Vector2(0, 40);

			for (int i = 0; i < menuEntries.size(); ++i) {
				Vector4 colour;
				if (menuEntries[i]->active == true) colour = activeColour;
				else colour = inactiveColour;

				Vector2 position = startPos - offset * i;

				Debug::Print(menuEntries[i]->text, position, colour);
			}
		}
		void Menu::UpdateNavigation() {
			if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_DOWN)) {
				menuEntries[activeEntry]->active = false;
				if ((activeEntry + 1) < menuEntries.size()) activeEntry++;
				menuEntries[activeEntry]->active = true;
			}
			if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_UP)) {
				menuEntries[activeEntry]->active = false;
				if ((activeEntry - 1) >= 0) activeEntry--;
				menuEntries[activeEntry]->active = true;
			}
		}
	}
}
