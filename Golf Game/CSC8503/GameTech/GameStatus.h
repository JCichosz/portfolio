#pragma once

/*
Bools used for state transitions.
*/
namespace NCL {
	namespace CSC8503 {
		struct GameStatus {
		public:
			GameStatus() {
				gameRunning = false;
				gameWon = false;
				paused = false;
				client = false;
				server = false;
				shutdown = false;
				goToMenu = false;
				worldInitialised = false;
			};

			bool gameRunning;
			bool gameWon;
			bool paused;
			bool client;
			bool server;
			bool shutdown;
			bool goToMenu;
			bool worldInitialised;
		};
	}
}

