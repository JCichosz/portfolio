#include "GolfGame.h"
#include "TutorialGame.h"
#include "../CSC8503Common/GameWorld.h"
#include "../../Plugins/OpenGLRendering/OGLMesh.h"
#include "../../Plugins/OpenGLRendering/OGLShader.h"
#include "../../Plugins/OpenGLRendering/OGLTexture.h"
#include "../../Common/TextureLoader.h"
#include "../CSC8503Common/State.h"
#include "../CSC8503Common/AnchorPointDistConstraint.h"
#include "../CSC8503Common/HingeConstraint.h"
#include "../CSC8503Common/StateTransition.h"
#include <functional>
#include "ClientPacketReceivers.h"
#include "ServerPackerReceivers.h"

using namespace NCL;
using namespace CSC8503;

GolfGame::GolfGame(): robotGrid(NavigationGrid("RobotGrid.txt")) {
	currentLevel = 0;

	server = nullptr;
	client = nullptr;
	displayHighscores = false;

	status = new GameStatus();
	menu = new Menu(status, this);
	pauseMenu = new PauseMenu(status, this);

	InitialiseGameStateMachine();

	physics->SetGravity(Vector3(0, -80, 0));
}


GolfGame::~GolfGame() {
	delete menu;
	delete status;
	delete pauseMenu;
}

void GolfGame::UpdateGame(float dt) {
	if ((playerObject != nullptr && playerObject->HasReachedGoal()) || !status->worldInitialised) {
		if (!status->worldInitialised) {
			currentLevel = 0;
			status->worldInitialised = true;
		}
		LoadLevel(currentLevel + 1);
	}
	if (!inSelectionMode) {
		world->GetMainCamera()->UpdateCamera(dt);
	}

	UpdateKeys();

	if (playerObject != nullptr) renderer->DrawString("Ball pushes: " + std::to_string(playerObject->GetBallPushes()), Vector2(10, 650));

	SelectObject();
	MoveSelectedObject(dt);

	world->UpdateWorld(dt);

	physics->Update(dt);
}

void GolfGame::Update(float dt) {
	if (!status->worldInitialised) {
		if (status->server) {
			mode = SERVER;

			int port = NetworkBase::GetDefaultPort();
			server = new GameServer(port, 3);

			InputPacketReceiver* pushBallReceiver = new InputPacketReceiver(server);
			server->RegisterPacketHandler(BasicNetworkMessages::Ball_Push, pushBallReceiver);

			PlayerNamePacketReceiver* playerNameReceiver = new PlayerNamePacketReceiver(this, server);
			server->RegisterPacketHandler(BasicNetworkMessages::Player_Name, playerNameReceiver);

			ResetPlayerPacketReceiver* resetPlayerReceiver = new ResetPlayerPacketReceiver(server);
			server->RegisterPacketHandler(BasicNetworkMessages::Reset_Player, resetPlayerReceiver);

			ServerLoadLevel(1);

			status->worldInitialised = true;
		}
		else if (status->client) {
			mode = CLIENT;

			ClearEverything();

			client = new GameClient();

			CreatePlayerPacketReceiver* createPlayerReceiver = new CreatePlayerPacketReceiver(this);
			client->RegisterPacketHandler(BasicNetworkMessages::Create_Player, createPlayerReceiver);

			CreateGameObjectPacketReceiver* createObjectReceiver = new CreateGameObjectPacketReceiver(this);
			client->RegisterPacketHandler(BasicNetworkMessages::Create_Object, createObjectReceiver);

			UpdateGameObjectPacketReceiver* updateReceiver = new UpdateGameObjectPacketReceiver(this);
			client->RegisterPacketHandler(BasicNetworkMessages::Update_Object, updateReceiver);

			DeleteLevelDataPacketReceiver* levelDeleteReceiver = new DeleteLevelDataPacketReceiver(this);
			client->RegisterPacketHandler(BasicNetworkMessages::Delete_Level_Data, levelDeleteReceiver);

			HighscorePacketReceiver* highscoreReceiver = new HighscorePacketReceiver(this);
			client->RegisterPacketHandler(BasicNetworkMessages::Highscore, highscoreReceiver);

			ShutdownPacketReceiver* shutdownReceiver = new ShutdownPacketReceiver(this);
			client->RegisterPacketHandler(BasicNetworkMessages::Shutdown, shutdownReceiver);

			int port = NetworkBase::GetDefaultPort();

			std::cout << "Please choose an IP address: \n";
			std::string ip;
			std::cin >> ip;

			client->Connect(ip, port);

			if (client->Connected()) {
				std::string name;

				std::cout << "Please choose a player name: \n";
				std::cin >> name;

				PlayerNamePacket namePlayer = PlayerNamePacket(name);
				client->SendPacket(namePlayer);
				client->UpdateClient();
			}

			status->worldInitialised = true;
		}
		else mode = SINGLEPLAYER;
	}

	switch (mode) {
		case SINGLEPLAYER: UpdateGame(dt); break;
		case CLIENT: UpdateClient(dt); break;
		case SERVER: UpdateServer(dt); break;
	}
}

void GolfGame::UpdateServer(float dt) {
	server->UpdateServer();

	auto players = server->GetPlayers();

	for(auto playerData: players) {
		Player* player = playerData.second->playerObject;

		if (player->HasReachedGoal()) {
			HighscoreEntry entry(player->GetName(), player->GetBallPushes());
			server->AddHighscore(currentLevel, entry);

			DeleteLevelDataPacket deleteLevel = DeleteLevelDataPacket();
			server->SendGlobalMessage(deleteLevel);

			std::vector<HighscoreEntry> levelHighscores = server->GetLevelHighscores(currentLevel);

			for(int i = 0; i < levelHighscores.size(); ++i) {
				HighscorePacket highscorePacket = HighscorePacket(currentLevel, levelHighscores[i]);
				server->SendGlobalMessage(highscorePacket);
			}
			player->ResetReachedGoal();

			ServerLoadLevel(currentLevel + 1);
			break;
		}
	}

	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_ESCAPE)) {
		status->paused = true;
	}

	world->UpdateWorld(dt);

	physics->Update(dt);

	if (!server->GetPlayers().empty()) {
		for (auto& o : gameObjects) {
			GameObject* obj = o.second;

			if (obj->GetObjectType() == DYNAMIC) {
				UpdateGameObjectPacket updatePacket = UpdateGameObjectPacket(o.first, obj->GetTransform().GetWorldPosition(),
					obj->GetTransform().GetLocalOrientation(), obj->GetPhysicsObject()->GetLinearVelocity(), obj->GetPhysicsObject()->GetAngularVelocity());
				server->SendGlobalMessage(updatePacket);
			}
		}
	}
}

void GolfGame::UpdateClient(float dt) {
	client->UpdateClient();

	if (displayHighscores) {
		Vector2 startPos = Vector2(400, 400);
		Vector2 verticalOffset = Vector2(0, 40);
		Vector2 horizontalOffset = Vector2( 400, 0);
		renderer->DrawString("HIGHSCORES", startPos + Vector2(100, 0));

		vector<HighscoreEntry> entries = highscores[currentLevel];

		for (int i = 0; i < entries.size(); ++i) {
			renderer->DrawString(entries[i].playerName, startPos - verticalOffset * (i+1));
			renderer->DrawString(std::to_string(entries[i].pushes), startPos - (verticalOffset * (i + 1) - horizontalOffset));
		}
	}
	if (!inSelectionMode) {
		world->GetMainCamera()->UpdateCamera(dt);
	}

	UpdateKeys();

	if (playerObject != nullptr) renderer->DrawString("Ball pushes: " + std::to_string(playerObject->GetBallPushes()), Vector2(10, 650));

	SelectObject();
	MoveSelectedObject(dt);

	
	world->UpdateWorld(dt);
}

void GolfGame::Initialise() {
	InitialiseGolfAssets();
	InitCamera();
}

void GolfGame::InitialiseGolfAssets() {
	cubeMesh = new OGLMesh("cube.msh");
	cubeMesh->SetPrimitiveType(GeometryPrimitive::Triangles);
	cubeMesh->UploadToGPU();

	sphereMesh = new OGLMesh("sphere.msh");
	sphereMesh->SetPrimitiveType(GeometryPrimitive::Triangles);
	sphereMesh->UploadToGPU();

	basicTex = (OGLTexture*)TextureLoader::LoadAPITexture("checkerboard.png");
	basicShader = new OGLShader("GameTechVert.glsl", "GameTechFrag.glsl");

	OGLTexture* rubber = (OGLTexture*)TextureLoader::LoadAPITexture("rubber.jpg");
	textures.emplace("rubber", rubber);

	OGLTexture* goal = (OGLTexture*)TextureLoader::LoadAPITexture("goal.jpg");
	textures.emplace("goal", goal);

	OGLTexture* player = (OGLTexture*)TextureLoader::LoadAPITexture("player.jpg");
	textures.emplace("player", player);

	OGLTexture* lava = (OGLTexture*)TextureLoader::LoadAPITexture("lava.png");
	textures.emplace("lava", lava);

	OGLTexture* ice = (OGLTexture*)TextureLoader::LoadAPITexture("ice.png");
	textures.emplace("ice", ice);

	OGLTexture* robot = (OGLTexture*)TextureLoader::LoadAPITexture("robot.png");
	textures.emplace("robot", robot);

	OGLTexture* danger = (OGLTexture*)TextureLoader::LoadAPITexture("danger.png");
	textures.emplace("danger", danger);
}

vector<Vector3> GolfGame::ChasePlayer(Vector3 robotPos, Vector3 playerPos, Vector3 mapOffset) {
	NavigationPath outPath;

	vector<Vector3> pathNodes;

	bool found = robotGrid.FindPath(robotPos, playerPos, outPath);

	Vector3 pos;
	while (outPath.PopWaypoint(pos)) {
		pathNodes.push_back(pos);
	}
	for (int i = 1; i < pathNodes.size(); ++i) {
		Vector3 a = pathNodes[i - 1];
		Vector3 b = pathNodes[i];

		a.x = a.x * robotGrid.GetNodeSize() / 15;
		a.y = 20;
		a.z = a.z * robotGrid.GetNodeSize() / 11;

		b.x = b.x * robotGrid.GetNodeSize() / 15;
		b.y = 20;
		b.z = b.z * robotGrid.GetNodeSize() / 11;
		Debug::DrawLine(a + mapOffset, b + mapOffset, Vector4(0, 1, 0, 1));
	}
	return pathNodes;
}


void GolfGame::UpdateKeys() {
	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_R) || PlayerBelowFloor()) {
		if (mode == SINGLEPLAYER) {
			LoadLevel(currentLevel);
		}
		else if (mode == CLIENT) {
			ResetPlayerPacket resetPlayer = ResetPlayerPacket(playerObject->GetPlayerId());
			client->SendPacket(resetPlayer);
			playerObject->ResetBallPushes();
		}
		world->GetMainCamera()->SetPitch(-35.0f);
		world->GetMainCamera()->SetYaw(playerObject->GetTransform().GetWorldOrientation().x);
		world->GetMainCamera()->SetPosition((playerObject->GetTransform().GetWorldPosition()) + Vector3(0, 120, 200));
	}
	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_TAB)) {
		displayHighscores = !displayHighscores;
	}
	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_B)) {
		world->GetMainCamera()->SetPitch(-35.0f);
		world->GetMainCamera()->SetYaw(playerObject->GetTransform().GetWorldOrientation().x);
		world->GetMainCamera()->SetPosition((playerObject->GetTransform().GetWorldPosition()) + Vector3(0, 120, 200));
	}
	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_ESCAPE)) {
		status->paused = true;
	}
}

void GolfGame::MoveSelectedObject(float dt) { 
	renderer->DrawString("Click Force:" + std::to_string(forceMagnitude), Vector2(10, 20));
	forceMagnitude += Window::GetMouse()->GetWheelMovement();

	if (forceMagnitude > 100.0f) forceMagnitude = 100.0f;
	if (forceMagnitude < 1.0f) forceMagnitude = 1.0f;

	//Push the selected object!
	if (Window::GetMouse()->ButtonPressed(NCL::MouseButtons::MOUSE_RIGHT)) {
		Ray ray = CollisionDetection::BuildRayFromMouse(*world->GetMainCamera());

		RayCollision closestCollision;
		if (world->Raycast(ray, closestCollision, true)) {
			if (closestCollision.node == playerObject) {
				playerObject->GetPhysicsObject()->AddImpulseAtPosition(ray.GetDirection() * forceMagnitude, closestCollision.collidedAt, dt);

				if (playerObject->GetObjectTag() == PLAYER) {
					((Player*)playerObject)->IncreaseBallPushes();

					if (mode == CLIENT) {
						InputPacket inputPacket = InputPacket(playerObject->GetNetworkID(), ray.GetDirection() * forceMagnitude, closestCollision.collidedAt);
						client->SendPacket(inputPacket);
					}
				}
			}
		}
	}
}

void GolfGame::ServerLoadLevel(int lvlNumber) {
	LoadLevel(lvlNumber);

	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	world->GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		int id = server->GetNextID();
		(*i)->SetNetworkID(id);
		gameObjects.emplace(id, (*i));
	}

	Vector3 startPos = Vector3(0, 100, 140);
	int num = 0;
	for (auto player : server->GetPlayers()) {
		int playerID = player.first;


		Player* newPlayer = AddPlayer(playerID, startPos + Vector3(0, 100, 0) * num, player.second->enetID);
		newPlayer->SetName(player.second->playerName);
		AddObjectToMap(newPlayer);
		server->GetPlayers()[playerID]->playerObject = newPlayer;

		CreatePlayerPacket playerPacket = CreatePlayerPacket(playerID, newPlayer->GetTransform().GetWorldPosition(), currentLevel, player.second->enetID);
		server->SendGlobalMessage(playerPacket);

		num++;
	}
	vector<HighscoreEntry> highscores = server->GetLevelHighscores(currentLevel);

	for (int i = 0; i < highscores.size(); ++i) {
		HighscorePacket highscorePacket = HighscorePacket(currentLevel, highscores[i]);
		server->SendGlobalMessage(highscorePacket);
	}

	world->UpdateWorld(0.00001f);

	for (auto o : gameObjects) {
		GameObject* obj = o.second;

		CreateGameObjectPacket objectPacket = CreateGameObjectPacket(obj->GetNetworkID(), obj->GetTransform().GetWorldPosition(),
			obj->GetTransform().GetLocalScale(), obj->GetPhysicsObject()->GetFriction(), obj->GetPhysicsObject()->GetRestitution(),
			obj->GetPhysicsObject()->GetInverseMass(), obj->GetName(), obj->GetTexture(), obj->GetTransform().GetLocalOrientation(),
			obj->GetObjectType(), obj->GetObjectTag(), obj->GetMeshType());

		server->SendGlobalMessage(objectPacket);
	}
	
}

void GolfGame::LoadLevel(int lvlNumber) {
	ClearEverything();
	
	playerObject = nullptr;

	switch(lvlNumber) {
		case 1: LoadFirstLevel(); break;
		case 2: LoadSecondLevel(); break;
		default: LoadFirstLevel(); break;
	}
}

void GolfGame::LoadFirstLevel() {
	currentLevel = 1;

	GameObject* floor1 = AddCube(Vector3(0, -25, 0), Vector3(500, 10, 500), FLOOR, 0.0f, "floor");

	if(mode == SINGLEPLAYER) playerObject = AddPlayer(0, Vector3(0, 100, 140), -1);

	GameObject* goal = AddCube(Vector3(320, 5, -330), Vector3(20, 20, 20), GOAL, 0.0f, "goal", STATIC, "goal");

	AddCube(Vector3(70, 5, 20), Vector3(10, 10, 150), WALL, 0.0f);
	AddCube(Vector3(-70, 5, -100), Vector3(10, 10, 270), WALL, 0.0f);
	AddCube(Vector3(0, 5, 180), Vector3(80, 10, 10), WALL, 0.0f);

	AddCube(Vector3(210, 5, -120), Vector3(130, 10, 10), WALL, 0.0f);
	AddCube(Vector3(140, 5, -360), Vector3(200, 10, 10), WALL, 0.0f);

	AddCube(Vector3(350, 5, -240), Vector3(10, 10, 130), WALL, 0.0f);

	GameObject* spinning = AddOrientedCube(Vector3(40, 5, -240), Vector3(10, 10, 70), Quaternion(Vector3(0, 1, 0), 0.78f), OBSTACLE, 0.0f, "spinningObs", DYNAMIC);

	StateMachine* spinningSM = new StateMachine();

	std::function<void(void*, float)> spinLeft = [](void* go, float dt) {
		((GameObject*)go)->GetPhysicsObject()->SetAngularVelocity(Vector3(0, 2, 0));
	};

	auto spinRight = [](void* go, float dt) {
		((GameObject*)go)->GetPhysicsObject()->SetAngularVelocity(Vector3(0, -2, 0));
	};

	GenericState* spinningLeftState = new GenericState(spinLeft, (void*)spinning);
	GenericState* spinningRightState = new GenericState(spinRight, (void*)spinning);

	spinningSM->AddState(spinningLeftState);
	spinningSM->AddState(spinningRightState);

	GenericTransition<GenericState*, float>::GenericTransitionFunc timedTransition = [](GenericState* s, float val) {
		bool overTime = ((GenericState*)s)->GetTimeSpentInState() > val;
		if (overTime) ((GenericState*)s)->ResetTimeInState();
		return overTime;
	};


	GenericTransition<GenericState*, float>* transitionA = new GenericTransition<GenericState*, float>(timedTransition, spinningLeftState, 5.0f, spinningLeftState, spinningRightState);

	GenericTransition<GenericState*, float>* transitionB = new GenericTransition<GenericState*, float>(timedTransition, spinningRightState, 5.0f, spinningRightState, spinningLeftState);

	spinningSM->AddTransition(transitionA);
	spinningSM->AddTransition(transitionB);

	spinning->SetStateMachine(spinningSM);

	GameObject* moving = AddCube(Vector3(200, 5, -240), Vector3(10, 10, 50), OBSTACLE, 0.0f, "movingObs", DYNAMIC, "rubber");
	moving->GetPhysicsObject()->SetRestitution(2.0f);

	StateMachine* movingSM = new StateMachine();

	std::function<void(void*, float)> moveDown = [](void* go, float dt) {
		((GameObject*)go)->GetPhysicsObject()->SetLinearVelocity(Vector3(0, 0, -50));
	};

	auto moveUp = [](void* go, float dt) {
		((GameObject*)go)->GetPhysicsObject()->SetLinearVelocity(Vector3(0, 0, 50));
	};

	GenericState* movingDownState = new GenericState(moveDown, (void*)moving);
	GenericState* movingUpState = new GenericState(moveUp, (void*)moving);

	movingSM->AddState(movingDownState);
	movingSM->AddState(movingUpState);

	GenericTransition<GameObject*, float>::GenericTransitionFunc moveUpTransition = [](GameObject* go, float val) {
		return go->GetTransform().GetWorldPosition().z < val;
	};

	GenericTransition<GameObject*, float>::GenericTransitionFunc moveDownTransition = [](GameObject* go, float val) {
		return go->GetTransform().GetWorldPosition().z > val;
	};

	GenericTransition<GameObject*, float>* startMoveDown = new GenericTransition<GameObject*, float>(moveUpTransition, moving, -290.0f, movingDownState, movingUpState);

	GenericTransition<GameObject*, float>* startMoveUp = new GenericTransition<GameObject*, float>(moveDownTransition, moving, -190.0f, movingUpState, movingDownState);

	movingSM->AddTransition(startMoveDown);
	movingSM->AddTransition(startMoveUp);

	moving->SetStateMachine(movingSM);
}

void GolfGame::LoadSecondLevel() {
	currentLevel = 2;

	GameObject* floor1 = AddCube(Vector3(0, -25, 0), Vector3(230, 10, 200), FLOOR, 0.0f, "", STATIC);
	floor1->GetPhysicsObject()->SetFriction(0.9f);

	AddCube(Vector3(0, -5, 210), Vector3(240, 30, 10), FLOOR, 0.0f, "", STATIC);

	AddCube(Vector3(-240, -5, -550), Vector3(10, 30, 750), FLOOR, 0.0f, "", STATIC);
	AddCube(Vector3(240, -5, -400), Vector3(10, 30, 600), FLOOR, 0.0f, "", STATIC);
	
	if (mode == SINGLEPLAYER) playerObject = AddPlayer(0, Vector3(0, 100, 140), -1);

	GameObject* ramp = AddOrientedCube(Vector3(80, -20, -230), Vector3(50, 60, 20), Quaternion(Vector3(1, 0, 0), 0.8f), OBSTACLE, 0.0f, "ramp", DYNAMIC);

	AddCube(Vector3(0, -25, -400), Vector3(230, 15, 200), RESET_PLAYER_OBSTACLE, 0.0f, "lava", STATIC, "lava");

	GameObject* floor2 = AddCube(Vector3(0, -25, -800), Vector3(230, 10, 200), FLOOR, 0.0f, "", STATIC);
	floor2->GetPhysicsObject()->SetFriction(0.9f);

	GameObject* ice = AddCube(Vector3(420, -19, -1150), Vector3(650, 5, 150), OBSTACLE, 0.0f, "ice", STATIC, "ice");
	ice->GetPhysicsObject()->SetFriction(0.01f);

	//  ice obstacles
	AddCube(Vector3(80, 15, -1100), Vector3(30, 30, 30), RESET_PLAYER_OBSTACLE, 0.0f, "", STATIC, "danger");
	AddCube(Vector3(350, 15, -1250), Vector3(30, 30, 30), RESET_PLAYER_OBSTACLE, 0.0f, "", STATIC, "danger");

	GameObject* bouncy = AddCube(Vector3(900, 15, -1200), Vector3(30, 30, 30), OBSTACLE, 0.0f, "", STATIC, "rubber");
	bouncy->GetPhysicsObject()->SetRestitution(7.0f);
	// end ice obstacles

	AddCube(Vector3(250, 25, -1310), Vector3(500, 40, 10), OBSTACLE, 0.0f, "", STATIC, "");
	AddCube(Vector3(660, -5, -990), Vector3(410, 30, 10), OBSTACLE, 0.0f, "", STATIC, "");

	AddCube(Vector3(1080, -5, -1500), Vector3(10, 30, 200), OBSTACLE, 0.0f, "", STATIC, "");
	AddCube(Vector3(760, -5, -1500), Vector3(10, 30, 200), OBSTACLE, 0.0f, "", STATIC, "");

	AddCube(Vector3(1080, -5, -2150), Vector3(10, 30, 450), OBSTACLE, 0.0f, "", STATIC, "");
	AddCube(Vector3(-240, -5, -2150), Vector3(10, 30, 450), OBSTACLE, 0.0f, "", STATIC, "");

	AddCube(Vector3(270, -5, -1710), Vector3(500, 30, 10), OBSTACLE, 0.0f, "", STATIC, "");
	AddCube(Vector3(420, -5, -2610), Vector3(650, 30, 10), OBSTACLE, 0.0f, "", STATIC, "");

	// Obstacles in robot area
	AddCube(Vector3(970, 25, -2200), Vector3(100, 30, 30), OBSTACLE, 0.0f, "", STATIC, "");
	AddCube(Vector3(870, 25, -1700), Vector3(100, 30, 20), OBSTACLE, 0.0f, "", STATIC, "");

	AddCube(Vector3(300, 25, -2300), Vector3(30, 30, 300), OBSTACLE, 0.0f, "", STATIC, "");
	AddCube(Vector3(600, 25, -2020), Vector3(30, 30, 300), OBSTACLE, 0.0f, "", STATIC, "");
	// end of obstacles

	GameObject* floor3 = AddCube(Vector3(920, -25, -1500), Vector3(150, 10, 200), FLOOR, 0.0f, "", STATIC);
	floor3->GetPhysicsObject()->SetFriction(0.9f);

	// 'saloon door'
	Vector3 leftWalPos = Vector3(780, 5, -1500);

	GameObject* leftWall = AddCube(leftWalPos, Vector3(10, 30, 20), WALL, 0.0f, "leftWall");
	GameObject* leftDoor = AddOrientedCube(leftWalPos + Vector3(70, 5,0), Vector3(60, 10, 10), Quaternion(), OBSTACLE, 10.0f, "leftDoor");

	HingeConstraint* leftHingeConstraint = new HingeConstraint(leftWall, leftDoor, Hinge::LEFT);
	AnchorPointDistConstraint* leftPositionConstraint = new AnchorPointDistConstraint(leftWall, Vector3(10, 0, 0), leftDoor, Vector3(-60, 0, 0), 0.0f);

	world->AddConstraint(leftPositionConstraint);
	world->AddConstraint(leftHingeConstraint);

	Vector3 rightWalPos = Vector3(1060, 5, -1500); 

	GameObject* rightWall = AddCube(rightWalPos, Vector3(10, 30, 20), WALL, 0.0f, "rightWall");
	GameObject* rightDoor = AddOrientedCube(rightWalPos + Vector3(-70, 5,0), Vector3(60, 10, 10), Quaternion(), OBSTACLE, 10.0f, "rightDoor");

	HingeConstraint* rightHingeConstraint = new HingeConstraint(rightWall, rightDoor, Hinge::RIGHT);
	AnchorPointDistConstraint* rightPositionConstraint = new AnchorPointDistConstraint(rightWall, Vector3(-10, 0, 0), rightDoor, Vector3(60, 0, 0), 0.0f);

	world->AddConstraint(rightPositionConstraint);
	world->AddConstraint(rightHingeConstraint);
	// end of door 

	Vector3 arenaPos = Vector3(420, -25, -2150);
	Vector3 arenaDim = Vector3(650, 10, 450);
	GameObject* floor4 = AddCube(arenaPos, arenaDim, FLOOR, 0.0f, "", STATIC);
	floor4->GetPhysicsObject()->SetFriction(0.9f);

	// Enemy robot
	Robot* robot = AddRobot(Vector3(-100, 45, -1900), Vector3(50, 50, 50), arenaPos, arenaDim + Vector3(0,100,0), 10.0f);

	StateMachine* robotSM = new StateMachine();

	std::function<void(void*, float)> patrolling = [this](void* go, float dt) {
		Robot* robot = (Robot*)go;

		if(robot->IsDestroyed()) {
			robot->Respawn();
		}

		Vector3 linearVel = Vector3(0, 0, 0);

		if (robot->GetTransform().GetWorldPosition().z < -2200) {
			robot->SetGoingForward(false);
		}
		if (robot->GetTransform().GetWorldPosition().z > -1850) {
			robot->SetGoingForward(true);
		}
		if (robot->GetTransform().GetWorldPosition().x > 100) {
			robot->SetGoingLeft(true);
		}
		if (robot->GetTransform().GetWorldPosition().x < -130) {
			robot->SetGoingLeft(false);
		}

		if (robot->GoingForward()) {
			linearVel += Vector3(0, 0, -50);
		}
		else {
			linearVel += Vector3(0, 0, 50);
		}
		if (robot->GoingLeft()) {
			linearVel += Vector3(-50, 0, 0);
		}
		else {
			linearVel += Vector3(50, 0, 0);
		}
		robot->GetPhysicsObject()->SetLinearVelocity(linearVel);

		if (mode == SINGLEPLAYER) {
			CollisionDetection::CollisionInfo info;

			if (CollisionDetection::AABBSphereIntersection(robot->GetArenaAABB(), Transform(robot->GetArenaOrigin()),
				*(SphereVolume*)playerObject->GetPhysicsObject()->GetVolumeType(), playerObject->GetTransform().GetWorldPosition(), info)) {
				robot->SetTarget(playerObject);
			}		
		}
		else if (mode == SERVER) {
			FindTargetForRobot(robot);
		}
	};

	auto chasing = [this](void* go, float dt) {		
		Robot* robot = (Robot*)go;

		robot->IncreaseTimeChasingTarget(dt);

		if(mode == SERVER && robot->GetTimeChasingTarget() > 10.0f) {
			FindTargetForRobot(robot);
		}

		Vector3 directionMove;
		
		Vector3 arenaOffset = robot->GetArenaOrigin() - robot->GetArenaAABB().GetHalfDimensions() + Vector3(-100, 0, -100);
		Vector3 robotPos = robot->GetTransform().GetWorldPosition() - arenaOffset;
		Vector3 playerPos = robot->GetTarget()->GetTransform().GetWorldPosition() - arenaOffset;

		auto path = ChasePlayer(robotPos, playerPos, arenaOffset);
		
		if (path.size() >= 2) {
			Vector3 direction = path[1] - path[0];
			direction.Normalise();

			if(!robot->GetPhysicsObject()->AtRest()) {
				robot->GetPhysicsObject()->SetAtRest(false);
			}
			robot->GetPhysicsObject()->AddForce(direction * 50);
		}
	};


	GenericState* robotPatrollingState = new GenericState(patrolling, (void*)robot);
	GenericState* robotChasingState = new GenericState(chasing, (void*)robot);
	GenericState* destroyedState = new GenericState({}, nullptr);

	robotSM->AddState(robotPatrollingState);
	robotSM->AddState(robotChasingState);
	robotSM->AddState(destroyedState);

	GenericTransition<GenericState*, float>::GenericTransitionFunc timedTransition = [](GenericState* s, float val) {
		bool overTime = ((GenericState*)s)->GetTimeSpentInState() > val;
		if (overTime) ((GenericState*)s)->ResetTimeInState();
		return overTime;
	};

	GenericTransition<Player*&, Player*>* startChase = new GenericTransition<Player*&, Player*>(GenericTransition<Player*&, Player*>::NotEqualsTransition, robot->GetTargetRef(), nullptr, robotPatrollingState, robotChasingState);
	GenericTransition<Player*&, Player*>* startPatrol = new GenericTransition<Player*&, Player*>(GenericTransition<Player*&, Player*>::EqualsTransition, robot->GetTargetRef(), nullptr, robotChasingState, robotPatrollingState);

	GenericTransition<bool&, bool>* destroyedChasing = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, robot->IsDestroyedRef(), true, robotChasingState, destroyedState);
	GenericTransition<bool&, bool>* destroyedPatrolling = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, robot->IsDestroyedRef(), true, robotPatrollingState, destroyedState);

	GenericTransition<GenericState*, float>* respawn = new GenericTransition<GenericState*, float>(timedTransition, destroyedState, 15.0f, destroyedState, robotPatrollingState);


	robotSM->AddTransition(startChase);
	robotSM->AddTransition(startPatrol);
	robotSM->AddTransition(destroyedChasing);
	robotSM->AddTransition(destroyedPatrolling);
	robotSM->AddTransition(respawn);
	robot->SetStateMachine(robotSM);

	GameObject* goal = AddCube(Vector3(-160, 55, -2530), Vector3(70, 70, 70), GOAL, 0.0f, "goal", STATIC, "goal");
}

void GolfGame::InitialiseGameStateMachine() {
	StateMachine* gameSM = new StateMachine();

	std::function<void(void*, float)> mainMenu = [](void* m, float dt) {
		((Menu*)m)->Update();
	};

	auto gameRunning = [](void* game, float dt) {
		((GolfGame*)game)->Update(dt);
	};

	auto paused = [](void* pause, float dt) {
		((PauseMenu*)pause)->Update();
	};

	GenericState* mainMenuState = new GenericState(mainMenu, (void*)menu);
	GenericState* gameRunningState = new GenericState(gameRunning, (void*)this);
	GenericState* pausedState = new GenericState(paused, (void*)pauseMenu);

	gameSM->AddState(mainMenuState);
	gameSM->AddState(gameRunningState);
	gameSM->AddState(pausedState);

	GenericTransition<bool&, bool>* menuToGame = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, status->gameRunning, true, mainMenuState, gameRunningState);
	GenericTransition<bool&, bool>* gameToPaused = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, status->paused, true, gameRunningState, pausedState);
	GenericTransition<bool&, bool>* pausedToGame = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, status->paused, false, pausedState, gameRunningState);
	GenericTransition<bool&, bool>* pausedToMenu = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, status->goToMenu, true, pausedState, mainMenuState);
	GenericTransition<bool&, bool>* gameToMenu = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, status->goToMenu, true, gameRunningState, mainMenuState);

	gameSM->AddTransition(menuToGame);
	gameSM->AddTransition(gameToPaused);
	gameSM->AddTransition(pausedToGame);
	gameSM->AddTransition(pausedToMenu);
	gameSM->AddTransition(gameToMenu);

	gameStateMachine = gameSM;
}

bool GolfGame::PlayerBelowFloor() const {
	return (playerObject != nullptr && playerObject->GetTransform().GetWorldPosition().y < -60.0f);
}

void GolfGame::FindTargetForRobot(Robot* robot) {
	vector<Player*> playersInArena;
	std::map<int, PlayerData*>& connectedPlayers = server->GetPlayers();

	for (auto data : connectedPlayers) {
		Player* player = data.second->playerObject;

		CollisionDetection::CollisionInfo info;
		if (CollisionDetection::AABBSphereIntersection(robot->GetArenaAABB(), Transform(robot->GetArenaOrigin()),
			*(SphereVolume*)player->GetPhysicsObject()->GetVolumeType(), player->GetTransform().GetWorldPosition(), info)
			&& player != robot->GetTarget()) {
			playersInArena.push_back(player);
		}
	}
	if (!playersInArena.empty()) {
		int targetIndex = rand() % playersInArena.size();
		robot->SetTarget(playersInArena[targetIndex]);
		robot->ResetTimeChasingTarget();
	}
}

Player* GolfGame::AddPlayer(int id, Vector3& initialPos, int enetID) {
	Player* player = new Player(id, initialPos);
	player->SetNetworkID(id);
	float radius = 20.0f;
	player->SetMeshType(SPHERE);

	Vector3 playerSize = Vector3(radius, radius, radius);
	SphereVolume* volume = new SphereVolume(radius);
	player->SetBoundingVolume((CollisionVolume*)volume);
	player->GetTransform().SetWorldScale(playerSize);
	player->GetTransform().SetWorldPosition(initialPos);

	OGLTexture* texture = textures.at("player");
	player->SetTexture("player");

	player->SetRenderObject(new RenderObject(&player->GetTransform(), sphereMesh, texture, basicShader));
	player->SetPhysicsObject(new PhysicsObject(&player->GetTransform(), player->GetBoundingVolume()));

	player->GetPhysicsObject()->SetInverseMass(10.0f);
	player->GetPhysicsObject()->InitSphereInertia();

	world->AddGameObject(player);
	player->UpdateBroadphaseAABB();

	if (mode == SINGLEPLAYER || mode == CLIENT && client->GetID() == enetID) {
		playerObject = player;
		playerObject->GetRenderObject()->SetColour(Vector4(1.0f, 1.0f, 1.0f, 1.0f));
	}
	else {
		player->GetRenderObject()->SetColour(Vector4((float) std::rand() / RAND_MAX, (float)std::rand() / RAND_MAX, (float)std::rand() / RAND_MAX, 1.0f));
	}

	return player;
}

GameObject* GolfGame::AddSphere(const Vector3& position, float radius, ObjectTag tag, float inverseMass, std::string name, ObjectType type, std::string tex, int id) {
	GameObject* sphere = new GameObject(tag, type, name);
	sphere->SetNetworkID(id);
	sphere->SetTexture(tex);
	sphere->SetMeshType(SPHERE);
	Vector3 sphereSize = Vector3(radius, radius, radius);
	SphereVolume* volume = new SphereVolume(radius);
	sphere->SetBoundingVolume((CollisionVolume*)volume);
	sphere->GetTransform().SetWorldScale(sphereSize);
	sphere->GetTransform().SetWorldPosition(position);

	OGLTexture* texture = basicTex;

	if (tex != "") texture = textures.at(tex);

	sphere->SetRenderObject(new RenderObject(&sphere->GetTransform(), sphereMesh, texture, basicShader));
	sphere->SetPhysicsObject(new PhysicsObject(&sphere->GetTransform(), sphere->GetBoundingVolume()));

	sphere->GetPhysicsObject()->SetInverseMass(inverseMass);
	sphere->GetPhysicsObject()->InitSphereInertia();

	if (tag == PLAYER) {
		sphere->GetRenderObject()->SetColour(Vector4((float)std::rand() / RAND_MAX, (float)std::rand() / RAND_MAX, (float)std::rand() / RAND_MAX, 1.0f));
	}

	world->AddGameObject(sphere);
	sphere->UpdateBroadphaseAABB();
	return sphere;
}

Robot * GolfGame::AddRobot(const Vector3 & position, Vector3 dimensions, Vector3 arenaPos, Vector3 arenaDim, float inverseMass, int id) {
	Robot* robot = new Robot(arenaDim, arenaPos);

	OGLTexture* texture = textures.at("robot");
	robot->SetTexture("robot");

	robot->SetNetworkID(id);
	AABBVolume* volume = new AABBVolume(dimensions);
	robot->SetMeshType(CUBE);

	robot->SetBoundingVolume((CollisionVolume*)volume);

	robot->GetTransform().SetWorldPosition(position);
	robot->GetTransform().SetWorldScale(dimensions);

	robot->SetRenderObject(new RenderObject(&robot->GetTransform(), cubeMesh, texture, basicShader));
	robot->SetPhysicsObject(new PhysicsObject(&robot->GetTransform(), robot->GetBoundingVolume()));

	robot->GetPhysicsObject()->SetInverseMass(inverseMass);
	robot->GetPhysicsObject()->SetInverseInertia(Vector3(0, 0, 0));
	robot->GetPhysicsObject()->UpdateInertiaTensor();
	world->AddGameObject(robot);

	robot->UpdateBroadphaseAABB();
	return robot;
}

GameObject * GolfGame::AddCube(const Vector3 & position, Vector3 dimensions, ObjectTag tag, float inverseMass, std::string name, ObjectType type, std::string tex, int id) {
	GameObject* cube = new GameObject(tag, type, name);
	cube->SetTexture(tex);
	cube->SetNetworkID(id);
	AABBVolume* volume = new AABBVolume(dimensions);
	cube->SetMeshType(CUBE);

	cube->SetBoundingVolume((CollisionVolume*)volume);

	cube->GetTransform().SetWorldPosition(position);
	cube->GetTransform().SetWorldScale(dimensions);

	OGLTexture* texture = basicTex;

	if (cube->GetTexture() != "") texture = textures.at(tex);

	cube->SetRenderObject(new RenderObject(&cube->GetTransform(), cubeMesh, texture, basicShader));
	cube->SetPhysicsObject(new PhysicsObject(&cube->GetTransform(), cube->GetBoundingVolume()));

	cube->GetPhysicsObject()->SetInverseMass(inverseMass);
	cube->GetPhysicsObject()->SetInverseInertia(Vector3(0,0,0));
	cube->GetPhysicsObject()->UpdateInertiaTensor();
	world->AddGameObject(cube);

	cube->UpdateBroadphaseAABB();
	return cube;
}

GameObject* GolfGame::AddOrientedCube(const Vector3 & position, Vector3 dimensions, Quaternion orientation, ObjectTag tag, float inverseMass, std::string name, ObjectType type, std::string tex, int id) {
	GameObject* cube = new GameObject(tag, type, name);
	cube->SetTexture(tex);
	cube->SetNetworkID(id);
	OBBVolume* volume = new OBBVolume(dimensions);
	cube->GetTransform().SetLocalOrientation(orientation);
	cube->SetMeshType(ORIENTED_CUBE);

	cube->SetBoundingVolume((CollisionVolume*)volume);

	cube->GetTransform().SetWorldPosition(position);
	cube->GetTransform().SetWorldScale(dimensions);

	OGLTexture* texture = basicTex;

	if (tex != "") texture = textures.at(tex);

	cube->SetRenderObject(new RenderObject(&cube->GetTransform(), cubeMesh, texture, basicShader));
	cube->SetPhysicsObject(new PhysicsObject(&cube->GetTransform(), cube->GetBoundingVolume()));

	cube->GetPhysicsObject()->SetInverseMass(inverseMass);
	cube->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(cube);

	cube->UpdateBroadphaseAABB();
	return cube;
}

void GolfGame::ClearEverything() {
	world->ClearAndErase();
	physics->Clear();
	gameObjects.clear();
	highscores.clear();
	playerObject = nullptr;

	InitCamera();
}

void NCL::CSC8503::GolfGame::DeleteServer() {
	if (server != nullptr) {
		delete server;
		server = nullptr;
	}
}

void GolfGame::AddHighscore(int i, HighscoreEntry score) {
	highscores[i].push_back(score);

	std::sort(highscores[i].begin(), highscores[i].end(), [](const HighscoreEntry& left, const HighscoreEntry& right) {
		return left.pushes < right.pushes;
	});

}

