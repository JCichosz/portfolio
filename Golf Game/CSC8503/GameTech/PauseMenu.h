#pragma once
#include "Menu.h"


namespace NCL {
	namespace CSC8503 {

		class PauseMenu : public Menu {
		public:
			PauseMenu(GameStatus* status, GolfGame* g);
			virtual ~PauseMenu();

			virtual void Update() override;

		protected:
			virtual void UpdateKeys() override;
			virtual void CreateEntries() override;
		};
	}
}

