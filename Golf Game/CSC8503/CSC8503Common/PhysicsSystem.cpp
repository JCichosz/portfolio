#include "PhysicsSystem.h"
#include "PhysicsObject.h"
#include "GameObject.h"
#include "CollisionDetection.h"
#include "../../Common/Quaternion.h"

#include "Constraint.h"

#include "Debug.h"
#include "Player.h"
#include <functional>
using namespace NCL;
using namespace CSC8503;

/*

These two variables help define the relationship between positions
and the forces that are added to objects to change those positions

*/

const float PhysicsSystem::UNIT_MULTIPLIER = 1.0f;
const float PhysicsSystem::UNIT_RECIPROCAL = 1.0f; // / UNIT_MULTIPLIER;

PhysicsSystem::PhysicsSystem(GameWorld& g) : gameWorld(g)	{
	applyGravity	= true;
	useBroadPhase	= true;	
	dTOffset		= 0.0f;
	globalDamping	= 0.95f;
	SetGravity(Vector3(0.0f, -9.8f, 0.0f));
}

PhysicsSystem::~PhysicsSystem()	{
}

void PhysicsSystem::SetGravity(const Vector3& g) {
	gravity = g * UNIT_MULTIPLIER;
}

/*

If the 'game' is ever reset, the PhysicsSystem must be
'cleared' to remove any old collisions that might still
be hanging around in the collision list. If your engine
is expanded to allow objects to be removed from the world,
you'll need to iterate through this collisions list to remove
any collisions they are in.

*/
void PhysicsSystem::Clear() {
	allCollisions.clear();
}

/*

This is the core of the physics engine update

*/
void PhysicsSystem::Update(float dt) {
	const float iterationDt = 1.0f / 120.0f; //Ideally we'll have 120 physics updates a second 
	dTOffset += dt; //We accumulate time delta here - there might be remainders from previous frame!

	int iterationCount = (int)(dTOffset / iterationDt); //And split it up here

	float subDt = dt / (float)iterationCount;	//How many seconds per iteration do we get?
	
	RestAndWake(dt);
	IntegrateAccel(dt); //Update accelerations from external forces

	for (int i = 0; i < iterationCount; ++i) {
		if (useBroadPhase) {
			BroadPhase();
			NarrowPhase();
		}
		else {
			BasicCollisionDetection();
		}

		//This is our simple iterative solver - 
		//we just run things multiple times, slowly moving things forward
		//and then rechecking that the constraints have been met

		int constraintIterationCount = 10;
		float constraintDt = subDt / (float)constraintIterationCount;

		for (int i = 0; i < constraintIterationCount; ++i) {
			UpdateConstraints(constraintDt);
			IntegrateVelocity(constraintDt); //update positions from new velocity changes
		}
		dTOffset -= iterationDt;
	}
	
	ClearForces();	//Once we've finished with the forces, reset them to zero
	
	UpdateCollisionList(); //Remove any old collisions
}

/*
Later on we're going to need to keep track of collisions
across multiple frames, so we store them in a set.

The first time they are added, we tell the objects they are colliding.
The frame they are to be removed, we tell them they're no longer colliding.

From this simple mechanism, we we build up gameplay interactions inside the
OnCollisionBegin / OnCollisionEnd functions (removing health when hit by a 
rocket launcher, gaining a point when the player hits the gold coin, and so on).
*/
void PhysicsSystem::UpdateCollisionList() {
	for (std::set<CollisionDetection::CollisionInfo>::iterator i = allCollisions.begin(); i != allCollisions.end(); ) {
		if ((*i).framesLeft == numCollisionFrames) {
			i->a->OnCollisionBegin(i->b);
			i->b->OnCollisionBegin(i->a);
		}
		(*i).framesLeft = (*i).framesLeft - 1;
		if ((*i).framesLeft < 0) {
			i->a->OnCollisionEnd(i->b);
			i->b->OnCollisionEnd(i->a);
			i = allCollisions.erase(i);
		}
		else {
			++i;
		}
	}
}

void PhysicsSystem::UpdateObjectAABBs() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		(*i)->UpdateBroadphaseAABB();
	}
}

/*

This is how we'll be doing collision detection in tutorial 4.
We step thorugh every pair of objects once (the inner for loop offset 
ensures this), and determine whether they collide, and if so, add them
to the collision set for later processing. The set will guarantee that
a particular pair will only be added once, so objects colliding for
multiple frames won't flood the set with duplicates.
*/
void PhysicsSystem::BasicCollisionDetection() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;
	gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		if ((*i)->GetPhysicsObject() == nullptr) continue;

		for (auto j = i + 1; j != last; ++j) {
			if ((*j)->GetPhysicsObject() == nullptr) continue;

			CollisionDetection::CollisionInfo info;

			if (!((*j)->GetObjectType() == STATIC && (*i)->GetObjectType() == STATIC)) {

				if (CollisionDetection::ObjectIntersection(*i, *j, info)) {
					ImpulseResolveCollision(*info.a, *info.b, info.point);

					info.framesLeft = numCollisionFrames;
					allCollisions.insert(info);
				}
			}
		}
	}

}

/*

In tutorial 5, we start determining the correct response to a collision,
so that objects separate back out. 

*/
void PhysicsSystem::ImpulseResolveCollision(GameObject& a, GameObject& b, CollisionDetection::ContactPoint& p) const {

	PhysicsObject* physA = a.GetPhysicsObject();
	PhysicsObject* physB = b.GetPhysicsObject();

	Transform& transformA = a.GetTransform();
	Transform& transformB = b.GetTransform();

	float totalMass = physA->GetInverseMass() + physB->GetInverseMass();

	if (totalMass == 0.0f) return;

	// Separation with projection method
	transformA.SetLocalPosition(transformA.GetLocalPosition() - (p.normal * p.penetration * (physA->GetInverseMass() / totalMass)));
	transformB.SetLocalPosition(transformB.GetLocalPosition() + (p.normal * p.penetration * (physB->GetInverseMass() / totalMass)));

	Vector3 relativeA = p.position - transformA.GetLocalPosition();
	Vector3 relativeB = p.position - transformB.GetLocalPosition();

	Vector3 angVelocityA = Vector3::Cross(physA->GetAngularVelocity(), relativeA);
	Vector3 angVelocityB = Vector3::Cross(physB->GetAngularVelocity(), relativeB);

	Vector3 fullVelocityA = physA->GetLinearVelocity() + angVelocityA;
	Vector3 fullVelocityB = physB->GetLinearVelocity() + angVelocityB;

	Vector3 contactVelocity = fullVelocityB - fullVelocityA;

	if (Vector3::Dot(contactVelocity, p.normal) > 0) return; // objects already moving away from each other

	float impulseForce = Vector3::Dot(contactVelocity, p.normal);

	Vector3 inertiaA = Vector3::Cross(relativeA, p.normal);
	Vector3 inertiaB = Vector3::Cross(relativeB, p.normal);

	float angularEffect = Vector3::Dot(inertiaA, physA->GetInertiaTensor() * inertiaA) + Vector3::Dot(inertiaB, physB->GetInertiaTensor() * inertiaB);

	float cRestitution = physA->GetRestitution() * physB->GetRestitution();

	float j = (-(1.0f + cRestitution) * impulseForce) / (totalMass + angularEffect);

	Vector3 impulse = p.normal * j;

	// Friction calculations
	Vector3 tangent = contactVelocity - (p.normal * Vector3::Dot(contactVelocity, p.normal));
	tangent.Normalise();

	float jFriction = -Vector3::Dot(contactVelocity, tangent);

	Vector3 frictionInertiaA = Vector3::Cross(relativeA, tangent);
	Vector3 frictionInertiaB = Vector3::Cross(relativeB, tangent);

	float frictionAngular = Vector3::Dot(frictionInertiaA, physA->GetInertiaTensor() * frictionInertiaA) 
							+ Vector3::Dot(frictionInertiaB, physB->GetInertiaTensor() * frictionInertiaB);

	jFriction = jFriction / (totalMass + frictionAngular);

	float frictionA = physA->GetFriction();
	float frictionB = physB->GetFriction();

	float cFriction = sqrt(frictionA * frictionB);

	Vector3 frictionImpulse = tangent * jFriction * cFriction;
	// end of friction calculations

	Vector3 totalImpulse = frictionImpulse + impulse;

	physA->ApplyLinearImpulse(-totalImpulse);
	physB->ApplyLinearImpulse(totalImpulse);

	if (physA->GetVolumeType()->type != VolumeType::AABB) {
		physA->ApplyAngularImpulse(Vector3::Cross(relativeA, -totalImpulse));
	}
	if (physB->GetVolumeType()->type != VolumeType::AABB) {
		physB->ApplyAngularImpulse(Vector3::Cross(relativeB, totalImpulse));
	}
}

/*

Later, we replace the BasicCollisionDetection method with a broadphase
and a narrowphase collision detection method. In the broad phase, we
split the world up using an acceleration structure, so that we can only
compare the collisions that we absolutely need to.

*/
void PhysicsSystem::BroadPhase() {
	broadphaseCollisions.clear();

	QuadTree<GameObject*> tree(Vector2(3000, 3000), 6, 5);

	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		Vector3 halfSizes;

		if (!(*i)->GetBroadphaseAABB(halfSizes)) continue;

		Vector3 pos = (*i)->GetConstTransform().GetWorldPosition();

		tree.Insert(*i, pos, halfSizes);

		tree.OperateOnContents([&](std::list<QuadTreeEntry<GameObject*>>& data) {
			CollisionDetection::CollisionInfo info;

			for (auto j = data.begin(); j != data.end(); ++j) {
				for (auto k = std::next(j); k != data.end(); ++k) {
					
					// Do not consider a pair of static objects as a collision pair
					if (!((*j).object->GetObjectType() == STATIC && (*k).object->GetObjectType() == STATIC)) {
						info.a = min((*j).object, (*k).object);
						info.b = max((*j).object, (*k).object);

						broadphaseCollisions.insert(info);
					}
				}
			}
		});
	}

	// Display quad tree when holding 1
	if (Window::GetKeyboard()->KeyHeld(KEYBOARD_1)) {
		tree.DebugDraw();
	}
}

/*

The broadphase will now only give us likely collisions, so we can now go through them,
and work out if they are truly colliding, and if so, add them into the main collision list
*/
void PhysicsSystem::NarrowPhase() {
	for (std::set<CollisionDetection::CollisionInfo>::iterator i = broadphaseCollisions.begin(); i != broadphaseCollisions.end(); ++i) {
		CollisionDetection::CollisionInfo info = *i;

		if (CollisionDetection::ObjectIntersection(info.a, info.b, info)) {
			info.framesLeft = numCollisionFrames;
			ImpulseResolveCollision(*info.a, *info.b, info.point);
			allCollisions.insert(info);
		}
	}
}

/*
Integration of acceleration and velocity is split up, so that we can
move objects multiple times during the course of a PhysicsUpdate,
without worrying about repeated forces accumulating etc. 

This function will update both linear and angular acceleration,
based on any forces that have been accumulated in the objects during
the course of the previous game frame.
*/
void PhysicsSystem::IntegrateAccel(float dt) {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {

		PhysicsObject* object = (*i)->GetPhysicsObject();


		if (object == nullptr || object->AtRest()) continue;

		if (object->GetVolumeType()->type == VolumeType::OBB) {
			(*i)->UpdateBroadphaseAABB();
		}

		float inverseMass = object->GetInverseMass();

		Vector3 linearVel = object->GetLinearVelocity();
		Vector3 force = object->GetForce();
		Vector3 accel = force * inverseMass;

		if (applyGravity && inverseMass > 0) accel += gravity;

		linearVel += accel * dt;
		object->SetLinearVelocity(linearVel);

		// Angular velocity
		if (object->GetVolumeType()->type == VolumeType::AABB) {
			object->SetAngularVelocity(Vector3(0, 0, 0));
		}
		else {
			Vector3 torque = object->GetTorque();
			Vector3 angularVel = object->GetAngularVelocity();

			object->UpdateInertiaTensor();

			Vector3 angularAccel = object->GetInertiaTensor() * torque;

			angularVel += angularAccel * dt;

			object->SetAngularVelocity(angularVel);
		}
	}
}
/*
This function integrates linear and angular velocity into
position and orientation. It may be called multiple times
throughout a physics update, to slowly move the objects through
the world, looking for collisions.
*/
void PhysicsSystem::IntegrateVelocity(float dt) {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	gameWorld.GetObjectIterators(first, last);

	float linearDampingFactor = 1.0f - 0.2f;
	float linearDamping = powf(linearDampingFactor, dt);

	for (auto i = first; i != last; ++i) {
		PhysicsObject* object = (*i)->GetPhysicsObject();

		if (object == nullptr || object->AtRest()) continue;

		Transform& transform = (*i)->GetTransform();

		Vector3 position = transform.GetLocalPosition();
		Vector3 linearVel = object->GetLinearVelocity();
		position += linearVel * dt;

		transform.SetLocalPosition(position);

		linearVel = linearVel *linearDamping;

		object->SetLinearVelocity(linearVel);

		// Angular velocity
		if (object->GetVolumeType()->type == VolumeType::AABB) {
			object->SetAngularVelocity(Vector3(0,0,0));
		}
		else {
			Quaternion orientation = transform.GetLocalOrientation();
			Vector3 angularVel = object->GetAngularVelocity();

			orientation = orientation + (Quaternion(angularVel * dt * 0.5f, 0.0f) * orientation);
			orientation.Normalise();

			transform.SetLocalOrientation(orientation);

			float angularDampingFactor = 1.0f - 0.6f;
			float angularDamping = powf(angularDampingFactor, dt);

			angularVel = angularVel *angularDamping;
			object->SetAngularVelocity(angularVel);
		}
	}
}

/*
Once we're finished with a physics update, we have to
clear out any accumulated forces, ready to receive new
ones in the next 'game' frame.
*/
void PhysicsSystem::ClearForces() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;
	gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		//Clear our object's forces for the next frame
		(*i)->GetPhysicsObject()->ClearForces();
	}
}

void PhysicsSystem::RestAndWake(float dt) {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;
	gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		PhysicsObject* po = (*i)->GetPhysicsObject();	
		po->UpdateAtRest(dt);
	}
}


/*

As part of the final physics tutorials, we add in the ability
to constrain objects based on some extra calculation, allowing
us to model springs and ropes etc. 

*/
void PhysicsSystem::UpdateConstraints(float dt) {
	std::vector<Constraint*>::const_iterator first;
	std::vector<Constraint*>::const_iterator last;
	gameWorld.GetConstraintIterators(first, last);

	for (auto i = first; i != last; ++i) {
		(*i)->UpdateConstraint(dt);
	}
}