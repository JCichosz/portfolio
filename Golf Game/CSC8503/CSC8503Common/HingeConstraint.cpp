#include "HingeConstraint.h"
#include "../../Common/Maths.h"

namespace NCL {
	namespace CSC8503 {
		HingeConstraint::HingeConstraint(GameObject* a, GameObject* b, Hinge hinge) :
			frame(a), door(b), hingeSide(hinge) {
			originalDoorPosition = door->GetTransform().GetWorldPosition();
		}

		void HingeConstraint::UpdateConstraint(float dt) {
			// Calculate the angle between original door origin position and current position
			Vector3 fromHingeToOriginalPos = originalDoorPosition - frame->GetTransform().GetWorldPosition();
			Vector3 fromHingeToCurrentPos = door->GetTransform().GetWorldPosition() - frame->GetTransform().GetWorldPosition();

			PhysicsObject* physFrame = frame->GetPhysicsObject();
			PhysicsObject* physDoor = door->GetPhysicsObject();			

			float angleBetween = Maths::RadiansToDegrees(acos(Vector3::Dot(fromHingeToOriginalPos.Normalised(), fromHingeToCurrentPos.Normalised())));
				
			Vector3 relativeAngularVel = physFrame->GetAngularVelocity() - physDoor->GetAngularVelocity();
			float constraintMass = physFrame->GetInverseMass() + physDoor->GetInverseMass();

			if (constraintMass > 0.0f && dt > 0.0f) {
				Vector3 relativePos = frame->GetConstTransform().GetWorldPosition() - door->GetConstTransform().GetWorldPosition();
				Vector3 direction = relativePos.Normalised();

				// Larger angle = larger impulse to rotate door back to original orientation
				float biasFactor = (abs(angleBetween)) * 0.00005f;
				float bias = -(biasFactor / dt); 

				Vector3 bImpulse = direction * bias;

				// Flip bias for doors with hinge on the other side
				if (hingeSide == Hinge::RIGHT) {
					bias = -bias;
				}
				if (fromHingeToCurrentPos.z > 0.0f)	physDoor->ApplyAngularImpulse(Vector3(0, -bias, 0));
				else physDoor->ApplyAngularImpulse(Vector3(0, bias, 0));
				
			}
			// Allow rotation only on the Y axis
			physDoor->SetAngularVelocity(Vector3(0, physDoor->GetAngularVelocity().y, 0));
		}

	}
}
