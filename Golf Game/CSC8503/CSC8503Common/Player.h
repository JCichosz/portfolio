#pragma once
#include "GameObject.h"

/*
 * Class representing a player object, used in both single and multiplayer.
 * Stores the number of ball pushes and whether the player has finished the level.
 */
namespace NCL {
	namespace CSC8503 {
		class Player : public GameObject {
		public:
			Player(int playerId, Vector3 initialPos);
			virtual ~Player();

			void SetInitialPosition(Vector3 pos)	{ initialPos = pos; }
			Vector3 GetInitialPosition() const		{ return initialPos; }
			void ResetPositionAndVelocity();

			void IncreaseBallPushes()				{ ballPushes++; }
			int GetBallPushes()		const			{ return ballPushes; }
			void ResetBallPushes()					{ ballPushes = 0; }

			int GetPlayerId()	const				{ return playerId; }

			bool HasReachedGoal()	const			{ return reachedGoal; }
			void ResetReachedGoal()					{ reachedGoal = false; }

			void OnCollisionBegin(GameObject* otherObject) override;

		protected:
			Vector3 initialPos;
			int		ballPushes;
			int		playerId;
			bool	reachedGoal;
		};
	}
}

