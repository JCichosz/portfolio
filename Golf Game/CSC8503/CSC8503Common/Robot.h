#pragma once
#include "Player.h"
#include "AABBVolume.h"

/*
 * Type of game object representing an enemy robot chasing players.
 * Needs information about its arena size and position to check if any players are
 * currently present in the arena.
 */

namespace NCL {
	namespace CSC8503 {
		class Robot : public GameObject {
		public:
			Robot(Vector3 arenaDimensions, Vector3 arenaOrg);
			virtual ~Robot();

			// Player currently being chased
			void SetTarget(Player* t) { target = t; }
			Player* GetTarget() const { return target; }
			Player*& GetTargetRef() { return target; }

			// AABB volume representing the robot's arena
			AABBVolume GetArenaAABB() const { return arenaAABB; }
			Vector3 GetArenaOrigin() const { return arenaOrigin; }

			// Bools used to apply velocity in the patrolling state
			void SetGoingLeft(bool b) { goingLeft = b; }
			void SetGoingForward(bool b) { goingForward = b; }

			bool GoingLeft() const { return goingLeft; }
			bool GoingForward() const { return goingForward; }

			void OnCollisionBegin(GameObject* otherObject) override;

			bool IsDestroyed() const { return destroyed; }
			bool& IsDestroyedRef() { return destroyed; }

			void Respawn();

			// Time spent chasing current target
			float GetTimeChasingTarget() const { return timeChasingTarget; }
			void IncreaseTimeChasingTarget(float dt) { timeChasingTarget += dt; }
			void ResetTimeChasingTarget() { timeChasingTarget = 0.0f; }

		protected:
			Player* target;
			AABBVolume arenaAABB;
			Vector3 arenaOrigin;

			bool goingLeft;
			bool goingForward;

			bool destroyed;
			float timeChasingTarget;
		};
	}
}
