#include "GameClient.h"
#include <iostream>
#include <string>

using namespace NCL;
using namespace CSC8503;

GameClient::GameClient()	{
	netHandle = enet_host_create(nullptr, 1, 1, 0, 0);
}

GameClient::~GameClient()	{
	//threadAlive = false;
	//updateThread.join();
	enet_host_destroy(netHandle);
}

bool GameClient::Connect(uint8_t a, uint8_t b, uint8_t c, uint8_t d, int portNum) {
	ENetAddress address;
	address.port = portNum;
	address.host = (d << 24) | (c << 16) | (b << 8) | (a);

	netPeer = enet_host_connect(netHandle, &address, 2, 0);

	isConnected = (netPeer != nullptr);
	return isConnected;
}

bool GameClient::Connect(std::string s, int portNum) {
	ENetAddress address;
	enet_address_set_host_ip(&address, s.c_str());
	address.port = portNum;

	netPeer = enet_host_connect(netHandle, &address, 2, 0);

	isConnected = false;
	for (int i = 0; i < 10; ++i) {
		UpdateClient();
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		if (isConnected) break;
	}

	return isConnected;
}

void GameClient::UpdateClient() {
	if (netHandle == nullptr) return;

	// handle all incoming packets
	ENetEvent event;

	while (enet_host_service(netHandle, &event, 0) > 0) {
		if (event.type == ENET_EVENT_TYPE_CONNECT) {
			std::cout << "Connected to server!" << std::endl;
			isConnected = true;
		}
		else if (event.type == ENET_EVENT_TYPE_RECEIVE) {
			std::cout << "Client: Packet received..." << std::endl;
			GamePacket* packet = (GamePacket*)event.packet->data;
			ProcessPacket(packet);
		}
		enet_packet_destroy(event.packet);
	}
}

void GameClient::SendPacket(GamePacket&  payload) {
	ENetPacket* dataPacket = enet_packet_create(&payload, payload.GetTotalSize(), 0);
	enet_peer_send(netPeer, 0, dataPacket);
}

void GameClient::ThreadedUpdate() {
	while (threadAlive) {
		UpdateClient();
		std::this_thread::yield();
	}
}
