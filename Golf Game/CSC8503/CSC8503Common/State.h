#pragma once
#include <functional>

namespace NCL {
	namespace CSC8503 {
		class State		{
		public:
			State();
			virtual ~State();
			virtual void Update(float dt) = 0; //Pure virtual base class
			virtual float GetTimeSpentInState() { return timeInState; }
			virtual void ResetTimeInState() { timeInState = 0.0f; }
		protected:
			float timeInState;
		};

	//	typedef void(*StateFunc)(void*, float);

		class GenericState : public State		{
		public:
			GenericState(std::function<void(void*, float)> someFunc, void* someData) {
				func		= someFunc;
				funcData	= someData;
			}
			virtual void Update(float dt) override {
				timeInState += dt;

				if (funcData != nullptr) {
					func(funcData, dt);
				}
			}

		protected:
			std::function<void(void*, float)> func;
			void* funcData;
		};
	}
}