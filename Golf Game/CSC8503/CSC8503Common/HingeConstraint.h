#pragma once
#include "Constraint.h"
#include "../../Common/Vector3.h"
#include "GameObject.h"
#include "Debug.h"

/*
 * A constraint modelling a self-closing door. Takes in two objects
 * ('door frame' and door), as well as a specifier where the hinge is located.
 * Constantly tries to return the door to its initial position.
 */
namespace NCL {
	namespace CSC8503 {
		class GameObject;

		// Which side of the door has a hinge
		enum class Hinge {
			LEFT, RIGHT
		};

		class HingeConstraint: public Constraint {
		public:
			HingeConstraint(GameObject* frame, GameObject* door, Hinge hingeSide);
			virtual ~HingeConstraint() {}

			void UpdateConstraint(float dt) override;

		protected:
			GameObject* frame;
			GameObject* door;
			Vector3		originalDoorPosition;
			Hinge		hingeSide;
		};
	}
}

