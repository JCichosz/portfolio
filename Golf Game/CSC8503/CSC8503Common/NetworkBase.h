#pragma once
#include <enet/enet.h>
#include <map>
#include <string>
#include "GameObject.h"
#include <vector>

namespace NCL {
	namespace CSC8503 {
		enum BasicNetworkMessages {
			None,
			Hello,
			Message,
			String_Message,
			Delta_State,	//1 byte per channel since the last state
			Full_State,		//Full transform etc
			Received_State, //received from a client, informs that its received packet n
			Player_Connected,
			Player_Disconnected,
			Shutdown,
			Create_Object,	// used in level loading
			Update_Object,	// update position etc for dynamic objects
			Create_Player,
			Ball_Push,		// details about the mouse raycast from client
			Highscore,		
			Player_Name,	// Sent when a new client connects
			Delete_Level_Data,
			Reset_Player
		};

		struct GamePacket {
			short size;
			short type;

			GamePacket() {
				type = BasicNetworkMessages::None;
				size = 0;
			}

			GamePacket(short type) {
				this->type = type;
				size = 0;
			}

			int GetTotalSize() {
				return sizeof(GamePacket) + size;
			}
		};

		struct StringPacket : public GamePacket {
			char	stringData[256];

			StringPacket(const std::string& message) {
				type = BasicNetworkMessages::String_Message;
				size = (short)message.length();

				memcpy(stringData, message.data(), size);
			};

			std::string GetStringFromData() {
				std::string realString(stringData);
				realString.resize(size);
				return realString;
			}
		};

		// Client sends chosen online nickname
		struct PlayerNamePacket : public GamePacket {
			std::string name;

			PlayerNamePacket(std::string n) {
				type = BasicNetworkMessages::Player_Name;
				size = sizeof(std::string) + sizeof(char) * n.size();
				name = n;
			}
		};

		// A highscore entry containing the player name and number of ball pushes
		struct HighscoreEntry {
			std::string playerName;
			int pushes;

			HighscoreEntry(std::string n, int i) {
				playerName = n;
				pushes = i;
			}
		};

		// Packet sent for avery highscore entry for a given level
		struct HighscorePacket : public GamePacket {
			HighscoreEntry highscore;
			int level;

			HighscorePacket(int lvl, HighscoreEntry score): highscore(score) {
				type = BasicNetworkMessages::Highscore;
				level = lvl;

				size += sizeof(std::string) + sizeof(char) * highscore.playerName.size() + sizeof(int) + 100;
				size += sizeof(std::string) + sizeof(char)  + sizeof(int);
				size += sizeof(int) * 2;
			}
		};

		// Informs client to clear all gameObjects etc
		struct DeleteLevelDataPacket : public GamePacket {
			DeleteLevelDataPacket() {
				type = BasicNetworkMessages::Delete_Level_Data;
			}

		};

		// Information about the ball push send to server
		struct InputPacket: public GamePacket {
			int playerID;
			Vector3 direction;
			Vector3 positionOfClick;

			InputPacket(int id, Vector3 dir, Vector3 pos) {
				type = BasicNetworkMessages::Ball_Push;
				size = sizeof(int) + sizeof(Vector3) * 2 + 32;

				playerID = id;
				direction = dir;
				positionOfClick = pos;
			}
		};

		// All information needed to recreate a game object on client's end
		struct CreateGameObjectPacket : public GamePacket {
			int objectID;
			Vector3 position;
			Vector3 scale;
			float friction;
			float restitution;
			float inverseMass;
			std::string name;
			std::string texture;
			Quaternion orientation;
			ObjectType objectType;
			ObjectTag tag;
			MeshType mesh;

			CreateGameObjectPacket(int id, Vector3 pos, Vector3 sc, float f, float r, float mass, std::string n, std::string tex, Quaternion ori , ObjectType t, ObjectTag tg, MeshType m) {
				type = BasicNetworkMessages::Create_Object;
				size = sizeof(int)* 4 + sizeof(Vector3) * 2 + sizeof(float) *3 + sizeof(std::string) + sizeof(std::string) + sizeof(char) * (tex.size() + n.size()) + sizeof(Quaternion) + 100;

				objectID = id;
				position = pos;
				scale = sc;
				friction = f;
				restitution = r;
				inverseMass = mass;
				orientation = ori;
				name = n;
				texture = tex;
				tag = tg;
				objectType = t;
				mesh = m;
			}
		};

		// Sent to inform clients of changes to dynamic objects
		struct UpdateGameObjectPacket : public GamePacket {
			int objectID;
			Vector3 position;
			Quaternion orientation;
			Vector3 linearVelocity;
			Vector3 angularVelocity;

			UpdateGameObjectPacket(int id, Vector3 pos, Quaternion ori, Vector3 linearV, Vector3 angularV) {
				type = BasicNetworkMessages::Update_Object;
				size = sizeof(Vector3) * 3 + sizeof(int) + sizeof(Quaternion) + 32;

				objectID = id;
				position = pos;
				orientation = ori;
				linearVelocity = linearV;
				angularVelocity = angularV;
			}
		};

		// Server sends back information about newly connected client
		struct CreatePlayerPacket: public GamePacket {
			int playerID;
			Vector3 position;
			int currentLevel;
			int enetID;

			CreatePlayerPacket(int p, Vector3 pos, int curr, int enet) {
				type = BasicNetworkMessages::Create_Player;
				size = sizeof(int) * 3 + sizeof(Vector3) * 2 + sizeof(Quaternion) + 32;

				playerID = p;
				enetID = enet;
				position = pos;
				currentLevel = curr;
			}
		};

		// Request to reset the player's position
		struct ResetPlayerPacket : public GamePacket {
			int playerID;

			ResetPlayerPacket(int id) {
				type = BasicNetworkMessages::Reset_Player;
				playerID = id;
				size = sizeof(int);
			}
		};

		struct PlayerDisconnectPacket : public GamePacket {
			int playerID;
			PlayerDisconnectPacket(int p) {
				type = BasicNetworkMessages::Player_Disconnected;
				playerID = p;
				size = sizeof(int);
			}
		};

		class PacketReceiver {
		public:
			virtual void ReceivePacket(int type, GamePacket* payload, int source = -1) = 0;
		};

		class NetworkBase {
		public:
			static void Initialise();
			static void Destroy();

			static int GetDefaultPort() { return 1234; }

			void RegisterPacketHandler(int msgID, PacketReceiver* receiver) {
				packetHandlers.insert(std::make_pair(msgID, receiver));
			}

		protected:
			NetworkBase();
			~NetworkBase();

			bool ProcessPacket(GamePacket* p, int peerID = -1);

			typedef std::multimap<int, PacketReceiver*>::const_iterator PacketHandlerIterator;

			bool GetPackethandlers(int msgID, PacketHandlerIterator& first, PacketHandlerIterator& last) const {
				auto range = packetHandlers.equal_range(msgID);

				if (range.first == packetHandlers.end()) {
					return false; //no handlers for this message type!
				}
				first = range.first;
				last = range.second;
				return true;
			}

			ENetHost* netHandle;

			std::multimap<int, PacketReceiver*> packetHandlers;
		};
	}
}