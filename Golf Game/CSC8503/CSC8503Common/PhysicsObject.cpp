#include "PhysicsObject.h"
#include "PhysicsSystem.h"
#include "../CSC8503Common/Transform.h"
using namespace NCL;
using namespace CSC8503;

PhysicsObject::PhysicsObject(Transform* parentTransform, const CollisionVolume* parentVolume)	{
	transform	= parentTransform;
	volume		= parentVolume;

	inverseMass = 1.0f;
	friction	= 0.8f;
	restitution = 0.66f;
}

PhysicsObject::~PhysicsObject()	{

}

void PhysicsObject::ApplyAngularImpulse(const Vector3& force) {
	angularVelocity += inverseInteriaTensor * force;
}

void PhysicsObject::ApplyLinearImpulse(const Vector3& force) {
	linearVelocity += force * inverseMass;
}

void PhysicsObject::AddForce(const Vector3& addedForce) {
	force += addedForce;
}

void PhysicsObject::AddImpulseAtPosition(const Vector3& addedForce, const Vector3& position) {
	ApplyLinearImpulse(addedForce);

	Vector3 localPos = transform->GetWorldPosition() - position;
	Vector3 angularForce = Vector3::Cross(addedForce, localPos);

	ApplyAngularImpulse(angularForce);
}

void PhysicsObject::AddImpulseAtPosition(const Vector3& addedForce, const Vector3& position, float dt) {
	AddForceAtPosition(addedForce / dt, position);
}

void PhysicsObject::AddForceAtPosition(const Vector3& addedForce, const Vector3& position) {
	Vector3 localPos = transform->GetWorldPosition() - position;

	force += addedForce * PhysicsSystem::UNIT_MULTIPLIER;
	torque += Vector3::Cross(addedForce, localPos);
}

void PhysicsObject::AddTorque(const Vector3& addedTorque) {
	torque += addedTorque;
}

void PhysicsObject::ClearForces() {
	force		= Vector3();
	torque		= Vector3();
}

void PhysicsObject::InitCubeInertia() {
	Vector3 dimensions	= transform->GetLocalScale() * 2.0f;
	Vector3 dimsSqr		= dimensions * dimensions;

	inverseInertia.x = (12.0f * inverseMass) / (dimsSqr.y + dimsSqr.z);
	inverseInertia.y = (12.0f * inverseMass) / (dimsSqr.x + dimsSqr.z);
	inverseInertia.z = (12.0f * inverseMass) / (dimsSqr.x + dimsSqr.y);
}

void PhysicsObject::InitSphereInertia() {
	float radius	= transform->GetLocalScale().GetMaxElement();
	float i			= 2.5f * inverseMass / (radius*radius);

	inverseInertia = Vector3(i, i, i);
}

void PhysicsObject::UpdateInertiaTensor() {
	Quaternion q = transform->GetWorldOrientation();
	
	Matrix3 invOrientation	= q.Conjugate().ToMatrix3();
	Matrix3 orientation		= q.ToMatrix3();

	inverseInteriaTensor = orientation * Matrix3::Scale(inverseInertia) *invOrientation;
}

void PhysicsObject::UpdateAtRest(float dt) {
	// Object should be at rest if velocities are small and no force is acting on it
	if (linearVelocity.Length() < 20.2f && angularVelocity.Length() < 0.1f && force.Length() < 0.0001f) {
		timeAtRest += dt; // accumulate the time the object is considered at rest

		// reset velocities and set at rest if object has not moved 
		// and no large enough forces acted on it for more then one second
		if (timeAtRest > 1.0f) { 
			atRest = true;
			linearVelocity = Vector3(0, 0, 0);
			angularVelocity = Vector3(0, 0, 0);
		}
	}
	else {
		atRest = false;
		timeAtRest = 0.0f;
	}
}
