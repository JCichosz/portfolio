#include "AnchorPointDistConstraint.h"
#include "Debug.h"


namespace NCL {
	namespace CSC8503 {
		AnchorPointDistConstraint::AnchorPointDistConstraint(GameObject * a, Vector3 anchorPosA, GameObject * b, Vector3 anchorPosB, float d) :
			objectA(a), anchorA(anchorPosA), objectB(b), anchorB(anchorPosB), distance(d) {
		}

		void AnchorPointDistConstraint::UpdateConstraint(float dt) {
			// Calculate current position of the anchor points
			Vector3 anchorPosA = (objectA->GetConstTransform().GetWorldPosition()) + (objectA->GetConstTransform().GetWorldOrientation().ToMatrix3() * anchorA);
			Vector3 anchorPosB = (objectB->GetConstTransform().GetWorldPosition()) + (objectB->GetConstTransform().GetWorldOrientation().ToMatrix3() * anchorB);

			Vector3 relativePos = anchorPosA - anchorPosB;

			float currentDistance = relativePos.Length();

			float offset = distance - currentDistance;

			if (abs(offset) > 0.0f) {
				Vector3 offsetDir = relativePos.Normalised();

				float dampingFactor = 1.0f - 0.97f;
				float damping = powf(dampingFactor, dt);

				PhysicsObject* physA = objectA->GetPhysicsObject();
				PhysicsObject* physB = objectB->GetPhysicsObject();

				Vector3 linearVelA = physA->GetLinearVelocity();
				Vector3 linearVelB = physB->GetLinearVelocity();

				// Heavily dampen to avoid energy explosion
				physA->SetLinearVelocity(linearVelA * damping);
				physB->SetLinearVelocity(linearVelB * damping);

				Vector3 angularVelA = physA->GetAngularVelocity();
				Vector3 angularVelB = physB->GetAngularVelocity();

				physA->SetAngularVelocity(angularVelA * damping);
				physB->SetAngularVelocity(angularVelB * damping);

				Vector3 relativeVel = physA->GetLinearVelocity() - physB->GetLinearVelocity();

				float constraintMass = physA->GetInverseMass() + physB->GetInverseMass();

				if (constraintMass > 0.0f && dt > 0.0f) {
					// Apply both angular and linear velocity to try and keep anchor points together
					// angular velocity applied in relation to anchor points to emulate door rotating 
					// around the anchor point
					Vector3 relativeA = anchorPosA - objectA->GetTransform().GetWorldPosition();
					Vector3 relativeB = anchorPosB - objectB->GetTransform().GetWorldPosition();

					Vector3 inertiaA = Vector3::Cross(relativeA, offsetDir);
					Vector3 inertiaB = Vector3::Cross(relativeB, offsetDir);

					float angularEffect = Vector3::Dot(inertiaA, physA->GetInertiaTensor() * inertiaA) + Vector3::Dot(inertiaB, physB->GetInertiaTensor() * inertiaB);

					float velocityDot = Vector3::Dot(relativeVel, offsetDir);
					float biasFactor = 0.005f;
					float bias = -(biasFactor / dt) * offset;

					float lambda = -(velocityDot + bias) / (constraintMass  + angularEffect);

					Vector3 aImpulse = offsetDir * lambda;
					Vector3 bImpulse = -offsetDir * lambda;

					physA->ApplyLinearImpulse(aImpulse);
					physB->ApplyLinearImpulse(bImpulse);

					if (physA->GetVolumeType()->type != VolumeType::AABB) {
						physA->ApplyAngularImpulse(Vector3::Cross(relativeA, aImpulse));
					}
					if (physB->GetVolumeType()->type != VolumeType::AABB) {
						physB->ApplyAngularImpulse(Vector3::Cross(relativeB, bImpulse));
					}
				}
			}
		}

	}
}