#pragma once
#include <thread>
#include <atomic>
#include "../CSC8503Common/Player.h"

#include "NetworkBase.h"

namespace NCL {
	namespace CSC8503 {
		class GameWorld;

		// Struct containg information about the players
		struct PlayerData {
			std::string playerName; // mane chosen by player at the start
			int enetID;				// client ID set by enet
			Player* playerObject;

			PlayerData(std::string name, Player* obj, int enet) {
				playerName = name;
				playerObject = obj;
				enetID = enet;
			}
		};

		class GameServer : public NetworkBase {
		public:
			GameServer(int onPort, int maxClients);
			~GameServer();

			bool Initialise();
			void Shutdown();

			void SetGameWorld(GameWorld &g);

			void ThreadedUpdate();

			bool SendGlobalMessage(int msgID);
			bool SendGlobalMessage(GamePacket& packet);

			void BroadcastSnapshot(bool deltaFrame);
			void UpdateMinimumState();

			virtual void UpdateServer();

			// Used to get unique IDs for game objects
			int GetNextID() { return nextID++; }

			void AddPlayer(Player* p, int enetID) { players[p->GetPlayerId()] =  new PlayerData(p->GetName(), p, enetID); }
			Player* GetPlayer(int i) { return players.at(i)->playerObject; }

			PlayerData* GetPlayerData(int i) { return players.at(i); }

			std::map<int, PlayerData*>& GetPlayers() { return players; }

			// Adds a highscore entry to a level leaderboard
			void AddHighscore(int lvl, HighscoreEntry entry) { highscores[lvl].push_back(entry); }
			std::map<int, std::vector<HighscoreEntry>>& GetHighscores() { return highscores; }

			// Get highscores from a specific level
			std::vector<HighscoreEntry> GetLevelHighscores(int lvl) { return highscores[lvl]; }

		protected:
			int			nextID;
			int			port;
			int			clientMax;
			int			clientCount;
			GameWorld*	gameWorld;

			std::atomic<bool> threadAlive;

			std::thread updateThread;

			int incomingDataRate;
			int outgoingDataRate;

			std::map<int, int> stateIDs;
			std::map<int, PlayerData*> players;
			std::map<int, std::vector<HighscoreEntry>> highscores;
		};
	}
}
