#include "Robot.h"


namespace NCL {
	namespace CSC8503 {
		Robot::Robot(Vector3 arenaDimensions, Vector3 arenaOrg): GameObject(ROBOT, DYNAMIC, "robot"), arenaAABB(AABBVolume(arenaDimensions)) {
			target = nullptr;
			arenaOrigin = arenaOrg;
			destroyed = false;
			timeChasingTarget = 0.0f;
		}

		Robot::~Robot() {
		}

		void Robot::OnCollisionBegin(GameObject * otherObject) {
			// If robot collided with a player with sufficient velocity, robot gets destroyed
			if (otherObject->GetObjectTag() == PLAYER && otherObject->GetPhysicsObject()->GetLinearVelocity().Length() > 60.0f) {
				destroyed = true;
				transform.SetWorldPosition(Vector3(0, -2500, 0));
			}
		}

		void Robot::Respawn() {
			destroyed = false;
			target = nullptr;
			timeChasingTarget = 0.0f;

			Vector3 respawnOffset = Vector3(((float)std::rand() / RAND_MAX) * 230, 0, ((float)std::rand() / RAND_MAX) * 350);
			transform.SetWorldPosition(Vector3(-130, 45, -2200) + respawnOffset);
		}
	}
}