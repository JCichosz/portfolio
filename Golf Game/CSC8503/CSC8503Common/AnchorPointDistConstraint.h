#pragma once
#include "Constraint.h"
#include "../../Common/Vector3.h"
#include "GameObject.h"

/*
 * A distance constraint that can be set up on arbitrary points of a mesh.
 * The anchor points are specified in local mesh space (middle of the left face of
 * a 10x10x10 cube would be Vector3(-10,0,0)).
 */

namespace NCL {
	namespace CSC8503 {
		class AnchorPointDistConstraint : public Constraint {
		public:
			AnchorPointDistConstraint(GameObject* a, Vector3 anchorPosA, GameObject* b, Vector3 anchorPosB, float distance);
			virtual ~AnchorPointDistConstraint() {}

			void UpdateConstraint(float dt) override;

		protected:
			GameObject* objectA;
			GameObject* objectB;

			Vector3 anchorA;
			Vector3 anchorB;

			float distance;
		};
	}
}
