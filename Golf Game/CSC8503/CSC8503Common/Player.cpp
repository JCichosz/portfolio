#include "Player.h"

using namespace NCL::CSC8503;

Player::Player(int id, Vector3 pos): GameObject(PLAYER, DYNAMIC, "player"), playerId(id), initialPos(pos) {
	reachedGoal = false;
}

Player::~Player() {
}

void NCL::CSC8503::Player::ResetPositionAndVelocity() {
	transform.SetWorldPosition(initialPos);
	physicsObject->SetAngularVelocity(Vector3(0, 0, 0));
	physicsObject->SetLinearVelocity(Vector3(0, 0, 0));
}

void Player::OnCollisionBegin(GameObject * otherObject) {
	if (otherObject->GetObjectTag() == GOAL) {
		reachedGoal = true;
	}
	else if (otherObject->GetObjectTag() == RESET_PLAYER_OBSTACLE) {
		ResetPositionAndVelocity();
	}
	// If collided with robot with a smaller velocity, player gets reset to the start
	// of the robot's arena
	else if(otherObject->GetObjectTag() == ROBOT) {
		if(physicsObject->GetLinearVelocity().Length() < 60.0f) {
			transform.SetWorldPosition(Vector3(900, 0, -1450));
			physicsObject->SetAngularVelocity(Vector3(0, 0, 0));
			physicsObject->SetLinearVelocity(Vector3(0, 0, 0));
		}
	}
}
