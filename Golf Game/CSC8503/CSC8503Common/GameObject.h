#pragma once
#include "Transform.h"
#include "CollisionVolume.h"
#include "StateMachine.h"
#include "PhysicsObject.h"
#include "RenderObject.h"
#include <vector>

using std::vector;

namespace NCL {
	namespace CSC8503 {

		// Specifies whether the object will move during gamplay
		enum ObjectType {
			STATIC,
			DYNAMIC
		};

		// Object tags to more easily distinguish between object types
		enum ObjectTag {
			PLAYER,
			GOAL,
			ROBOT,
			FLOOR,
			WALL,
			OBSTACLE,
			RESET_PLAYER_OBSTACLE
		};

		// Informs the client which mesh to use when creating an object
		enum MeshType {
			SPHERE,
			CUBE,
			ORIENTED_CUBE
		};

		class GameObject	{
		public:
			GameObject(ObjectTag tag, ObjectType t = STATIC, string name = "");
			~GameObject();

			// ID used to identify objects between server & clients 
			void SetNetworkID(int id) { objectID = id; }
			int GetNetworkID() { return objectID; }

			void SetBoundingVolume(CollisionVolume* vol)		{ boundingVolume = vol; }

			const CollisionVolume* GetBoundingVolume() const	{ return boundingVolume; }

			bool IsActive() const								{ return isActive; } 

			const Transform& GetConstTransform() const			{ return transform; }

			Transform& GetTransform()							{ return transform; }

			RenderObject* GetRenderObject() const				{ return renderObject; }

			PhysicsObject* GetPhysicsObject() const				{ return physicsObject;	}

			StateMachine* GetStateMachine() const				{ return stateMachine; }

			void SetRenderObject(RenderObject* newObject)		{ renderObject = newObject; }

			void SetPhysicsObject(PhysicsObject* newObject)		{ physicsObject = newObject; }

			void SetStateMachine(StateMachine* sm)				{ stateMachine = sm; }

			const string& GetName() const						{ return name; }
			void SetName(const std::string n)					{ name = n; }

			const ObjectType GetObjectType() const				{ return objectType; }
			const ObjectTag GetObjectTag() const				{ return objectTag; }

			void SetTexture(std::string tex)					{ texture = tex; }
			std::string GetTexture()							{ return texture; }

			void SetMeshType(MeshType t)						{ meshType = t; }
			MeshType GetMeshType()								{ return meshType; }

			virtual void OnCollisionBegin(GameObject* otherObject) {
				//std::cout << "OnCollisionBegin event occured!\n";
			}

			virtual void OnCollisionEnd(GameObject* otherObject) {
				//std::cout << "OnCollisionEnd event occured!\n";
			}

			bool InsideAABB(const Vector3& pos, const Vector3& halfSize);

			bool GetBroadphaseAABB(Vector3& outsize) const;
			void UpdateBroadphaseAABB();

	

		protected:
			int objectID = -1;

			Transform			transform;
			std::string			texture;
			CollisionVolume*	boundingVolume;
			PhysicsObject*		physicsObject;
			RenderObject*		renderObject;
			StateMachine*		stateMachine; // logic attached to the object

			bool	isActive;
			string	name;

			Vector3 broadphaseAABB;

			ObjectType	objectType;
			ObjectTag	objectTag;
			MeshType	meshType;
		};
	}
}

