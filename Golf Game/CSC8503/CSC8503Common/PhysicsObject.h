#pragma once
#include "../../Common/Vector3.h"
#include "../../Common/Matrix3.h"

using namespace NCL::Maths;

namespace NCL {
	class CollisionVolume;
	
	namespace CSC8503 {
		class Transform;

		class PhysicsObject	{
		public:
			PhysicsObject(Transform* parentTransform, const CollisionVolume* parentVolume);
			~PhysicsObject();

			Vector3 GetLinearVelocity() const {
				return linearVelocity;
			}

			Vector3 GetAngularVelocity() const {
				return angularVelocity;
			}

			Vector3 GetTorque() const {
				return torque;
			}

			Vector3 GetForce() const {
				return force;
			}

			void SetInverseMass(float invMass) {
				inverseMass = invMass;
			}

			float GetInverseMass() const {
				return inverseMass;
			}

			void SetRestitution(float res) {
				restitution = res;
			}

			float GetRestitution() const {
				return restitution;
			}

			void SetFriction(float f) { friction = f; }
			float GetFriction() const { return friction; }

			void ApplyAngularImpulse(const Vector3& force);
			void ApplyLinearImpulse(const Vector3& force);
			
			void AddImpulseAtPosition(const Vector3& addedForce, const Vector3& position);
			void AddImpulseAtPosition(const Vector3& addedForce, const Vector3& position, float dt);

			void AddForce(const Vector3& force);

			void AddForceAtPosition(const Vector3& force, const Vector3& position);

			void AddTorque(const Vector3& torque);

			const CollisionVolume* GetVolumeType() const { return volume; }

			void ClearForces();

			void SetLinearVelocity(const Vector3& v) {
				linearVelocity = v;
			}

			void SetAngularVelocity(const Vector3& v) {
				angularVelocity = v;
			}

			void SetInverseInertia(const Vector3 inertia) { inverseInertia = inertia; }

			void InitCubeInertia();
			void InitSphereInertia();

			void UpdateInertiaTensor();

			Matrix3 GetInertiaTensor() const {
				return inverseInteriaTensor;
			}

			void SetAtRest(bool b) { atRest = b; }
			bool AtRest() const { return atRest; }

			// Checks if the object has not been moving for a while, if so sets it to be at rest
			void UpdateAtRest(float dt);

		protected:
			const CollisionVolume* volume;
			Transform*		transform;

			float inverseMass;
			float friction;
			float restitution;

			bool atRest;
			float timeAtRest;

			//linear 
			Vector3 linearVelocity;
			Vector3 force;

			//angular 
			Vector3 angularVelocity;
			Vector3 torque;
			Vector3 inverseInertia;
			Matrix3 inverseInteriaTensor;
		};
	}
}

