# Portfolio

## Golf Game
A physics-based golf game, which can be played in multiplayer.

## Graphics Demo
A set of scenes showcasing various graphical features.

## Horse Kinematics
Includes scripts created for the Unity project. Main algorithm file is named  **HorseLocomotion.cs**. <br />

## Game Engine
A 3D game engine utilising middleware to create an arcade game. The player controls a dragon and collects hovering rings.<br />

## Shaders
Coursework focusing on implementing various effects using glsl shaders - located in the **OpenGLGraphics/Shaders** folder. <br />
Currently only runs in debug due to old version of the SOIL library. <br />

## Mobile App
The folder contains only manually created/modified files, not the actual Android Studio project. <br />



