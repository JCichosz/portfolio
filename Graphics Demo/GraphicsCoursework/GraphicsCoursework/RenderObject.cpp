#include "RenderObject.h"
#include "Assets.h"
#include "../nclgl/Camera.h"

RenderObject::RenderObject() {
	mesh = nullptr;
	shader = "";
	modelScale = Vector3(1, 1, 1);
	boundingRadius = 0.0f;
	texture = 0;
	bumpMap = 0;
	transparent = false;
	frontCull = false;
	shineFactor = 0.2;
}


RenderObject::~RenderObject() {

}

void RenderObject::draw(Matrix4 viewMatrix, Matrix4 projectionMatrix, Matrix4 textureMatrix, Matrix4 worldTransform, float msec, float timeElapsed) {
	GLuint program = Assets::getShader(shader)->GetProgram();

	glUseProgram(program);

	if (shader == "reflection") {
		glUniform1i(glGetUniformLocation(program, "cubeTex"), 15);
		glActiveTexture(GL_TEXTURE15);
		glBindTexture(GL_TEXTURE_CUBE_MAP, Assets::getTexture("defaultSkybox"));

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, false, (float*)&projectionMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "textureMatrix"), 1, false, (float*)&textureMatrix);

	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, false,	(float*)&(worldTransform*Matrix4::Scale(modelScale)));
	glUniform1f(glGetUniformLocation(program, "msec"), msec);
	glUniform1f(glGetUniformLocation(program, "elapsed"), timeElapsed);
	glUniform1f(glGetUniformLocation(program, "shine"), shineFactor);
	glUniform1i(glGetUniformLocation(program, "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(program, "bumpTex"), 5);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, bumpMap);
	glActiveTexture(GL_TEXTURE0);
	mesh->Draw();

	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
}

void RenderObject::update(float msec) {
}
