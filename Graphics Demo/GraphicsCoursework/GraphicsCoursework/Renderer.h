#pragma once
#include "../nclgl/OGLRenderer.h"
#include "../nclgl/SceneNode.h"
#include "../nclgl/Camera.h"
#include "../nclgl/Frustum.h"
#include <map>
#include "Scene.h"
#include "gltext.h"

const int NUM_SCENES = 3;

class Renderer: public OGLRenderer {
public:
	Renderer(Window& w);
	virtual ~Renderer();

	virtual void UpdateScene(float msec) override;

	void RenderScene();
	void renderWithShadows();

	int getCurrentScene() const { return currentScene; }
	int getNumScenes() const { return NUM_SCENES; }

	bool usingShadows() const { return shadows; }
	void setShadows(bool b) { shadows = b; }

	void renderToScreen(int fps);
	void drawSkybox(RenderObject* ro);

	GLuint getSceneFbo()  const { return sceneFBO; }
	GLuint& getColourAttachment(int i) { return sceneColourTexture[i]; }
	GLuint& getDepthAttachment() { return sceneDepthTexture; }

	RenderObject* getScreenQuad() const { return screenQuad; }

	float getTimeElapsed()  const { return timeElapsed; }

	void switchScene(int number);

	void initialiseScenes();

	void drawShadows(vector<Light*>& lights);
	void drawCombined(vector<Light*>& lights, Shader* s);

protected:
	GLTtext* fpsText;

	RenderObject* screenQuad;

	GLuint sceneFBO;
	GLuint sceneColourTexture[2];
	GLuint sceneDepthTexture;

	GLuint shadowFBO;

	int currentScene;
	float msec;
	float timeElapsed = 0;

	Camera* camera;
	Frustum frameFrustum;

	Scene* scenes[NUM_SCENES];

	void drawNodeList(vector<SceneNode*>& nodes);
	void drawShadowMeshes(vector<SceneNode*>& nodes, GLuint program);

	void setCameraAndEffects();
	void updateLightUniforms(vector<Light*> lights, GLuint program);

	bool shadows;
};

