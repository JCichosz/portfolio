#include "ParticleEmitter.h"


ParticleEmitter::ParticleEmitter(int num, std::string tex) : numParticles(num) {
	texture = Assets::getTexture(tex);
	particles = new ParticleMesh(numParticles);

	for (int i = 0; i < numParticles; ++i) {
		Vector3 initialPosition = Vector3((float)rand() / RAND_MAX+ 1.0f, (float)rand() / RAND_MAX * 0.5f + 1.0f, -1.0f);
		Vector4 colour = Vector4((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX, 1.0f);
		Vector3 direction = Vector3(-(float)rand() / RAND_MAX * 2.5f + 0.2f, -(float)rand() / RAND_MAX * 2.5f, 1.0f);
		direction.Normalise();
		float speed = (float)rand() / RAND_MAX * 3.0f + 1.0f;
		float rotation = (float)rand() / RAND_MAX * 180.0f;
		float size = (float)rand() / RAND_MAX * 0.03f + 0.01f;
		float lifetime = (float)rand() / RAND_MAX * 4.0f + 3.5f;

		(*particles)[i] = Particle(initialPosition, direction, rotation, speed, size, lifetime, colour);
	}
}

ParticleEmitter::~ParticleEmitter() {
	delete particles;
}

void ParticleEmitter::updateParticles(float msec) {
	for (int i = 0; i < numParticles; ++i) {
		(*particles)[i].update(msec);
	}
}

void ParticleEmitter::drawParticles(Matrix4 viewMatrix, Matrix4 projectionMatrix, float msec) {
	GLuint program = Assets::getShader("particle")->GetProgram();
	glUseProgram(program);

	glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, false, (float*)&projectionMatrix);
	glUniform1f(glGetUniformLocation(program, "msec"), msec);
	glUniform1i(glGetUniformLocation(program, "diffuseTex"), 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	particles->draw();

	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
}
