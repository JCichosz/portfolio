#include "CubeMap.h"
#include "RenderObject.h"
#include "Assets.h"

CubeMap::CubeMap() {
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
}


CubeMap::~CubeMap() {
}

void CubeMap::draw(Matrix4 viewMatrix, Matrix4 projectionMatrix, Matrix4 textureMatrix, Matrix4 worldTransform, float msec, float timeElapsed) {
	GLuint program = Assets::getShader(shader)->GetProgram();
	glUseProgram(program);

	glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, false, (float*)&projectionMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, false, (float*)&(worldTransform*Matrix4::Scale(modelScale)));
	glUniformMatrix4fv(glGetUniformLocation(program, "textureMatrix"), 1, false, (float*)&textureMatrix);

	glUniform1f(glGetUniformLocation(program, "msec"), msec);
	glUniform1f(glGetUniformLocation(program, "elapsed"), timeElapsed);
	glUniform1i(glGetUniformLocation(program, "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(program, "bumpTex"), 1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, bumpMap);

	mesh->Draw();

	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
}
