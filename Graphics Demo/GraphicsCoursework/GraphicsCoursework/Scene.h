#pragma once
#include "../nclgl/SceneNode.h"
#include "../nclgl/Light.h"
#include "ParticleEmitter.h"

class Frustum;

class Scene {
public:
	Scene();
	~Scene();

	void update(float msec, float timeElapsed, int current, float pitch, float yaw);

	void activate() { root->activate(); }
	void deactivate() { root->deactivate(); }

	static Scene* initialiseSceneOne();
	static Scene* initialiseSceneTwo();
	static Scene* initialiseSceneThree();

	SceneNode* getRoot() const { return root; }

	RenderObject* getSkybox() const { return skybox; }
	void setSkybox(RenderObject* ro) { skybox = ro; }

	Vector3 getInitialCameraPos() const { return initialCameraPos; }
	float getInitialPitch() const { return initialPitch;	}
	float getInitialYaw() const { return initialYaw; }

	void addTransparent(SceneNode* node) { transparent.push_back(node); }
	void addOpaque(SceneNode* node) { opaque.push_back(node); }
	void addFrontFaceCull(SceneNode* node) { frontFaceCull.push_back(node); }

	void buildNodeLists(SceneNode* from, Frustum& frameFrustum, Vector3 cemeraPosition);
	void sortNodeLists();
	void clearNodeLists();

	void addLight(Light* l) { lights.push_back(l); }
	void addEmitter(ParticleEmitter* p) { emitters.push_back(p); }
	void addSceneNode(std::string name, SceneNode* node) { nodes.emplace(name, node); }

	SceneNode* getSceneNode(std::string name) const { return nodes.at(name); }

	vector<SceneNode*>& getTransparentList() { return transparent; }
	vector<SceneNode*>& getOpaqueList() { return opaque; }
	vector<SceneNode*>& getFrontFaceCullList() { return frontFaceCull; }

	vector<Light*>& getLights() { return lights; }
	vector<ParticleEmitter*>& getEmitters() { return emitters; }


protected:
	SceneNode* root;
	RenderObject* skybox;

	vector<Light*> lights;
	vector<ParticleEmitter*> emitters;

	map<std::string, SceneNode*> nodes;

	vector<SceneNode*> transparent;
	vector<SceneNode*> opaque;
	vector<SceneNode*> frontFaceCull;

	Vector3 initialCameraPos;
	float initialPitch;
	float initialYaw;

	int sceneNumber;

	static Matrix4 treePos;
};

