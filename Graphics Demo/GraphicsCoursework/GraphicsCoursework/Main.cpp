#include "Assets.h"
#include "Renderer.h"
#include "PostProcessing.h"
#include "gltext.h"
#include <chrono>

int main() {
	int width = 1920;
	int height = 1080;
	Window w("Advanced Graphics", width, height, false);

	if (!w.HasInitialised()) {
		return -1;
	}
	Renderer renderer(w);
	if (!renderer.HasInitialised()) {
		return -1;
	}

	
	Assets::initialise();

	PostProcessing postProcessing(w.GetScreenSize().x, w.GetScreenSize().y);
	renderer.initialiseScenes();

	w.LockMouseToWindow(true);
	w.ShowOSPointer(false);

	float timeInScene = 0.0f;
	w.GetTimer()->GetTimedMS();

	bool autoCycle = true;

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		auto start = std::chrono::high_resolution_clock::now();

		float msec = w.GetTimer()->GetTimedMS();
		renderer.UpdateScene(msec);
		if(autoCycle) timeInScene += msec * 0.001f;

		if (renderer.usingShadows()) renderer.renderWithShadows();
		else renderer.RenderScene();

		postProcessing.addPostProcessing(renderer.getColourAttachment(0), renderer.getColourAttachment(1), renderer.getScreenQuad(), msec, renderer.getTimeElapsed());
		
		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> timeTaken = end - start;
		int fps = 1 / timeTaken.count();

		renderer.renderToScreen(fps);

		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_LEFT)) {
			int next = renderer.getCurrentScene() - 1;
			if(next >= 0) renderer.switchScene(next);
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_RIGHT)) {
			int next = renderer.getCurrentScene() + 1;
			if (next < renderer.getNumScenes()) renderer.switchScene(next);
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_PAUSE)) {
			autoCycle = !autoCycle;
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_C)) {
			Camera::setFreecam(!Camera::getFreecam());
			if (Camera::getFreecam() == true) autoCycle = false;
		}
		if (autoCycle && timeInScene > 10.0f) {
			int next = renderer.getCurrentScene() + 1;
			if (next >= renderer.getNumScenes()) next = 0;
			timeInScene = 0.0f;
			renderer.switchScene(next);
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_NUMPAD1)) {
			PostProcessing::setBloom(!PostProcessing::getBloom());
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_NUMPAD2)) {
			PostProcessing::setPaperEffect(!PostProcessing::getPaperEffect());
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_NUMPAD3)) {
			PostProcessing::setToneMapping(!PostProcessing::getToneMapping());
		}
	}


	Assets::clear();
	return 0;
}