#include "Assets.h"
#include "../nclgl/OBJMesh.h"
#include "../nclgl/HeightMap.h"
#include "CubeMap.h"
#include "../nclgl/MD5FileData.h"

map<std::string, Mesh*> Assets::meshes;
map<std::string, GLuint> Assets::textures;
map<std::string, Shader*> Assets::shaders;

MD5FileData* Assets::hellknightData;

void Assets::initialise() {
	loadMeshes();
	loadTextures();
	loadShaders();
	loadHellknight();
}

void Assets::loadObjMesh(std::string name, std::string filepath) {
	OBJMesh* m = new OBJMesh();
	m->LoadOBJMesh(filepath);
	meshes.emplace(name, m);
}

void Assets::loadMeshes() {
	loadObjMesh("dragon", MESHES"dragon.obj");
	loadObjMesh("amaterasu", MESHES"amaterasu.obj");
	loadObjMesh("sphere", MESHES"sphere.obj");
	loadObjMesh("cone", MESHES"cone.obj");
	loadObjMesh("hollowCone", MESHES"coneNoFace.obj");
	loadObjMesh("cube", MESHES"cube.obj");

	HeightMap* ammyTerrain = new HeightMap(TEXTURES"terrain.raw");
	meshes.emplace("ammyTerrain", ammyTerrain);

	HeightMap* beachTerrain = new HeightMap(TEXTURES"beachHeightmap.raw");
	meshes.emplace("beachTerrain", beachTerrain);

	Mesh* quad = Mesh::GenerateQuad();
	meshes.emplace("quad", quad);

	Mesh* patches = Mesh::GeneratePatchesQuad();
	meshes.emplace("patches", patches);

}

void Assets::loadTexture(std::string name, std::string path, bool repeat) {
	GLuint tex = SOIL_load_OGL_texture(path.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	textures.emplace(name, tex);

	if(repeat) repeatTexture(tex);
}

void Assets::loadTextures() {
	loadTexture("metal", TEXTURES"gold.png", true);
	loadTexture("paper", TEXTURES"watercolorPaper-Vecteezy.png", false);
	loadTexture("black", TEXTURES"black.png", false);
	loadTexture("flatNormals", TEXTURES"flat_normal.png", true);
	loadTexture("petal", TEXTURES"cherryPetal.png", false);
	loadTexture("guardianSapling", TEXTURES"guardianSapling.png", false);
	loadTexture("dragonNormals", TEXTURES"dragonNormals.png", false);
	loadTexture("ammyColour", TEXTURES"amaterasu_color.png", true);
	loadTexture("ammyNormals", TEXTURES"amaterasu_normal.png", true);
	loadTexture("okamiGrass", TEXTURES"okami_grass.png", true);
	loadTexture("rockyGrass", TEXTURES"rockyGrass.png", true);
	loadTexture("fire", TEXTURES"fireSeamless.png", true);
	loadTexture("water", TEXTURES"beachWater.png", true);
	loadTexture("sand", TEXTURES"sand.png", true);
	loadTexture("brick", TEXTURES"brick.tga", true);
	loadTexture("brickNormals", TEXTURES"brickDOT3.tga", true);
	loadTexture("hellknight", TEXTURES"hellknight.tga", true);
	loadTexture("hellknightBump", TEXTURES"hellknight_local.tga", true);

	GLuint okamiSkybox = SOIL_load_OGL_cubemap(TEXTURES"okami_sky_right.png", TEXTURES"okami_sky_left.png",
		TEXTURES"okami_sky_up.png", TEXTURES"okami_sky_down.png", TEXTURES"okami_sky_back.png", TEXTURES"okami_sky_front.png",
		SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, 0);
	textures.emplace("okamiSkybox", okamiSkybox);

	GLuint okamiNightSkybox = SOIL_load_OGL_cubemap(TEXTURES"okami_night_right.png", TEXTURES"okami_night_left.png",
		TEXTURES"okami_night_up.png", TEXTURES"okami_night_down.png", TEXTURES"okami_night_back.png", TEXTURES"okami_night_front.png",
		SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, 0);
	textures.emplace("okamiNightSkybox", okamiNightSkybox);

	GLuint defaultSkybox = SOIL_load_OGL_cubemap(TEXTURES"rusted_west.jpg", TEXTURES"rusted_east.jpg",
		TEXTURES"rusted_up.jpg", TEXTURES"rusted_down.jpg", TEXTURES"rusted_south.jpg", TEXTURES"rusted_north.jpg",
		SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, 0);
	textures.emplace("defaultSkybox", defaultSkybox);
}


void Assets::loadShader(std::string name, std::string vertex, std::string fragment) {
	Shader* s = new Shader(VERTEX + vertex, FRAGMENT + fragment);
	shaders.emplace(name, s);

	if (!s->LinkProgram()) {
		std::cout << "Failed to link " << name << " shader! \n";
	}
}

void Assets::loadShaders() {
	loadShader("basic", VERTEX"TexturedVertex.glsl", FRAGMENT"TexturedFragment.glsl");
	loadShader("skybox", VERTEX"skyboxVertex.glsl", FRAGMENT"skyboxFragment.glsl");
	loadShader("skyboxBlend", VERTEX"skyboxVertex.glsl", FRAGMENT"skyboxBlendFragment.glsl");
	loadShader("bump", VERTEX"bumpVertex.glsl", FRAGMENT"bumpFragment.glsl");
	loadShader("spotlight", VERTEX"bumpVertex.glsl", FRAGMENT"spotlightFragment.glsl");
	loadShader("paper", VERTEX"textureVertexPassThrough.glsl", FRAGMENT"paperFragment.glsl");
	loadShader("toneMapping", VERTEX"textureVertexPassThrough.glsl", FRAGMENT"toneMappingFragment.glsl");
	loadShader("drawBright", VERTEX"textureVertexPassThrough.glsl", FRAGMENT"drawBrightFragment.glsl");
	loadShader("bloom", VERTEX"textureVertexPassThrough.glsl", FRAGMENT"bloomFragment.glsl");
	loadShader("bloomResult", VERTEX"textureVertexPassThrough.glsl", FRAGMENT"bloomResultFragment.glsl");
	loadShader("screenQuad", VERTEX"textureVertexPassThrough.glsl", FRAGMENT"TexturedFragment.glsl");
	loadShader("shadow", VERTEX"shadowVert.glsl", FRAGMENT"shadowFrag.glsl");
	loadShader("shadowSceneBump", VERTEX"shadowscenevert.glsl", FRAGMENT"shadowscenefragBump.glsl");
	loadShader("hellShader", VERTEX"skeletonVertex.glsl", FRAGMENT"skeletonFragment.glsl");
	loadShader("reflection", VERTEX"PerPixelVertex.glsl", FRAGMENT"reflectFragment.glsl");

	Shader* particle = new Shader(VERTEX"particleVertex.glsl", FRAGMENT"particleFragment.glsl", GEOMETRY"geometryQuadParticle.glsl");
	shaders.emplace("particle", particle);
	if (!particle->LinkProgram()) {
		std::cout << "Failed to link particle shader! \n";
	}

	Shader* tesselation = new Shader(VERTEX"PerPixelVertex.glsl", FRAGMENT"reflectFragment.glsl", 
		"", TESSELATION"basicTessellation.glsl", TESSELATION"evaluationTess.glsl");
	shaders.emplace("tesselation", tesselation);

	if (!tesselation->LinkProgram()) {
		std::cout << "Failed to link textureMove shader! \n";
	}
}


void Assets::loadHellknight() {
	hellknightData = new MD5FileData(MESHES"hellknight.md5mesh");
	hellknightData->AddAnim(ANIMATIONS"idle2.md5anim");
	hellknightData->AddAnim(ANIMATIONS"attack2.md5anim");
	hellknightData->AddAnim(ANIMATIONS"walk7.md5anim");
}

void Assets::repeatTexture(GLuint target) {
	glBindTexture(GL_TEXTURE_2D, target);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);
}


void Assets::clear() {
	for (auto m : meshes) {
		delete m.second;
	}
	for (auto t : textures) {
		glDeleteTextures(1, &(t.second));
	}
	for (auto s : shaders) {
		delete s.second;
	}
}

Mesh* Assets::getMesh(std::string name) {
	return meshes.at(name);
}

GLuint Assets::getTexture(std::string name) {
	return textures.at(name);
}

Shader* Assets::getShader(std::string name) {
	return shaders.at(name);
}
