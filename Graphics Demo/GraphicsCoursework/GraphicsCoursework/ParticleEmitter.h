#pragma once
#include "RenderObject.h"
#include "Particle.h"
#include "Assets.h"
#include "ParticleMesh.h"

class ParticleEmitter {
public:
	ParticleEmitter(int num = 100, std::string tex = "");
	~ParticleEmitter();

	GLuint getTexture() const { return texture; }
	void setTexture(std::string tex) { texture = Assets::getTexture(tex); }

	void updateParticles(float msec);
	void drawParticles(Matrix4 viewMatrix, Matrix4 projectionMatrix, float msec);

protected:
	int numParticles;
	GLuint texture;
	ParticleMesh* particles;
};

