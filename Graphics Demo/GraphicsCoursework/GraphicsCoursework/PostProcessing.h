#pragma once
#include "GL/glew.h"
#include "GL/wglew.h"
#include "../nclgl/Matrix4.h"
#include "RenderObject.h"

#define BLOOM_PASSES 1
#define MOTION_BLUR_PASSES 3

class PostProcessing {
public:
	PostProcessing(int w, int h);
	~PostProcessing();

	static void setBloom(bool b) { bloom = b; }
	static bool getBloom() { return bloom; }

	static void setPaperEffect(bool b) { paperEffect = b; }
	static bool getPaperEffect() { return paperEffect; }

	static void setToneMapping(bool b) { toneMapping = b; }
	static bool getToneMapping() { return toneMapping; }

	void addPostProcessing(GLuint& colourTexOne, GLuint& colourTexTwo, RenderObject* quad, float msec, float timeElapsed);

protected:
	static bool bloom;
	static bool paperEffect;
	static bool toneMapping;

	GLuint postProcessFBO;
	GLuint brightnessTexture;

	int width;
	int height;

	void updateMatrices(GLuint program, Matrix4& viewMatrix, Matrix4& projMatrix, GLuint effect, float msec, float timeElapsed);

	void swapTextures(GLuint& colourTexOne, GLuint& colourTexTwo);
};

