#include "ParticleMesh.h"

ParticleMesh::ParticleMesh(int num): numParticles(num) {
	glGenVertexArrays(1, &arrayObject);
	glBindVertexArray(arrayObject);

	glGenBuffers(1, &bufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, bufferObject);
	glBufferStorage(GL_ARRAY_BUFFER, numParticles * sizeof(Particle), nullptr, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)offsetof(Particle, position));
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)offsetof(Particle, colour));
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)offsetof(Particle, rotation));
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)offsetof(Particle, size));
	glEnableVertexAttribArray(3);

	data = (Particle*)glMapBufferRange(GL_ARRAY_BUFFER, 0, numParticles * sizeof(Particle), GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

ParticleMesh::~ParticleMesh() {
	glBindBuffer(GL_ARRAY_BUFFER, bufferObject);
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glDeleteVertexArrays(1, &arrayObject);
	delete[] data;
}

void ParticleMesh::draw() {
	glBindVertexArray(arrayObject);
	glDrawArrays(GL_POINTS, 0, numParticles);

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}


