#include "Particle.h"


Particle::Particle(Vector3 initialPos, Vector3 dir, float rot, float spd, float s, float life, Vector4 col):
initialPosition(initialPos), position(initialPos),
colour(col),
directionOfMovement(dir),
speed(spd),
rotation(rot),
size(s),
lifetime(life),
age(0.0f){
}

Particle::~Particle() {
}

void Particle::update(float msec) {
	float time = msec * 0.0001f;
	age += time;
	if (age > lifetime) {
		position = initialPosition;
		age = 0.0f;
	}

	rotation += time;
	position += directionOfMovement * time * speed;
}
