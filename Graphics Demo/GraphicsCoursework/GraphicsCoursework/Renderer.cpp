#include "Renderer.h"
#include "Assets.h"
#include "../nclgl/HeightMap.h"
#include "CubeMap.h"
#include "ParticleEmitter.h"
#include "PostProcessing.h"
#include <string>

Renderer::Renderer(Window& w): OGLRenderer(w) {
	camera = new Camera(0.0f, 0.0f, Vector3(0,0,0));
	projMatrix = Matrix4::Perspective(1.0f, 10000.0f, (float)width / (float)height, 45.0f);
	shadowProjMatrix = Matrix4::Perspective(1.0f, 500.0f, 1.0f, 90.0f);

	shadows = true;
	screenQuad = new RenderObject();

	gltInit();
	fpsText = gltCreateText();
	gltColor(1.0f, 1.0f, 1.0f, 1.0f);

	glGenTextures(1, &sceneDepthTexture);
	glBindTexture(GL_TEXTURE_2D, sceneDepthTexture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);

	for (int i = 0; i < 2; i++) {
		glGenTextures(1, &sceneColourTexture[i]);
		glBindTexture(GL_TEXTURE_2D, sceneColourTexture[i]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	glGenFramebuffers(1, &sceneFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, sceneDepthTexture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, sceneDepthTexture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, sceneColourTexture[0], 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE || !sceneDepthTexture || !sceneColourTexture[0]) {
		std::cout << "Failed to attach to Frame buffer! \n";
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	glGenFramebuffers(1, &shadowFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
	glDrawBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	init = true;
}


Renderer::~Renderer() {
	delete screenQuad;
	glDeleteTextures(2, sceneColourTexture);
	glDeleteTextures(1, &sceneDepthTexture);
	glDeleteFramebuffers(1, &sceneFBO);
	glDeleteFramebuffers(1, &shadowFBO);


	gltDeleteText(fpsText);
	gltTerminate();

	delete camera;

	for (int i = 0; i < NUM_SCENES; i++) {
		delete scenes[i];
	}
}

void Renderer::UpdateScene(float m) {
	msec = m;
	camera->UpdateCamera(msec);
	viewMatrix = camera->BuildViewMatrix();

	frameFrustum.FromMatrix(projMatrix*viewMatrix);

	timeElapsed += msec  * 0.0005f;
	textureMatrix = Matrix4::Translation(Vector3(0.0f, 0.0f, 1.0f) * timeElapsed);

	scenes[currentScene]->update(msec, timeElapsed, currentScene, camera->GetPitch(), camera->GetYaw());
}

void Renderer::RenderScene() {
	glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, sceneColourTexture[0], 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	scenes[currentScene]->clearNodeLists();
	scenes[currentScene]->buildNodeLists(scenes[currentScene]->getRoot(), frameFrustum, camera->GetPosition());
	scenes[currentScene]->sortNodeLists();

	if(scenes[currentScene]->getSkybox()) drawSkybox(scenes[currentScene]->getSkybox());

	if (!scenes[currentScene]->getLights().empty()) {
		vector<Light*> lights = scenes[currentScene]->getLights();
		Shader* lightShader = Assets::getShader("bump");
		GLuint program = lightShader->GetProgram();
		glUseProgram(program);
		updateLightUniforms(lights, program);
		glUniform3fv(glGetUniformLocation(program, "cameraPos"), 1, (float*)&camera->GetPosition());

		lightShader = Assets::getShader("reflection");
		program = lightShader->GetProgram();
		glUseProgram(program);
		updateLightUniforms(lights, program);
		glUniform3fv(glGetUniformLocation(program, "cameraPos"), 1, (float*)&camera->GetPosition());
		
		lightShader = Assets::getShader("hellShader");
		program = lightShader->GetProgram();
		glUseProgram(program);
		updateLightUniforms(lights, program);
		glUniform3fv(glGetUniformLocation(program, "cameraPos"), 1, (float*)&camera->GetPosition());
	
		glUseProgram(0);
	}

	drawNodeList(scenes[currentScene]->getOpaqueList());
	glCullFace(GL_FRONT);
	drawNodeList(scenes[currentScene]->getFrontFaceCullList());
	glCullFace(GL_BACK);
	drawNodeList(scenes[currentScene]->getTransparentList());

	Matrix4 ortho = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	auto emitters = scenes[currentScene]->getEmitters();
	for (auto i = emitters.begin(); i != emitters.end(); ++i) {
		(*i)->drawParticles(viewMatrix, ortho, msec);
	}

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::renderWithShadows() {
	auto lights = scenes[currentScene]->getLights();

	if (lights.size() == 0) {
		RenderScene(); 
		return;
	}

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	scenes[currentScene]->clearNodeLists();
	scenes[currentScene]->buildNodeLists(scenes[currentScene]->getRoot(), frameFrustum, camera->GetPosition());
	scenes[currentScene]->sortNodeLists();

	drawShadows(lights);

	glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, sceneColourTexture[0], 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	if (scenes[currentScene]->getSkybox()) drawSkybox(scenes[currentScene]->getSkybox());
	drawCombined(lights, Assets::getShader("shadowSceneBump"));
}


void Renderer::drawShadows(vector<Light*>& lights) {

	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);


	glViewport(0, 0, SHADOW_SIZE, SHADOW_SIZE);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	Shader* s = Assets::getShader("shadow");
	GLuint program = s->GetProgram();

	glUseProgram(program);

	for (int i = 0; i < lights.size(); i++) {
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, lights[i]->GetShadowTexture(), 0);
		glClear(GL_DEPTH_BUFFER_BIT);

		Matrix4 shadowViewMatrix = Matrix4::BuildViewMatrix(lights[i]->GetPosition(), Vector3(0, 1, -10));

		glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, false, (float*)&modelMatrix);
		glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, false, (float*)&shadowViewMatrix);
		glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, false, (float*)&shadowProjMatrix);
		glUniformMatrix4fv(glGetUniformLocation(program, "textureMatrix"), 1, false, (float*)&textureMatrix);

		glCullFace(GL_FRONT);
		drawShadowMeshes(scenes[currentScene]->getOpaqueList(), program);
		drawShadowMeshes(scenes[currentScene]->getFrontFaceCullList(), program);
		drawShadowMeshes(scenes[currentScene]->getTransparentList(), program);
		glCullFace(GL_BACK);
	}
	glUseProgram(0);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glViewport(0, 0, width, height);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::drawCombined(vector<Light*>& lights, Shader* s) {

	GLuint program = s->GetProgram();

	glUseProgram(program);

	glUniform1i(glGetUniformLocation(program, "diffuseTex"), 0);

	glUniform1i(glGetUniformLocation(program, "bumpTex"), 1);

	glUniform3fv(glGetUniformLocation(program, "cameraPos"), 1, (float*)&camera->GetPosition());

	updateLightUniforms(lights, program);

	viewMatrix = camera->BuildViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, false, (float*)&projMatrix);

	drawShadowMeshes(scenes[currentScene]->getOpaqueList(), program);
	drawShadowMeshes(scenes[currentScene]->getFrontFaceCullList(), program);
	drawShadowMeshes(scenes[currentScene]->getTransparentList(), program);

	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::drawShadowMeshes(vector<SceneNode*>& nodes, GLuint program) {
	for (auto i = nodes.begin(); i != nodes.end(); ++i) {

		Matrix4 modelScale = Matrix4::Scale((*i)->getRenderObject()->getModelScale());
		Matrix4 modelTransform = (*i)->getWorldTransform();

		Matrix4 modelM = modelTransform * modelScale;

		glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, false, (float*)&modelM);

		glUniform1f(glGetUniformLocation(program, "msec"), msec);
		glUniform1f(glGetUniformLocation(program, "elapsed"), timeElapsed);
		glUniform1f(glGetUniformLocation(program, "shine"), (*i)->getRenderObject()->getShineFactor());
		glUniform1i(glGetUniformLocation(program, "diffuseTex"), 0);
		glUniform1i(glGetUniformLocation(program, "bumpTex"), 1);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, (*i)->getRenderObject()->getTexture());

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, (*i)->getRenderObject()->getBumpMap());

		(*i)->getRenderObject()->getMesh()->Draw();
	}
}

void Renderer::renderToScreen(int fps) {
	screenQuad->setShader("screenQuad");
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);

	Matrix4 screenMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	viewMatrix.ToIdentity();
	
	screenQuad->setTexture(sceneColourTexture[0]);
	screenQuad->draw(viewMatrix, screenMatrix, viewMatrix, textureMatrix, msec, timeElapsed);

	std::string text = "FPS: " + std::to_string(fps);
	gltSetText(fpsText, text.c_str());
	gltDrawText2D(fpsText, 2, 2, 3);

	glEnable(GL_DEPTH_TEST);
	SwapBuffers();
}

void Renderer::drawSkybox(RenderObject* ro) {
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	Matrix4 identity;
	identity.ToIdentity();
	ro->draw(viewMatrix, projMatrix, identity, textureMatrix, msec, timeElapsed);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

void Renderer::switchScene(int number) {
	if (number <= NUM_SCENES && number >= 0 && number != currentScene) {
		scenes[currentScene]->deactivate();

		currentScene = number;
		scenes[currentScene]->activate();
		setCameraAndEffects();
	}
}

void Renderer::initialiseScenes() {
	screenQuad->setMesh(Assets::getMesh("quad"));
	screenQuad->setShader("screenQuad");

	scenes[0] = Scene::initialiseSceneOne();
	scenes[1] = Scene::initialiseSceneTwo();
	scenes[2] = Scene::initialiseSceneThree();

	currentScene = 0;
	scenes[currentScene]->activate();
	setCameraAndEffects();
}

void Renderer::drawNodeList(vector<SceneNode*>& nodes) {
	for (auto i = nodes.begin(); i != nodes.end(); ++i) {
		(*i)->Draw(viewMatrix, projMatrix, textureMatrix, msec, timeElapsed);
	}
}

void Renderer::setCameraAndEffects() {
	if (currentScene == 0) {
		PostProcessing::setBloom(true);
		PostProcessing::setPaperEffect(false);
		PostProcessing::setToneMapping(false);

		shadows = false;

		camera->SetPosition(Vector3(-87.8f, 66.6f, -27.0f));
		camera->SetPitch(-17.3f);
		camera->SetYaw(281.0f);
	}
	else if (currentScene == 1) {
		PostProcessing::setBloom(false);
		PostProcessing::setPaperEffect(false);
		PostProcessing::setToneMapping(false);

		shadows = true;

		camera->SetPosition(Vector3(-8.7f, 9.1f, 5.0f));
		camera->SetPitch(-10.6f);
		camera->SetYaw(351.0f);
	}
	else if (currentScene == 2) {
		PostProcessing::setBloom(true);
		PostProcessing::setPaperEffect(true);
		PostProcessing::setToneMapping(true);
		shadows = false;

		camera->SetPosition(Vector3(13.7f, -6.3f, -30.0f));
		camera->SetPitch(14.0f);
		camera->SetYaw(12.0f);
	}
}

void Renderer::updateLightUniforms(vector<Light*> lights, GLuint program) {
	int num = lights.size();
	glUniform1i(glGetUniformLocation(program, "numLights"), num);

	for (int i = 0; i < num; i++) {
		Matrix4 shadowViewMatrix = Matrix4::BuildViewMatrix(lights[i]->GetPosition(), Vector3(0, 1, -10));

		Matrix4 shadowMatrix = biasMatrix * (shadowProjMatrix * shadowViewMatrix);

		glActiveTexture(GL_TEXTURE6 + i);
		glBindTexture(GL_TEXTURE_2D, lights[i]->GetShadowTexture());

		GLuint loc = glGetUniformLocation(program, ("allLights[" + to_string(i) + "].shadowTex").c_str());
		glUniform1i(loc, 6 + i);

		loc = glGetUniformLocation(program, ("allLights[" + to_string(i) + "].position").c_str());
		glUniform3fv(loc, 1, (float*)&lights[i]->GetPosition());

		loc = glGetUniformLocation(program, ("allLights[" + to_string(i) + "].colour").c_str());
		glUniform4fv(loc, 1, (float*)&lights[i]->GetColour());

		loc = glGetUniformLocation(program, ("allLights[" + to_string(i) + "].shadowMatrix").c_str());
		glUniformMatrix4fv(loc, 1, false, (float*)&shadowMatrix);

		loc = glGetUniformLocation(program, ("allLights[" + to_string(i) + "].radius").c_str());
		glUniform1f(loc, lights[i]->GetRadius());

		loc = glGetUniformLocation(program, ("allLights[" + to_string(i) + "].cutOffAngle").c_str());
		glUniform1f(loc, lights[i]->GetCutOffAngle());

		loc = glGetUniformLocation(program, ("allLights[" + to_string(i) + "].direction").c_str());
		glUniform3fv(loc, 1, (float*)&lights[i]->GetDirection());
	}
	glActiveTexture(GL_TEXTURE0);
}




