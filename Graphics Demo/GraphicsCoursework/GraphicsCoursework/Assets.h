#pragma once
#include <map>
#include "../nclgl/Mesh.h"
#include "../nclgl/MD5Anim.h"

#define TEXTURES "../../Textures/"
#define MESHES "../../Meshes/"
#define ANIMATIONS "../../Animations/"
#define VERTEX "../../Shaders/Vertex/"
#define FRAGMENT "../../Shaders/Fragment/"
#define GEOMETRY "../../Shaders/Geometry/"
#define TESSELATION "../../Shaders/Tesselation/"


class Assets {
public:
	static void initialise();
	static void clear();

	static Mesh* getMesh(std::string name);
	static GLuint getTexture(std::string name);
	static Shader* getShader(std::string name);

	static MD5FileData* getHellknightData() { return hellknightData; }

protected:
	static map<std::string, Mesh*> meshes;
	static map<std::string, GLuint> textures;
	static map<std::string, Shader*> shaders;

	static MD5FileData* hellknightData;

	static void loadMeshes();
	static void loadTextures();
	static void loadShaders();
	static void loadHellknight();

	static void repeatTexture(GLuint target);

	static void loadShader(std::string name, std::string vertex, std::string fragment);
	static void loadTexture(std::string name, std::string filepath, bool repeat);
	static void loadObjMesh(std::string name, std::string filepath);
};

