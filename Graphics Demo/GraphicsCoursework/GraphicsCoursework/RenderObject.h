#pragma once
#include "../nclgl/Matrix4.h"
#include "../nclgl/Mesh.h"

class Camera;

class RenderObject {
public:
	RenderObject();
	virtual ~RenderObject();


	Vector3 getModelScale() const { return modelScale; }
	void setModelScale(Vector3 s) { modelScale = s; }

	Mesh* getMesh() const { return mesh; }
	void setMesh(Mesh* m) { mesh = m; }

	std::string getShader() const { return shader; }
	void setShader(std::string s) { shader = s; }

	float getBoundingRadius() const { return boundingRadius; }
	void setBoundingRadius(float r) { boundingRadius = r; }

	float getShineFactor() const { return shineFactor; }
	void setShineFactor(float r) { shineFactor = r; }

	void setTexture(GLuint tex) { texture = tex; }
	GLuint getTexture() { return texture; }

	void setBumpMap(GLuint tex) { bumpMap = tex; }
	GLuint getBumpMap() const { return bumpMap; }

	void setTransparent(bool b) { transparent = b; }
	bool isTransparent() const { return transparent; }

	void setFrontCull(bool b) { frontCull = b; }
	bool isFrontCulled() const { return frontCull; }

	virtual void draw(Matrix4 viewMatrix, Matrix4 projectionMatrix, Matrix4 textureMatrix, 
		Matrix4 worldTransform, float msec, float timeElapsed);
	virtual void update(float msec);

protected:
	Mesh* mesh;
	std::string shader;

	Vector3 modelScale;

	GLuint texture;
	GLuint bumpMap;

	bool transparent;
	bool frontCull;
	float boundingRadius;
	float shineFactor;
};

