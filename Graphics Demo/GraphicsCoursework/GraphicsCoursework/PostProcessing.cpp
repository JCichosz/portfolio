#include "PostProcessing.h"
#include "Assets.h"

bool PostProcessing::bloom;
bool PostProcessing::paperEffect;
bool PostProcessing::toneMapping;

PostProcessing::PostProcessing(int w, int h): width(w), height(h) {
	bloom = false;
	paperEffect = false;
	toneMapping = false;

	glGenTextures(1, &brightnessTexture);
	glBindTexture(GL_TEXTURE_2D, brightnessTexture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glGenFramebuffers(1, &postProcessFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

PostProcessing::~PostProcessing() {
	glDeleteFramebuffers(1, &postProcessFBO);
}


void PostProcessing::addPostProcessing(GLuint& colourTexOne, GLuint& colourTexTwo, RenderObject* quad, float msec, float timeElapsed) {
	glBindFramebuffer(GL_FRAMEBUFFER, postProcessFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colourTexTwo, 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	Matrix4 projectionMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	Matrix4 identity;
	identity.ToIdentity();
	Matrix4 viewMatrix = identity;

	glDisable(GL_DEPTH_TEST);

	if (toneMapping) {
		Shader* s = Assets::getShader("toneMapping");
		quad->setShader("toneMapping");
		GLuint program = s->GetProgram();
		glUseProgram(program);
		updateMatrices(program, viewMatrix, projectionMatrix, Assets::getTexture("paper"), msec, timeElapsed);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colourTexTwo, 0);
		quad->setTexture(colourTexOne);
		quad->draw(viewMatrix, projectionMatrix, viewMatrix, identity, msec, timeElapsed);
		swapTextures(colourTexOne, colourTexTwo);
	}
	if (bloom) {
		Shader* s = Assets::getShader("drawBright");
		quad->setShader("drawBright");

		GLuint program = s->GetProgram();
		glUseProgram(program);
		updateMatrices(program, viewMatrix, projectionMatrix, 0, msec, timeElapsed);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, brightnessTexture, 0);
		quad->setTexture(colourTexOne);
		quad->draw(viewMatrix, projectionMatrix, viewMatrix, identity, msec, timeElapsed);

		s = Assets::getShader("bloom");
		quad->setShader("bloom");

		for (int i = 0; i < BLOOM_PASSES; i++) {
			program = s->GetProgram();
			glUseProgram(program);
			updateMatrices(program, viewMatrix, projectionMatrix, 0, msec, timeElapsed);

			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colourTexTwo, 0);
			quad->setTexture(brightnessTexture);
			quad->draw(viewMatrix, projectionMatrix, viewMatrix, identity, msec, timeElapsed);
			swapTextures(brightnessTexture, colourTexTwo);
		}
		s = Assets::getShader("bloomResult");
		quad->setShader("bloomResult");

		program = s->GetProgram();
		glUseProgram(program);
		updateMatrices(program, viewMatrix, projectionMatrix, brightnessTexture, msec, timeElapsed);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colourTexTwo, 0);
		quad->setTexture(colourTexOne);
		quad->draw(viewMatrix, projectionMatrix, viewMatrix, identity, msec, timeElapsed);
		swapTextures(colourTexOne, colourTexTwo);
	}

	if (paperEffect) {
		Shader* s = Assets::getShader("paper");
		
		quad->setShader("paper");
		GLuint program = s->GetProgram();
		glUseProgram(program);
		updateMatrices(program, viewMatrix, projectionMatrix, Assets::getTexture("paper"), msec, timeElapsed);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colourTexTwo, 0);
		quad->setTexture(colourTexOne);
		quad->draw(viewMatrix, projectionMatrix, viewMatrix, identity, msec, timeElapsed);
		swapTextures(colourTexOne, colourTexTwo);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);
	glEnable(GL_DEPTH_TEST);
}

void PostProcessing::updateMatrices(GLuint program, Matrix4& viewMatrix, Matrix4& projMatrix, GLuint effect, float msec, float timeElapsed) {
	glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, false, (float*)&projMatrix);
	glUniform2f(glGetUniformLocation(program, "pixelSize"), 1.0f / width, 1.0f / height);
	glUniform1i(glGetUniformLocation(program, "effectTex"), 10);
	glUniform1f(glGetUniformLocation(program, "msec"), msec);
	glUniform1f(glGetUniformLocation(program, "elapsed"), timeElapsed);

	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_2D, effect);

	glActiveTexture(GL_TEXTURE0);
}

void PostProcessing::swapTextures(GLuint& colourTexOne, GLuint& colourTexTwo) {
	GLuint temp = colourTexOne;
	colourTexOne = colourTexTwo;
	colourTexTwo = temp;
}
