#pragma once
#include "RenderObject.h"

class CubeMap: public RenderObject {
public:
	CubeMap();
	~CubeMap();

	virtual void draw(Matrix4 viewMatrix, Matrix4 projectionMatrix, Matrix4 textureMatrix, Matrix4 worldTransform, float msec, float timeElapsed) override;

};

