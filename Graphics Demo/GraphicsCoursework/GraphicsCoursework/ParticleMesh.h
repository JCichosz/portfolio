#pragma once
#include "Particle.h"
#include "GL/glew.h"
#include "GL/wglew.h"

class ParticleMesh {
public:
	ParticleMesh(int num);
	~ParticleMesh();

	void draw();

	Particle& operator[](int index) { return data[index]; }

protected:
	Particle* data;
	int numParticles;

	GLuint arrayObject;
	GLuint bufferObject;
};

