#include "Scene.h"
#include "Assets.h"
#include <algorithm>
#include "../nclgl/Frustum.h"
#include "CubeMap.h"
#include "../nclgl/MD5Node.h"

Matrix4 Scene::treePos;

Scene::Scene() {
	root = new SceneNode();
	Matrix4 pos;
	pos.ToIdentity();
	root->setTransform(pos);
}

Scene::~Scene() {
	lights.clear();
}

void Scene::update(float msec, float timeElapsed, int current, float pitch, float yaw) {
	for (ParticleEmitter* p : emitters) {
		p->updateParticles(msec);
	}
	if (current == 0) {
		lights[0]->SetPosition(lights[0]->GetPosition() + Vector3(sin(timeElapsed), 0, 0));
	}
	if (current == 1) {
		SceneNode* dragon = nodes.at("dragon");
		dragon->setTransform(dragon->getTransform() * Matrix4::Rotation(msec * 0.05f, Vector3(0, 1, 0)));

		SceneNode* firebreath = nodes.at("firebreath");
		firebreath->setTransform(firebreath->getTransform() * Matrix4::Rotation(msec * 0.5f, Vector3(1, -1, 0)));
	
		for (auto l: lights) {
			l->SetPosition(l->GetPosition() + (Vector3(0, sin(timeElapsed) * 0.5f, 0) * 0.1f));
		}
	}
	if (current == 2) {
		SceneNode* tree = nodes.at("tree");
		tree->setTransform(treePos *  Matrix4::Rotation(yaw, Vector3(0, 1, 0)));
	}

	root->Update(msec);
}

Scene* Scene::initialiseSceneOne() {
	Scene* sceneOne = new Scene();

	RenderObject* beach = new RenderObject();
	beach->setMesh(Assets::getMesh("ammyTerrain"));
	beach->setModelScale(Vector3(1.5f, 0.5f, 1.5f));
	beach->setTexture(Assets::getTexture("sand"));
	beach->setBumpMap(Assets::getTexture("flatNormals"));
	beach->setShader("bump");
	SceneNode* beachNode = new SceneNode();
	beachNode->setTransform(Matrix4::Translation(Vector3(-600, -60, -900)));
	beachNode->setRenderObject(beach);
	sceneOne->getRoot()->AddChild(beachNode);

	RenderObject* water = new RenderObject();
	water->setMesh(Assets::getMesh("beachTerrain"));
	water->setModelScale(Vector3(1.5, 0.5, 1.5));
	water->setTexture(Assets::getTexture("water"));
	water->setShader("reflection");
	water->setShineFactor(0.8f);
	SceneNode* waterNode = new SceneNode();
	waterNode->setTransform(Matrix4::Translation(Vector3(-600, -20, -500)) * Matrix4::Scale(Vector3(1.0f, 0.1f, 1.0f)));
	waterNode->setRenderObject(water);
	sceneOne->getRoot()->AddChild(waterNode);

	for (int i = 0; i < 40; i++) {
		RenderObject* pedestal = new RenderObject();
		pedestal->setMesh(Assets::getMesh("sphere"));
		pedestal->setModelScale(Vector3(8.0f, 8.0f, 8.0f));
		pedestal->setTexture(Assets::getTexture("brick"));
		pedestal->setBumpMap(Assets::getTexture("brickNormals"));
		pedestal->setShader("bump");
		pedestal->setBoundingRadius(8.0f);
		float shine = (float)rand() / RAND_MAX + 0.1f;
		pedestal->setShineFactor(shine);

		SceneNode* pedestalNode = new SceneNode();

		Vector3 transform = Vector3((float)rand() / RAND_MAX * 200.0f, 3.0f, -(float)rand() / RAND_MAX * 200.0f);
		float rot = (float)rand() / RAND_MAX * 360.0f;
		pedestalNode->setTransform(Matrix4::Translation(transform) * Matrix4::Rotation(rot, Vector3(0,1,0)));
		pedestalNode->setRenderObject(pedestal);
		sceneOne->getRoot()->AddChild(pedestalNode);

		MD5Node* hellknightNode = new MD5Node(*(Assets::getHellknightData()));
		hellknightNode->getRenderObject()->setBoundingRadius(10.0f);

		int anim = (float)rand() / RAND_MAX * 50.0f;
		int mod = anim % 3;

		switch (mod) {
			case 0: hellknightNode->PlayAnim(ANIMATIONS"attack2.md5anim"); break;
			case 1: hellknightNode->PlayAnim(ANIMATIONS"idle2.md5anim"); break;
			case 2: hellknightNode->PlayAnim(ANIMATIONS"walk7.md5anim"); break;
		}
		hellknightNode->setTransform(Matrix4::Translation(Vector3(0, 7, 0))*Matrix4::Scale(Vector3(0.1f, 0.1f, 0.1f)));
		pedestalNode->AddChild(hellknightNode);
	}

	Light* light1 = new Light(Vector3(-43, 171, -230), Vector4(1, 1, 1, 1), 10000.0f);
	sceneOne->addLight(light1);

	CubeMap* skybox = new CubeMap();
	skybox->setMesh(Assets::getMesh("quad"));
	skybox->setTexture(Assets::getTexture("defaultSkybox"));
	skybox->setShader("skybox");
	sceneOne->setSkybox(skybox);

	return sceneOne;
}

Scene* Scene::initialiseSceneTwo() {
	Scene* sceneTwo = new Scene();

	RenderObject* dragon = new RenderObject();
	dragon->setMesh(Assets::getMesh("dragon"));
	dragon->setModelScale(Vector3(1.0f, 1.0f, 1.0f));
	dragon->setTexture(Assets::getTexture("metal"));
	dragon->setBumpMap(Assets::getTexture("dragonNormals"));
	dragon->setShader("shadowSceneBump");
	dragon->setShineFactor(0.8f);
	dragon->setBoundingRadius(10.0f);

	SceneNode* dragonNode = new SceneNode();
	dragonNode->setTransform(Matrix4::Translation(Vector3(0, 0, -20)));
	dragonNode->setRenderObject(dragon);
	sceneTwo->addSceneNode("dragon", dragonNode);
	sceneTwo->getRoot()->AddChild(dragonNode);

	RenderObject* fireBreath = new RenderObject();
	fireBreath->setMesh(Assets::getMesh("sphere"));
	fireBreath->setModelScale(Vector3(0.5f, 0.5f, 0.5f));
	fireBreath->setTexture(Assets::getTexture("fire"));
	fireBreath->setBumpMap(Assets::getTexture("flatNormals"));
	fireBreath->setShader("textureMove");
	fireBreath->setShineFactor(0.5f);

	SceneNode* fireNode = new SceneNode();
	fireNode->setTransform(Matrix4::Translation(Vector3(0, 2, 8)));
	fireNode->setRenderObject(fireBreath);
	sceneTwo->getSceneNode("dragon")->AddChild(fireNode);
	sceneTwo->addSceneNode("firebreath", fireNode);


	RenderObject* ground = new RenderObject();
	ground->setMesh(Assets::getMesh("quad"));
	ground->setModelScale(Vector3(100.0f, 100.0f, 100.0f));
	ground->setTexture(Assets::getTexture("rockyGrass"));
	ground->setBumpMap(Assets::getTexture("flatNormals"));
	ground->setShader("shadowSceneBump");
	ground->setShineFactor(0.1f);
	SceneNode* groundNode = new SceneNode();
	groundNode->setTransform(Matrix4::Translation(Vector3(0, -2, -15)) * Matrix4::Rotation(-90, Vector3(1, 0, 0)));
	groundNode->setRenderObject(ground);
	sceneTwo->getRoot()->AddChild(groundNode);

	RenderObject* wall = new RenderObject();
	wall->setMesh(Assets::getMesh("quad"));
	wall->setModelScale(Vector3(30.0f, 30.0f, 30.0f));
	wall->setTexture(Assets::getTexture("brick"));
	wall->setBumpMap(Assets::getTexture("brickNormals"));
	wall->setShader("shadowSceneBump");
	wall->setShineFactor(0.1f);
	SceneNode* wallNode = new SceneNode();
	wallNode->setTransform(Matrix4::Translation(Vector3(0, 2, -60)) * Matrix4::Rotation(-25.0f, Vector3(1,0,0)));
	wallNode->setRenderObject(wall);
	sceneTwo->getRoot()->AddChild(wallNode);


	Light* light1 = new Light(Vector3(10,7, 5), Vector4(1, 1, 1, 1), 100.0f);
	Light* light2 = new Light(Vector3(-10,7, 5), Vector4(1, 1, 1, 1),100.0f);

	sceneTwo->addLight(light1);
	sceneTwo->addLight(light2);

	CubeMap* skybox = new CubeMap();
	skybox->setMesh(Assets::getMesh("quad"));
	skybox->setTexture(Assets::getTexture("defaultSkybox"));
	skybox->setShader("skybox");
	sceneTwo->setSkybox(skybox);

	return sceneTwo;
}

Scene* Scene::initialiseSceneThree() {
	Scene* sceneThree = new Scene();

	RenderObject* amaterasu = new RenderObject();
	amaterasu->setMesh(Assets::getMesh("amaterasu"));
	amaterasu->setModelScale(Vector3(1.0f, 1.0f, 1.0f));
	amaterasu->setTexture(Assets::getTexture("ammyColour"));
	amaterasu->setBumpMap(Assets::getTexture("ammyNormals"));
	amaterasu->setShader("basic");

	SceneNode* ammyNode = new SceneNode();
	ammyNode->setTransform(Matrix4::Translation(Vector3(15, -15, -100)) *Matrix4::Rotation(-120.0f, Vector3(0, 1, 0)));
	ammyNode->setRenderObject(amaterasu);
	sceneThree->getRoot()->AddChild(ammyNode);

	RenderObject* terrain = new RenderObject();
	terrain->setMesh(Assets::getMesh("ammyTerrain"));
	terrain->setModelScale(Vector3(0.5f, 0.5f, 0.5f));
	terrain->setTexture(Assets::getTexture("okamiGrass"));
	terrain->setShader("basic");
	SceneNode* terrainNode = new SceneNode();
	terrainNode->setTransform(Matrix4::Translation(Vector3(-1200, -30, -1700)));
	terrainNode->setRenderObject(terrain);
	sceneThree->getRoot()->AddChild(terrainNode);

	RenderObject* terrainTwo = new RenderObject();
	terrainTwo->setMesh(Assets::getMesh("ammyTerrain"));
	terrainTwo->setModelScale(Vector3(0.5f, 0.5f, 0.5f));
	terrainTwo->setTexture(Assets::getTexture("black"));
	terrainTwo->setShader("basic");
	terrainTwo->setFrontCull(true);
	SceneNode* terrainTwoNode = new SceneNode();
	terrainTwoNode->setTransform(Matrix4::Translation(Vector3(-1200, -29, -1700)));
	terrainTwoNode->setRenderObject(terrainTwo);
	sceneThree->getRoot()->AddChild(terrainTwoNode);

	RenderObject* guardianSapling = new RenderObject();
	guardianSapling->setMesh(Assets::getMesh("quad"));
	guardianSapling->setModelScale(Vector3(100.0f, 100.0f, 100.0f));
	guardianSapling->setTexture(Assets::getTexture("guardianSapling"));
	guardianSapling->setShader("basic");
	guardianSapling->setTransparent(true);
	SceneNode* guardianSaplingNode = new SceneNode();

	treePos = Matrix4::Translation(Vector3(-50, 55, -300));

	guardianSaplingNode->setTransform(Matrix4::Translation(Vector3(-50, 55, -300)));
	guardianSaplingNode->setRenderObject(guardianSapling);
	sceneThree->getRoot()->AddChild(guardianSaplingNode);
	sceneThree->addSceneNode("tree", guardianSaplingNode);

	CubeMap* skybox = new CubeMap();
	skybox->setMesh(Assets::getMesh("quad"));
	skybox->setTexture(Assets::getTexture("okamiSkybox"));
	skybox->setBumpMap(Assets::getTexture("okamiNightSkybox"));
	skybox->setShader("skyboxBlend");
	sceneThree->setSkybox(skybox);

	ParticleEmitter* cherryPetals = new ParticleEmitter(300, "petal");
	sceneThree->addEmitter(cherryPetals);

	return sceneThree;
}


void Scene::buildNodeLists(SceneNode* from, Frustum& frameFrustum, Vector3 cameraPosition) {
	if (from->getRenderObject() && frameFrustum.InsideFrustum(*from) && from->isActive()) {
		Vector3 dir = from->getWorldTransform().GetPositionVector() - cameraPosition;
		from->SetCameraDistance(Vector3::Dot(dir, dir));

		if (from->getRenderObject()->isTransparent()) transparent.push_back(from);
		else if(from->getRenderObject()->isFrontCulled()) frontFaceCull.push_back(from);
		else opaque.push_back(from);
	}

	for (auto i = from->GetChildIteratorStart(); i != from->GetChildIteratorEnd(); ++i) {
		buildNodeLists((*i), frameFrustum, cameraPosition);
	}
}

void Scene::sortNodeLists() {
	std::sort(transparent.begin(), transparent.end(), SceneNode::CompareByCameraDistance);
	std::sort(opaque.begin(), opaque.end(), SceneNode::CompareByCameraDistance);
}

void Scene::clearNodeLists() {
	transparent.clear();
	opaque.clear();
	frontFaceCull.clear();
}
