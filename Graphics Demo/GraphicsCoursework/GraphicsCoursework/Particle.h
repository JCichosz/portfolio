#pragma once
#include "../nclgl/Vector3.h"
#include "../nclgl/Vector4.h"
#include <string>

class Particle {
public:
	Particle(Vector3 initialPos, Vector3 dir, float rot, float spd, float size, float lifetime, Vector4 col = Vector4(1,1,1,1));
	~Particle();

	void update(float msec);

	Vector4 getColour() const { return colour; }
	void setColour(Vector4 col) { colour = col; }

	Vector3 getPosition() const { return position; }
	void setPosition(Vector3 pos) { position = pos; }

	float getRotation() const { return rotation; }
	void setRotation(float r) { rotation = r; }

	float getSize() const { return size; }
	void setSize(float s) { size = s; }

	Vector3 initialPosition;
	Vector3 position;
	Vector4 colour;
	Vector3 directionOfMovement;
	float speed;
	float rotation;
	float size;
	float lifetime;
	float age;
};

