#pragma once
#include "Vector4.h"
#include "Vector3.h"

const int SHADOW_SIZE = 2048;

class Light {
public:

	Light() {
		position = Vector3(0.0f, 0.0f, 0.0f);
		colour = Vector4(0.0, 0.0, 0.0, 1.0f);
		radius = 1.0f;

		glGenTextures(1, &shadowTexture);
		glBindTexture(GL_TEXTURE_2D, shadowTexture);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_SIZE, SHADOW_SIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	Light(Vector3 pos, Vector4 col, float r, Vector3 dir = Vector3(0,0,0), float cutOff = 360.0f) {
		dir.Normalise();
		position = pos;
		colour = col;
		radius = r;
		direction = dir;
		cutOffAngle = cutOff;

		glGenTextures(1, &shadowTexture);
		glBindTexture(GL_TEXTURE_2D, shadowTexture);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_SIZE, SHADOW_SIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glBindTexture(GL_TEXTURE_2D, 0);
	}


	~Light() {
		glDeleteTextures(1, &shadowTexture);
	}

	Vector3 GetPosition() const { return position; }
	void SetPosition(Vector3 pos) { position = pos; }

	Vector3 GetDirection() const { return direction; }
	void SetDirection(Vector3 dir) { direction = dir; }

	float GetRadius() const { return radius; }
	void SetRadius(float r) { radius = r; }

	float GetCutOffAngle() const { return cutOffAngle; }
	void SetCutOffAngle(float a) { cutOffAngle = a; }

	Vector4 GetColour() const { return colour; }
	void SetColour(Vector4 col) { colour = col; }

	GLuint GetShadowTexture() const { return shadowTexture; }

protected:
	Vector3 position;
	Vector4 colour;
	float radius;
	float cutOffAngle;
	Vector3 direction;
	GLuint shadowTexture;
};
