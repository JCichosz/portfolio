#pragma once

#include "Matrix4.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Mesh.h"
#include <vector>
#include "..\GraphicsCoursework\RenderObject.h"

class SceneNode {
public:
	SceneNode();
	~SceneNode();

	virtual void Update(float msec);
	virtual void Draw(Matrix4 viewMatrix, Matrix4 projectionMatrix, Matrix4 textureMatrix, float msec, float timeElapsed);

	bool isActive() { return active; }

	void activate() { activateChildren(); }
	void deactivate() { deactivateChildren(); }

	void AddChild(SceneNode* node);

	void setRenderObject(RenderObject* ro) { renderObject = ro; }
	RenderObject* getRenderObject() { return renderObject; }

	float GetCameraDistance() { return distanceFromCamera; }
	void SetCameraDistance(float d) { distanceFromCamera = d; }

	static bool CompareByCameraDistance(SceneNode* a, SceneNode* b) {
		return (a->distanceFromCamera < b->distanceFromCamera) ? true : false;
	}

	void setTransform(const Matrix4& matrix) { transform = matrix; }
	const Matrix4& getTransform()	const { return transform; }

	Matrix4 getWorldTransform() const { return worldTransform; }
	void setWorldTransform(const Matrix4& matrix) { worldTransform = matrix; }

	std::vector<SceneNode*>::const_iterator GetChildIteratorStart() { return children.begin(); }
	std::vector<SceneNode*>::const_iterator GetChildIteratorEnd() { return children.end(); }

protected:
	SceneNode* parent;
	RenderObject* renderObject;
	float distanceFromCamera;
	bool active;

	Matrix4 worldTransform;
	Matrix4 transform;

	std::vector<SceneNode*> children;
	void deactivateChildren();
	void activateChildren();
};
