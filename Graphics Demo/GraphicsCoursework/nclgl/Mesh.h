#pragma once
#include "OGLRenderer.h"
#include <vector>

enum MeshBuffer {
	VERTEX_BUFFER, COLOUR_BUFFER, TEXTURE_BUFFER, 
	NORMAL_BUFFER, TANGENT_BUFFER, INDEX_BUFFER, MAX_BUFFER
};

class Mesh {
public:
	Mesh();
	virtual ~Mesh();

	virtual void Draw();

	static Mesh* GenerateTriangle();
	static Mesh* GenerateQuad();
	static Mesh* GeneratePatchesQuad();
	static Mesh* GeneratePoints(int number);

	void changeVertices(std::vector<Vector3>& verts);
	void changeColours(std::vector<Vector4>& colours);

	void rebuffer() { RebufferData(); }

protected:
	virtual void RebufferData();
	void BufferData();
	void GenerateNormals();
	void GenerateTangents();
	Vector3 GenerateTangent(const Vector3& a, const Vector3& b, const Vector3& c,
		const Vector2& ta, const Vector2& tb, const Vector2& tc);

	GLuint arrayObject;
	GLuint bufferObject[MAX_BUFFER];
	GLuint numVertices;
	GLuint type;

	Vector3* vertices;
	Vector4* colours;
	Vector2* textureCoords;
	Vector3* normals;
	Vector3* tangents;

	GLuint numIndices;
	unsigned int* indices;
};