#include "SceneNode.h"

SceneNode::SceneNode() {	
	parent = nullptr;
	distanceFromCamera = 0.0f;
	Matrix4 identity;
	identity.ToIdentity();
	transform = identity;
}

SceneNode::~SceneNode() {
	for (unsigned int i = 0; i < children.size(); ++i) {
		delete children[i];
	}
}

void SceneNode::AddChild(SceneNode* node) {
	children.push_back(node);
	node->parent = this;
}

void SceneNode::deactivateChildren() {
	active = false;
	for (SceneNode* node : children) {
		node->deactivateChildren();
	}
}

void SceneNode::activateChildren() {
	active = true;
	for (SceneNode* node : children) {
		node->activateChildren();
	}
}

void SceneNode::Draw(Matrix4 viewMatrix, Matrix4 projectionMatrix, Matrix4 textureMatrix, float msec, float timeElapsed) {
	if (renderObject && renderObject->getMesh()) renderObject->draw(viewMatrix, projectionMatrix, textureMatrix, worldTransform, msec, timeElapsed);
}

void SceneNode::Update(float msec) {
	if (parent) {
		Matrix4 newTransform = parent->worldTransform * transform;
		setWorldTransform(newTransform);
	}
	else  setWorldTransform(transform);

	for (vector<SceneNode*>::iterator i = children.begin(); i != children.end(); ++i) {
		(*i)->Update(msec);
	}
}