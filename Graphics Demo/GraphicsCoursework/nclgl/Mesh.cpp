#include "Mesh.h"


Mesh::Mesh() {
	for (int i = 0; i < MAX_BUFFER; ++i) {
		bufferObject[i] = 0;
	}
	glGenVertexArrays(1, &arrayObject);

	numVertices = 0;
	vertices = nullptr;
	colours = nullptr;
	type = GL_TRIANGLES;

	textureCoords = nullptr;
	normals = nullptr;
	tangents = nullptr;
	indices = nullptr;

	numIndices = 0;
}

Mesh::~Mesh() {
	glDeleteVertexArrays(1, &arrayObject);

	delete[] vertices;
	delete[] colours;
	delete[] textureCoords;
	delete[] indices;
	delete[] normals;
	delete[] tangents;
}

Mesh* Mesh::GenerateTriangle() {
	Mesh* m = new Mesh();
	m->numVertices = 3;
	m->vertices = new Vector3[m->numVertices];

	m->vertices[0] = Vector3(0.0f, 0.5f, 0.0f);
	m->vertices[1] = Vector3(0.5f, -0.5f, 0.0f);
	m->vertices[2] = Vector3(-0.5f, -0.5f, 0.0f);

	m->textureCoords = new Vector2[m->numVertices];
	m->textureCoords[0] = Vector2(0.5f, 0.0f);
	m->textureCoords[1] = Vector2(1.0f, 1.0f);
	m->textureCoords[2] = Vector2(0.0f,  1.0f);

	m->colours = new Vector4[m->numVertices];
	m->colours[0] = Vector4(1.0f, 0.0f, 0.0f, 1.0f);
	m->colours[1] = Vector4(0.0f, 1.0f, 0.0f, 1.0f);
	m->colours[2] = Vector4(0.0f, 0.0f, 1.0f, 1.0f);

	m->BufferData();
	return m;
}

Mesh * Mesh::GenerateQuad() {
	Mesh* quad = new Mesh();
	quad->numVertices = 4;
	quad->type = GL_TRIANGLES;

	quad->vertices = new Vector3[quad->numVertices];
	quad->textureCoords = new Vector2[quad->numVertices];
	quad->colours = new Vector4[quad->numVertices];
	quad->normals = new Vector3[quad->numVertices];
	quad->tangents = new Vector3[quad->numVertices];
	quad->numIndices = 6;
	quad->indices = new unsigned int[quad->numIndices];

	quad->indices[0] = 0;
	quad->indices[1] = 2;
	quad->indices[2] = 1;
	quad->indices[3] = 2;
	quad->indices[4] = 3;
	quad->indices[5] = 1;

	quad->vertices[0] = Vector3(-1.0f, 1.0f, 0.0f);
	quad->vertices[1] = Vector3(1.0f, 1.0f, 0.0f);
	quad->vertices[2] = Vector3(-1.0f, -1.0f, 0.0f);
	quad->vertices[3] = Vector3(1.0f, -1.0f, 0.0f);	

	quad->textureCoords[0] = Vector2(0.0f, 1.0f);
	quad->textureCoords[1] = Vector2(1.0f, 1.0f);
	quad->textureCoords[2] = Vector2(0.0f, 0.0f);
	quad->textureCoords[3] = Vector2(1.0f, 0.0f);

	for (int i = 0; i < 4; ++i) {
		quad->colours[i] = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
		quad->normals[i] = Vector3(0.0f, 0.0f, -1.0f);
		quad->tangents[i] = Vector3(1.0f, 0.0f, 0.0f);
	}
	quad->GenerateNormals();
	quad->BufferData();
	return quad;
}

Mesh * Mesh::GeneratePatchesQuad() {
	Mesh* quad = new Mesh();
	quad->numVertices = 4;
	quad->type = GL_PATCHES;

	quad->vertices = new Vector3[quad->numVertices];
	quad->textureCoords = new Vector2[quad->numVertices];
	quad->colours = new Vector4[quad->numVertices];
	quad->normals = new Vector3[quad->numVertices];
	quad->tangents = new Vector3[quad->numVertices];
	quad->numIndices = 6;
	quad->indices = new unsigned int[quad->numIndices];

	quad->indices[0] = 0;
	quad->indices[1] = 2;
	quad->indices[2] = 1;
	quad->indices[3] = 2;
	quad->indices[4] = 3;
	quad->indices[5] = 1;

	quad->vertices[0] = Vector3(-1.0f, 1.0f, 0.0f);
	quad->vertices[1] = Vector3(1.0f, 1.0f, 0.0f);
	quad->vertices[2] = Vector3(-1.0f, -1.0f, 0.0f);
	quad->vertices[3] = Vector3(1.0f, -1.0f, 0.0f);

	quad->textureCoords[0] = Vector2(0.0f, 1.0f);
	quad->textureCoords[1] = Vector2(1.0f, 1.0f);
	quad->textureCoords[2] = Vector2(0.0f, 0.0f);
	quad->textureCoords[3] = Vector2(1.0f, 0.0f);

	for (int i = 0; i < 4; ++i) {
		quad->colours[i] = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
		quad->normals[i] = Vector3(0.0f, 0.0f, -1.0f);
		quad->tangents[i] = Vector3(1.0f, 0.0f, 0.0f);
	}
	quad->GenerateNormals();
	quad->BufferData();
	return quad;
}

Mesh* Mesh::GeneratePoints(int number) {
	Mesh* m = new Mesh();
	m->numVertices = number;
	m->type = GL_POINTS;

	m->vertices = new Vector3[m->numVertices];
	m->colours = new Vector4[m->numVertices];

	for (int i = 0; i < number; ++i) {
		m->vertices[i] = Vector3(0.0f, 0.0f, 0.0f);
		m->colours[i] = Vector4(1,1,1,1);
	}
	m->BufferData();
	return m;
}

void Mesh::changeVertices(vector<Vector3>& verts) {
	for (int i = 0; i < numVertices; ++i) {
		vertices[i] = verts[i];
	}
}

void Mesh::changeColours(vector<Vector4>& col) {
	for (int i = 0; i < numVertices; ++i) {
		colours[i] = col[i];
	}
}


void Mesh::GenerateNormals() {
	if (!normals) normals = new Vector3[numVertices];

	for (GLuint i = 0; i < numVertices; ++i) {
		normals[i] = Vector3();
	}

	if (indices) { // per-vertex normals
		for (GLuint i = 0; i < numIndices; i += 3) {
			unsigned int a = indices[i];
			unsigned int b = indices[i+1];
			unsigned int c = indices[i+2];

			Vector3 normal = Vector3::Cross((vertices[b] - vertices[a]), (vertices[c] - vertices[a]));

			normals[a] += normal;
			normals[b] += normal;
			normals[c] += normal;
		}
	}
	else { // per-face normals
		for (GLuint i = 0; i < numVertices; i += 3) {
			Vector3& a = vertices[i];
			Vector3& b = vertices[i+1];
			Vector3& c = vertices[i+2];

			Vector3 normal = Vector3::Cross(b - a, c - a);

			normals[i] = normal;
			normals[i+1] = normal;
			normals[i+2] = normal;
		}
	}

	for (GLuint i = 0; i < numVertices; ++i) {
		normals[i].Normalise();
	}

}

void Mesh::GenerateTangents() {
	if (!tangents) tangents = new Vector3[numVertices];

	if (!textureCoords) return;

	for (GLuint i = 0; i < numVertices; ++i) {
		tangents[i] = Vector3();
	}

	if (indices) {
		for (GLuint i = 0; i < numIndices; i += 3) {
			int a = indices[i];
			int b = indices[i + 1];
			int c = indices[i + 2];

			Vector3 tangent = GenerateTangent(vertices[a], vertices[b],
				vertices[c], textureCoords[a],
				textureCoords[b], textureCoords[c]);

			tangents[a] += tangent;
			tangents[b] += tangent;
			tangents[c] += tangent;
		}
	}
	else {
		for (GLuint i = 0; i < numVertices; i += 3) {
			Vector3 tangent = GenerateTangent(vertices[i], vertices[i + 1],
				vertices[i + 2], textureCoords[i],
				textureCoords[i + 1], textureCoords[i + 2]);

			tangents[i] += tangent;
			tangents[i + 1] += tangent;
			tangents[i + 2] += tangent;
		}
	}

	for (GLuint i = 0; i < numVertices; ++i) {
		tangents[i].Normalise();
	}

}

Vector3 Mesh::GenerateTangent(const Vector3 & a, const Vector3 & b, const Vector3 & c, const Vector2 & ta, const Vector2 & tb, const Vector2 & tc) {
	Vector2 coord1 = tb - ta;
	Vector2 coord2 = tc - ta;

	Vector3 vertex1 = b - a;
	Vector3 vertex2 = c - a;

	Vector3 axis = Vector3(vertex1 * coord2.y - vertex2 * coord1.y);

	float factor = 1.0f / (coord1.x * coord2.y - coord2.x * coord1.y);

	return axis * factor;
}


void Mesh::RebufferData() {
	glBindBuffer(GL_ARRAY_BUFFER, bufferObject[VERTEX_BUFFER]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vector3), (void*)vertices);

	if (textureCoords) {
		glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TEXTURE_BUFFER]);
		glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vector2), (void*)textureCoords);
	}

	if (colours) {
		glBindBuffer(GL_ARRAY_BUFFER, bufferObject[COLOUR_BUFFER]);
		glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vector4), (void*)colours);
	}

	if (normals) {
		glBindBuffer(GL_ARRAY_BUFFER, bufferObject[NORMAL_BUFFER]);
		glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vector3), (void*)normals);
	}

	if (tangents) {
		glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TANGENT_BUFFER]);
		glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vector3), (void*)tangents);
	}

	if (indices) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject[INDEX_BUFFER]);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numVertices * sizeof(unsigned int), (void*)indices);
	}
}

void Mesh::BufferData() {
	glBindVertexArray(arrayObject);

	glGenBuffers(1, &bufferObject[VERTEX_BUFFER]);
	glBindBuffer(GL_ARRAY_BUFFER, bufferObject[VERTEX_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(Vector3), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VERTEX_BUFFER, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(VERTEX_BUFFER);


	if (textureCoords) {
		glGenBuffers(1, &bufferObject[TEXTURE_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TEXTURE_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(Vector2), textureCoords, GL_STATIC_DRAW);
		glVertexAttribPointer(TEXTURE_BUFFER, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(TEXTURE_BUFFER);
	}

	if (colours) {
		glGenBuffers(1, &bufferObject[COLOUR_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObject[COLOUR_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(Vector4), colours, GL_STATIC_DRAW);
		glVertexAttribPointer(COLOUR_BUFFER, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(COLOUR_BUFFER);
	}

	if (indices) {
		glGenBuffers(1, &bufferObject[INDEX_BUFFER]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject[INDEX_BUFFER]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(GLuint), indices, GL_STATIC_DRAW);
	}

	if (normals) {
		glGenBuffers(1, &bufferObject[NORMAL_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObject[NORMAL_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(Vector3), normals, GL_STATIC_DRAW);
		glVertexAttribPointer(NORMAL_BUFFER, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(NORMAL_BUFFER);
	}
	if (tangents) {
		glGenBuffers(1, &bufferObject[TANGENT_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TANGENT_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(Vector3), tangents, GL_STATIC_DRAW);
		glVertexAttribPointer(TANGENT_BUFFER, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(TANGENT_BUFFER);
	}

	glBindVertexArray(0);
}

void Mesh::Draw() {
	glBindVertexArray(arrayObject);
	if (bufferObject[INDEX_BUFFER]) glDrawElements(type, numIndices, GL_UNSIGNED_INT, 0);
	else glDrawArrays(type, 0, numVertices);

	glBindVertexArray(0);
}

