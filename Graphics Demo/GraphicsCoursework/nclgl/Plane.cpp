#include "Plane.h"

Plane::Plane(const Vector3& n, float d, bool normalise) {
	if (normalise) {
		float length = sqrt(Vector3::Dot(n, n));

		normal = n / length;
		distance = d / length;
	}
	else {
		normal = n;
		distance = d;
	}
}

bool Plane::SphereInPlane(const Vector3 & position, float radius) const {
	if (Vector3::Dot(position, normal) + distance <= -radius) return false;
	else return true;
}
