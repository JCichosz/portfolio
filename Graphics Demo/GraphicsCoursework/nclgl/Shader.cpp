#include "Shader.h"

Shader::Shader(string vFile, string fFile, string gFile, string tcsFile, string tesFile) {
	program = glCreateProgram();
	objects[SHADER_VERTEX] = GenerateShader(vFile, GL_VERTEX_SHADER);
	objects[SHADER_FRAGMENT] = GenerateShader(fFile, GL_FRAGMENT_SHADER);
	objects[SHADER_GEOMETRY] = 0;
	objects[SHADER_TCS] = 0;
	objects[SHADER_TES] = 0;

	if (!gFile.empty()) {
		objects[SHADER_GEOMETRY] = GenerateShader(gFile, GL_GEOMETRY_SHADER);
		glAttachShader(program, objects[SHADER_GEOMETRY]);
	}
	if (!tcsFile.empty()) {
		objects[SHADER_TCS] = GenerateShader(tcsFile, GL_TESS_CONTROL_SHADER);
		glAttachShader(program, objects[SHADER_TCS]);
	}
	if (!tesFile.empty()) {
		objects[SHADER_TES] = GenerateShader(tesFile, GL_TESS_EVALUATION_SHADER);
		glAttachShader(program, objects[SHADER_TES]);
	}
	glAttachShader(program, objects[SHADER_VERTEX]);
	glAttachShader(program, objects[SHADER_FRAGMENT]);
	SetDefaultAttributes();
}

Shader::~Shader() {
	if (program) {
		for (int i = 0; i < SHADER_MAX; ++i) {
			if (objects[i]) {
				glDetachShader(program, objects[i]);
				glDeleteShader(objects[i]);
			}
		}
		glDeleteProgram(program);
	}
}

GLuint Shader::GenerateShader(string from, GLenum type) {
	cout << "Compiling shader..." << endl;

	string load;
	if (!LoadShaderFile(from, load)) {
		cout << "Compiling failed! :(" << endl;
		loadFailed = true;
		return 0;
	}

	GLuint shader = glCreateShader(type);

	const char* chars = load.c_str();
	glShaderSource(shader, 1, &chars, NULL);
	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {
		cout << "Compiling failed! :(" << endl;
		char error[512];
		glGetInfoLogARB(shader, sizeof(error), NULL, error);
		cout << error << "\n";
		loadFailed = true;
		return 0;
	}
	cout << "Compiling success! :)" << endl <<endl;
	loadFailed = false;
	return shader;
}

bool Shader::LoadShaderFile(string from, string& into) {
	ifstream file;
	string temp;

	cout << "Loading shader text from " << from << endl << endl;

	file.open(from.c_str());
	if (!file.is_open()) {
		cout << "File does not exist!" << endl;
		return false;
	}

	while (!file.eof()) {
		getline(file, temp);
		into += temp + "\n";
	}

	file.close();
	cout << into << endl << endl;
	cout << "Loaded shader text!" <<  endl << endl;
	return true;
}

void Shader::SetDefaultAttributes() {
	glBindAttribLocation(program, VERTEX_BUFFER, "position");
	glBindAttribLocation(program, COLOUR_BUFFER, "colour");
	glBindAttribLocation(program, NORMAL_BUFFER, "normal");
	glBindAttribLocation(program, TANGENT_BUFFER, "tangent");
	glBindAttribLocation(program, TEXTURE_BUFFER, "texCoord");
}

bool Shader::LinkProgram() {
	if (loadFailed) {
		return false;
	}
	glLinkProgram(program);

	GLint code;
	glGetProgramiv(program, GL_LINK_STATUS, &code);

	if (code == GL_FALSE) {
		cout << "Linking failed! Error log as follows:" << endl;
		char error[2048];
		glGetProgramInfoLog(program, sizeof(error), NULL, error);
		cout << error << endl;
		return false;
	}

	return code == GL_TRUE ? true : false;
}