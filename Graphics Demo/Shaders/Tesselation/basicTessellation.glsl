#version 400 core

layout(vertices = 3) out;

uniform float tessLevelInner;
uniform float tessLevelOuter;
uniform float time;

in Vertex {
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} IN[];

out Vertex {
	vec2 texCoord;
	vec4 colour;
	vec3 worldPos;
	vec3 normal;
} OUT[];


void main(void) {

	int numVertices = 2000;

	gl_TessLevelInner[0] = numVertices;
	gl_TessLevelInner[1] = numVertices;

	gl_TessLevelOuter[0] = numVertices;
	gl_TessLevelOuter[1] = numVertices;
	gl_TessLevelOuter[2] = numVertices;

	OUT[gl_InvocationID].texCoord = IN[gl_InvocationID].texCoord;
	OUT[gl_InvocationID].colour = IN[gl_InvocationID].colour;
	OUT[gl_InvocationID].worldPos = IN[gl_InvocationID].worldPos;
	OUT[gl_InvocationID].normal = IN[gl_InvocationID].normal;

	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

}