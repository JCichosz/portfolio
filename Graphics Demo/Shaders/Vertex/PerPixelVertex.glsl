#version 150

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

in vec3 position;
in vec4 colour;
in vec3 normal;
in vec2 texCoord;

#define MAX_LIGHTS 1

uniform struct Light {
	vec3 position;
	vec4 colour;
	float radius;
	float cutOffAngle;
	vec3 direction;
	sampler2DShadow shadowTex;
	mat4 shadowMatrix;
} allLights[MAX_LIGHTS];

out Vertex {
	vec4 colour;
	vec2 texCoord;
	vec3 normal;
	vec3 worldPos;
} OUT;

void main(void) {
	OUT.colour = colour;
	OUT.texCoord = texCoord;
	
	mat3 normalMatrix = transpose(inverse(mat3(modelMatrix)));
	
	OUT.normal = normalize(normalMatrix * normalize(normal));
	OUT.worldPos = (modelMatrix * vec4(position, 1)).xyz;
	
	mat4 mvp = projMatrix * viewMatrix * modelMatrix;
	
	gl_Position = mvp * vec4(position, 1.0);
}