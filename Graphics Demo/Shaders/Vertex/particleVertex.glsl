#version 460

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform mat4 textureMatrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 colour;
layout(location = 2) in float rotation;
layout(location = 3) in float size;

out Vertex {
	vec4 colour;
	float rotation;
	float size;
} OUT;

void main(void) {
	gl_Position = vec4(position, 1.0);
	OUT.rotation = rotation;
	OUT.size = size;
}