#version 150 core

#define MAX_LIGHTS 2

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

in vec3 position;
in vec3 colour;
in vec3 normal;
in vec3 tangent;
in vec2 texCoord;

uniform struct Light {
	vec3 position;
	vec4 colour;
	float radius;
	float cutOffAngle;
	vec3 direction;
	sampler2DShadow shadowTex;
	mat4 shadowMatrix;
} allLights[MAX_LIGHTS];

out Vertex {
	vec3 colour;
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
	vec3 worldPos;
	vec4 shadowProj[MAX_LIGHTS];	
} OUT;

void main() {
	mat3 normalMatrix = transpose(inverse(mat3(modelMatrix)));
	
	OUT.colour = colour;
	OUT.texCoord = texCoord;
	
	OUT.normal = normalize(normalMatrix * normalize(normal));
	OUT.tangent = normalize(normalMatrix * normalize(tangent));

	OUT.binormal = normalize(normalMatrix * normalize(cross(normal, tangent)));
	
	OUT.worldPos = (modelMatrix * vec4(position, 1)).xyz;
	for(int i = 0; i < MAX_LIGHTS; i++) {
		OUT.shadowProj[i] = (allLights[i].shadowMatrix * modelMatrix* vec4(position+(normal*0.001), 1));
	}
	
	gl_Position = (projMatrix * viewMatrix * modelMatrix) * vec4(position, 1.0);
}
