#version 150

uniform sampler2D diffuseTex;
uniform vec2 pixelSize;

in Vertex {
	vec2 	texCoord;
	vec4 colour;
} IN;

out vec4 fragColor;

const float weights[5] = float[](0.12,0.22, 0.32, 0.22, 0.12);

void main(void){
	
	vec2 valuesVertical[5] = vec2[](vec2(0.0, -pixelSize.y*2), vec2(0.0, -pixelSize.y*1), vec2(0.0, 0.0), vec2 (0.0 , pixelSize.y *1) , vec2 (0.0 , pixelSize.y *2));
	vec2 valuesHorizontal[5] = vec2[](vec2(-pixelSize.x*2, 0.0), vec2(-pixelSize.x*1, 0.0), vec2(0.0, 0.0), vec2 (pixelSize.x *1, 0.0) , vec2 (pixelSize.x *2, 0.0));


	for(int i = 0; i < 5; i++) {
		vec4 tmp = texture2D(diffuseTex, IN.texCoord.xy + valuesVertical[i]);
		fragColor += tmp * weights[i];
		
		tmp = texture2D(diffuseTex, IN.texCoord.xy + valuesHorizontal[i]);
		fragColor += tmp * weights[i];
	}
}