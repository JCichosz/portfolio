#version 150 core

uniform sampler2D diffuseTex;

 in Vertex {
	vec2 texCoord;
	vec4 colour;
	float rotation;
	float size;
 } IN;

 out vec4 fragColour;
 
 
 void main() {
	vec4 col = texture(diffuseTex, IN.texCoord);
	
	if(col.a == 0) {
		discard;
	}
	
	fragColour = col;
 }
