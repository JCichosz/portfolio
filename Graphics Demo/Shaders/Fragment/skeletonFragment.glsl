#version 150
uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;

uniform vec3 cameraPos;
uniform float shine;

#define MAX_LIGHTS 1

uniform struct Light {
	vec3 position;
	vec4 colour;
	float radius;
	float cutOffAngle;
	vec3 direction;
	sampler2DShadow shadowTex;
	mat4 shadowMatrix;
} allLights[MAX_LIGHTS];

in Vertex {
	vec4 colour;
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
	vec3 worldPos;
} IN;

out vec4 fragColour;

void main(void){
	vec4 diffuse = texture(diffuseTex, IN.texCoord);
	
	mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);
	vec3 normal = normalize(TBN *(texture(bumpTex, IN.texCoord).rgb)); // * 2.0 - 1.0));

	vec3 incident = normalize(allLights[0].position - IN.worldPos);
	float lambert = max(0.0, dot(incident, normal));
	
	float distance = length(allLights[0].position - IN.worldPos);
	float attenuation = 1.0 - clamp(distance / allLights[0].radius, 0.0, 1.0);
	
	vec3 viewDir = normalize(cameraPos - IN.worldPos);
	vec3 halfDir = normalize(incident + viewDir);
	
	float rFactor = max(0.0, dot(halfDir, normal));
	float sFactor = pow(rFactor, 50.0);
	
	vec3 colour = ( diffuse.rgb * allLights[0].colour.rgb);
	colour += (allLights[0].colour.rgb * sFactor) * shine;
	fragColour = vec4 (colour * attenuation * lambert , diffuse.a);
	fragColour.rgb += (diffuse.rgb * allLights[0].colour.rgb) * 0.1;
	
}