#version 150
uniform sampler2D diffuseTex;
uniform float elapsed;

in Vertex {
	vec2 texCoord;
	vec4 colour;
} IN;

out vec4 fragColor;

void main(void){	
	vec3 col = texture(diffuseTex, IN.texCoord).rgb;
	float sum = col.r + col.g + col.b;
	float average = sum / 3;
	
	
	fragColor = vec4(col * average, 1);
}