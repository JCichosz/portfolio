#version 330 core

uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;
uniform vec3 cameraPos;

uniform float shine;
uniform int numLights;

#define MAX_LIGHTS 2

uniform struct Light {
	vec3 position;
	vec4 colour;
	float radius;
	float cutOffAngle;
	vec3 direction;
	sampler2DShadow shadowTex;
	mat4 shadowMatrix;
} allLights[MAX_LIGHTS];

in Vertex {
	vec3 colour;
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
	vec3 worldPos;
	vec4 shadowProj[MAX_LIGHTS];	
} IN;

out vec4 fragColour;

void main() {
	fragColour = vec4(0,0,0,0);		
	mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);
	vec3 normal = normalize(TBN*(texture(bumpTex, IN.texCoord).rgb));
		
	vec4 diffuse = texture(diffuseTex, IN.texCoord);
	
	for(int i = 0; i < numLights; i++) {	
		vec3 incident = normalize(allLights[i].position - IN.worldPos);
		float lambert = max(0.0, dot(incident, normal));
		
		float dist = length(allLights[i].position - IN.worldPos);
		float attenuation = 1.0 - clamp(dist / allLights[i].radius, 0.0, 1.0);
		
		vec3 viewDir = normalize(cameraPos - IN.worldPos);
		vec3 halfDir = normalize(incident + viewDir);
		
		float rFactor = max(0.0, dot(halfDir, normal));
		float sFactor = pow(rFactor, 33.0);
		
		float shadow = 1.0;
		
		if(IN.shadowProj[i].w > 0.0) {
			shadow = textureProj(allLights[i].shadowTex, IN.shadowProj[i]);
		}
		
		lambert *= shadow;
		
		vec3 colour = (diffuse.rgb * allLights[i].colour.rgb);
		colour += (allLights[i].colour.rgb * sFactor) * shine;
		fragColour += vec4(colour * attenuation * lambert, diffuse.a);	
	}
	fragColour.rgb += diffuse.rgb * 0.1;
}
