#version 150 core

uniform samplerCube diffuseTex;
uniform samplerCube bumpTex;
uniform float elapsed;
uniform vec3 cameraPos;

in Vertex {
	vec3 normal;
} IN;

out vec4 fragColor;

void main(void){
	vec4 day = texture(diffuseTex, normalize(IN.normal));
	vec4 night = texture(bumpTex, normalize(IN.normal));
	
	float factor = ((cos(elapsed) * 0.5) +0.5);
	
	fragColor = day * factor + night * (1- factor);
}