#version 330 core

uniform sampler2D diffuseTex;
uniform sampler2D effectTex;

in Vertex {
	vec2 texCoord;
	vec4 colour;
} IN;

out vec4 fragColour;

void main(void) {
	vec3 colour = texture(diffuseTex , IN.texCoord).xyz;
	vec3 effect = texture(effectTex , IN.texCoord).xyz;
	
	float factor = effect.r;
	
	fragColour = vec4(colour * factor, 1);
}