#version 150

uniform sampler2D diffuseTex;
uniform sampler2D effectTex;
uniform vec2 pixelSize;

in Vertex {
	vec2 	texCoord;
	vec4 colour;
} IN;

out vec4 fragColor;

void main(void){

	vec4 brightCol = texture2D(effectTex, IN.texCoord.xy);
	vec4 diffuseCol = texture2D(diffuseTex, IN.texCoord.xy);

	fragColor = brightCol * 0.3f + diffuseCol * 0.7;
}