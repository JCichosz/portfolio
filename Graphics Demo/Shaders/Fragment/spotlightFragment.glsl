#version 150
uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;
uniform sampler2D lightTex;
uniform float shine;
uniform float lightRadius;
uniform vec3 cameraPos;
uniform vec4 lightColour;
uniform vec3 lightPos;
uniform vec3 lightDir;
uniform float cutOffAngle;

in Vertex {
	vec4 colour;
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
	vec3 worldPos;
} IN;

out vec4 fragColour;

void main(void){
	vec3 coneDir = normalize(lightDir);
	vec3 rayDirection = lightPos - IN.worldPos;
	
	float lightToPosAngle = degrees(dot(normalize(rayDirection), normalize(lightDir)));
	
	vec4 diffuse = texture(diffuseTex, IN.texCoord);
	
	mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);
	vec3 normal = normalize( TBN *(texture(bumpTex , IN.texCoord ).rgb * 2.0 - 1.0));

	vec3 incident = normalize(rayDirection);
	float lambert = max(0.0, dot(incident, normal));
	
	float distance = length(lightPos - IN.worldPos);
	float attenuation = 1.0; //- clamp(distance / lightRadius, 0.0, 1.0);
	
	if(lightToPosAngle > cutOffAngle || distance > lightRadius) {
		attenuation = 0.0;
	}

	
	vec3 viewDir = normalize(cameraPos - IN.worldPos);
	vec3 halfDir = normalize(incident + viewDir);
	
	float rFactor = max(0.0, dot(halfDir, normal));
	float sFactor = pow(rFactor, 50.0);
	
	vec3 colour = ( diffuse.rgb * lightColour.rgb);
	colour += (lightColour.rgb * sFactor) * shine;
	fragColour = vec4 (colour * attenuation * lambert , diffuse.a);
	fragColour.rgb += (diffuse.rgb * lightColour.rgb) * 0.1;

}