#version 150
uniform sampler2D diffuseTex;
uniform float elapsed;

in Vertex {
	vec2 texCoord;
	vec4 colour;
} IN;

out vec4 fragColor;

void main(void){	
	vec3 col = texture(diffuseTex, IN.texCoord).rgb;
	col.r = col.r * (cos(elapsed) * 0.5 +0.5) *1.2;
	col.g = col.g * max(0.5,(cos(elapsed) *0.5 + 0.5));
	
	fragColor = vec4(col, 1);
}